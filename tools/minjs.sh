#!/bin/sh

#run from /tools folder; parent contains /src
cd ../src

echo "Minifying JS: jfig.min.js"
$(npm bin)/uglifyjs \
js/mousedrag.js \
js/formtools.js \
js/base/jfigLabel.js \
js/base/jfigImage.js \
js/base/jfigEPS.js \
js/base/jfigEPSPathCap.js \
js/base/jfigEPSPathGeneric.js \
js/base/jfigEPSPath.js \
js/base/jfigEPSArc.js \
js/base/jfigCompound.js \
js/jfigProject.js \
js/jfigCanvas.js \
js/jfig.js \
js/graph2d/jfigAxes.js \
js/graph2d/jfigGraph.js \
js/graph2d/jfigGraphImplicit.js \
js/graph2d/jfigGraphDiscrete.js \
js/graph2d/jfigGraphBars.js \
js/graph3d/jfigAxes3d.js \
-m -o jsmin/jfig.min.js
