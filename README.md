# JFig

![](http://tdm.bz/img/jfig/jfig0-thumb.jpg)
![](http://tdm.bz/img/jfig/jfig1-thumb.jpg)

## Use

If you just want to use JFig to make a quick EPS/LaTeX figure, go to <http://tdm.bz/jfig>.

The icons at the top next to `Objects` will place a new element in the figure, and when an object is highlighted, the icons next to `Actions` will perform an action on that specific object (change z-order, delete, etc).  Some objects are nested: graphs are sub-objects of axes.  That means graphs are created via the `Actions` menu for the axes, and if axes are moved or deleted, the graphs will also be moved or deleted.

Projects can be saved and loaded (they are encoded into a JSON object).  Click the icon next to `Export` to download an `eps` and/or `tex` file; these can be included in LaTeX as `\input{filename.tex}` (figure + labels) or `\includegraphics{filename.eps}` (figure only).
