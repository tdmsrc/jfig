
//=======================================================
// OBJECT LIST
//=======================================================

function UIObjectList(){ };

UIObjectList.menuItems = [];

//populate using object list from x (x = any component, e.g. project.contents)
//-----------------------------------------------
UIObjectList.populate = function($target, x){
  
  //clear list
  $target.empty();
  UIObjectList.menuItems = [];
  
  //recreate menu
  $.each(x.getObjectList(0), function(i,M){
    
    //create table row
    var indentString = "";
    for(var i=0; i<M.indent; i++){ indentString += "&nbsp;&nbsp;"; }
    var $menuItem = $("<tr>").append( $("<td>")
      .html(indentString + "<img src=" + M.icon + "> " + M.caption)
      .append("<br>")
    );
    
    //add to menu
    $target.append($menuItem);
    
    //add mouseover handler
    $menuItem
      .mouseenter(function(){ UIObjectList.mouseoverMenuItem(M.object); })
      .mouseleave(function(){ UIObjectList.mouseoverMenuItem(); });
    
    //add click handler
    $menuItem.click(function(){ onSelect(M.object); });
    
    //add to array and link to target object
    $menuItem.data("target", M.object);
    UIObjectList.menuItems.push($menuItem);
  });
};

//update BG color to reflect highlight/mouseover
//-----------------------------------------------
UIObjectList.updateCSS = function($target){
  
  //update BG color of menu items according to target's mouseover/highlight
  $.each(UIObjectList.menuItems, function(i,$menuItem){
    var x = $menuItem.data("target");
    
    //update menu item CSS
    if(x.highlight || x.mouseover){
      $menuItem.css("background", FigOptions.COLOR_BG(x.mouseover, x.highlight));
    }else{
      $menuItem.css("background", "none");
    }
  });
};

//mouseover item
//-----------------------------------------------
UIObjectList.mouseoverMenuItem = function(x){

  //un-mouseover all  
  var redraw0 = project.contents.setMouseover(false);
  
  //mouseover first pick
  if(typeof(x) !== "undefined"){ 
    var redraw1 = x.setMouseover(true);
  }
  
  //redraw if necessary
  if(redraw0 || redraw1){ draw(contextBG,context,figGeom); }
  
  //update menu items' BG color
  UIObjectList.updateCSS();
};


//=======================================================
// ANCHORS
//=======================================================

//position p (units: FIG), icon image src string
//draggable and snaps to grid by default, either can set to false
function UIAnchor(icon, p){
  this.p = [p[0], p[1]];  
  this.$anchor = $("<img>", {"src": icon, "class": "jfig-anchor"});
  
  //optional handlers
  this.onClick = function(){};
  this.onDrag = function(p){}; //p units: FIG
  this.onDragEnd = function(p){}; //p units: FIG
  
  this.draggable = true;
  this.snap = true;
  
  $canvasWrapper.append(this.$anchor);
  this.updateCSS(figGeom);
  
  UIAnchor.anchors.push(this);
};

//TODO global array of UIAnchors
//-----------------------------------------------
UIAnchor.anchors = [];

//reposition anchor element, p is new position (units: CTX)
//-----------------------------------------------
UIAnchor.prototype.reposition = function(p){
  
  this.p[0] = p[0]; this.p[1] = p[1];
  this.updateCSS(figGeom);
};

//check if p hits anchor; return true or false (p units: CTX)
//-----------------------------------------------
UIAnchor.prototype.pick = function(p, figGeom){

    //get width, height, and position
    var aw = this.$anchor.width(), ah = this.$anchor.height();
    var actx = figGeom.FIGtoCTX(this.p);
    
    //check if pick success
    return ( (Math.abs(actx[0]-p[0]) < aw/2) && (Math.abs(actx[1]-p[1]) < ah/2) );
}

//update position
//-----------------------------------------------
UIAnchor.prototype.updateCSS = function(figGeom){

  //get width, height, and position
  var aw = this.$anchor.width(), ah = this.$anchor.height();
  var actx = figGeom.FIGtoCTX(this.p);
  
  //update CSS left and top
  this.$anchor.css({"left": (actx[0]-aw/2) + "px", "top": (actx[1]-ah/2) + "px"});
};

//hide/show/remove all anchors
//-----------------------------------------------
UIAnchor.hideAll = function($anchorContainer){
  $anchorContainer.find(".jfig-anchor").hide();
};

UIAnchor.showAll = function($anchorContainer){
  $anchorContainer.find(".jfig-anchor").show();
};

UIAnchor.removeAll = function($anchorContainer){
  $anchorContainer.find(".jfig-anchor").remove();
  UIAnchor.anchors = [];
};

//call updateCSS on every UIAnchor
//-----------------------------------------------
UIAnchor.updateCSS = function(figGeom){
  $.each(UIAnchor.anchors, function(i,A){ A.updateCSS(figGeom); });
};

//check if p hits any anchor; return the anchor if so (p units: CTX)
//-----------------------------------------------
UIAnchor.pick = function(p, figGeom){
  var pickResult;
  
  //loop through all anchors
  $.each(UIAnchor.anchors, function(i,A){ 
    if(A.pick(p, figGeom)){ pickResult = A; }
  });
  
  return pickResult;
};


//=======================================================
// TOOLBARS
//=======================================================

function UIToolbar(){ };

UIToolbar.createGroup = function($target, caption, buttons){

  //group caption
  $target.append(caption + ": ");
  
  //loop through buttons
  for(var j=0, jl=buttons.length; j<jl; j++){
    UIToolbar.createButton($target, buttons[j].caption, buttons[j].icon, buttons[j].action);
  }
};

UIToolbar.createButton = function($target, caption, icon, action){
  
  //create button
  var $button = $("<a>", {"class": "toolbar-button", "href": "javascript:void(0)"});
  //$button.append(caption + " ");
  $button.append("<img src=\"" + icon + "\">");
  
  //add handlers
  $button.click(action);
  $button.on("mousemove", function(e){
    $tooltip.html(caption);
    var p = [e.pageX, e.pageY];
    var w = $tooltip.width() / 2;
    //$tooltip.css({"left": (p[0]+4) + "px", "top": (p[1]+4) + "px"});
    $tooltip.css({"left": (p[0]-w+4) + "px"});
    $tooltip.show();
  });
  $button.on("mouseleave", function(e){
    $tooltip.hide();
  });
  
  //add button      
  $target.append($button);
};

//groups = array of objects w/ properties "caption" and "buttons"
//buttons = array of objects w/ properties "caption", "icon", and "action"
//-----------------------------------------------
UIToolbar.create = function($target, groups){

  //clear any existing elements
  $target.empty();
  
  //loop through groups
  for(var i=0, il=groups.length; i<il; i++){
    if(i != 0){ $target.append("&nbsp;&nbsp;&nbsp;&nbsp;"); }
    UIToolbar.createGroup($target, groups[i].caption, groups[i].buttons);
  }
};


//=======================================================
//UIMethods
//methods for use in setOptionsHTML/getCustomToolbarButtons/etc
//=======================================================
var UIMethods = Object.freeze({
  
  //CHANGED PROJECT FILENAME/EPS/TEX
  //------------------------------------------
  changedFilename: function(){
    //update download link captions
    $linkEPS.html(project.filename + ".eps");
    $linkTEX.html(project.filename + ".tex");
    $outputJFGLink.html(project.filename + ".jfg");
    
    //update content
    project.outputTEX($outputTEX); //TEX refers to EPS file
    project.outputJFG($outputJFG); //JFG stores filename
  },
  
  //trigger redraw and update of EPS file
  changedEPS: function(){
    draw(contextBG,context,figGeom); //draw EPS onto canvas
    project.outputEPS($outputEPS); //output EPS
    project.outputJFG($outputJFG);  //output JFG
  },
  
  //trigger update of TEX file
  changedLabels: function(){
    project.outputTEX($outputTEX); //output TEX
    project.outputJFG($outputJFG); //output JFG
  },
  
  //TODO: 
  changedImages: function(){
    project.outputJFG($outputJFG); //output JFG
  },
  
  //REFRESH ANCHORS
  //------------------------------------------
  refreshAnchors: function(target){
    UIAnchor.removeAll($canvasWrapper);
    if(typeof(target.getAnchors) == typeof(Function)){
      target.getAnchors(figGeom, UIMethods);
    }
  },
    
  //UPDATE OBJECT LIST
  //------------------------------------------
  changedObjectList: function(){
    UIObjectList.populate($menu, project.contents);
  },
  
  //ADD OBJECT/LABEL/COMPOUND
  //------------------------------------------
  //add EPS object and optionally set focus
  addObject: function(o, focus){
    
    o.removeFromContents = function(){ 
      project.contents.removeObject(o); 
      if(typeof(o.onDelete) == typeof(Function)){ o.onDelete(); }
      
      UIMethods.changedObjectList();
      UIMethods.changedEPS();
    };
    project.contents.addObject(o);
    project.expandZ(o.zsort);
    if(typeof(o.onAdd) == typeof(Function)){ o.onAdd(UIMethods, figGeom); }
    
    UIMethods.changedObjectList();
    UIMethods.changedEPS();
    if(focus){ onSelect(o); }
  },
  
  //add label and optionally set focus
  addLabel: function(l, focus){
    
    l.removeFromContents = function(){ 
      project.contents.removeLabel(l); 
      if(typeof(l.onDelete) == typeof(Function)){ l.onDelete(); }

      UIMethods.changedObjectList();
      UIMethods.changedLabels();
    };
    project.contents.addLabel(l, figGeom);
    project.expandZ(l.zsort);
    if(typeof(l.onAdd) == typeof(Function)){ l.onAdd(UIMethods, figGeom); }

    UIMethods.changedObjectList();
    UIMethods.changedLabels();
    if(focus){ onSelect(l); }
  },
  
  //add image and optionally set focus
  addImage: function(d, focus){
  
    d.removeFromContents = function(){
      project.contents.removeImage(d);
      if(typeof(d.onDelete) == typeof(Function)){ d.onDelete(); }
      
      UIMethods.changedObjectList();
      UIMethods.changedImages();
    };
    project.contents.addImage(d, figGeom);
    project.expandZ(d.zsort);
    if(typeof(d.onAdd) == typeof(Function)){ d.onAdd(UIMethods, figGeom); }
    
    UIMethods.changedObjectList();
    UIMethods.changedImages();
    if(focus){ onSelect(d); }
  },
  
  //add compound and optionally set focus
  addCompound: function(c, focus){
    
    c.removeFromContents = function(){ 
      project.contents.removeCompound(c); 
      if(typeof(c.onDelete) == typeof(Function)){ c.onDelete(); }
      
      UIMethods.changedObjectList();
      UIMethods.changedEPS(); 
      UIMethods.changedLabels();
      UIMethods.changedImages();
    };
    project.contents.addCompound(c, figGeom);
    project.expandZ(c.zsort);
    if(typeof(c.onAdd) == typeof(Function)){ c.onAdd(UIMethods, figGeom); }
    
    UIMethods.changedObjectList();
    UIMethods.changedEPS();
    UIMethods.changedLabels();
    UIMethods.changedImages();
    if(focus){ onSelect(c); }
  }

});

    
//=======================================================
// onSelect
//=======================================================

//only use within onSelect; to deselect everything use "onSelect()"
var unSelect = function(){
  project.contents.setHighlight(false);
  
  //clear custom toolbar, options panel, and anchors
  $toolbarSelection.empty();
  $optionsPanel.hide();
  
  $inputJFGPanel.hide();
  $outputJFGPanel.hide();
  $outputPanelEPSTEX.hide();
  
  UIAnchor.removeAll($canvasWrapper);
};

var onSelect = function(target){
  
  //deselect all  
  unSelect();
  
  //if actual target has been specified
  if(typeof(target) !== "undefined"){ 
    target.setHighlight(true);
    
    //add custom toolbar buttons
    var buttons = [];
    if(typeof(target.getCustomToolbarButtons) == typeof(Function)){ 
      buttons = target.getCustomToolbarButtons(UIMethods);
      //TODO: put a toolbar separator here
    }
    buttons.push(
      //------------------
      { caption: "To Front", icon: "image/z-front.png", action: function(){
        if(target.zsort >= project.zmax){ return; }
        target.zsort = project.zmax+1;
        project.expandZ(target.zsort);
        UIMethods.changedEPS();
      }},
      //------------------
      { caption: "To Back", icon: "image/z-back.png", action: function(){
        if(target.zsort <= project.zmin){ return; }
        target.zsort = project.zmin-1;
        project.expandZ(target.zsort);
        UIMethods.changedEPS();
      }},
      //------------------
      { caption: "Duplicate (not implemented)", icon: "image/duplicate.png", action: function(){
        //TODO
      }},
      //------------------
      { caption: "Remove", icon: "image/delete.png", action: function(){
        unSelect();
        $tooltip.hide();
        target.removeFromContents();
      }}
      //------------------
    );
    UIToolbar.create( $toolbarSelection, [{ caption: "Action", buttons: buttons }] );
    
    //display options panel
    if(typeof(target.setOptionsHTML) == typeof(Function)){
      $optionsPanel.empty();
      target.setOptionsHTML($optionsPanel, figGeom, UIMethods);
      $optionsPanel.show();
    }
    
    //create anchors
    if(typeof(target.getAnchors) == typeof(Function)){
      target.getAnchors(figGeom, UIMethods);
    }
  }
  
  //redraw
  draw(contextBG,context,figGeom);
  //update object list CSS to reflect mouseover/highlight
  UIObjectList.updateCSS();
};


//=======================================================
// MOUSE HANDLER
//=======================================================

var UIMouseDragHandler = {
  
  //anchorBeingDragged: undefined, //<- gets set to whatever anchor is being dragged
  anchorMode: false,
  
  onDragStart: function(p){
    
    //pick anchor
    var anchorPicked = UIAnchor.pick(p, figGeom);
    if(typeof(anchorPicked) !== "undefined"){
      if(anchorPicked.draggable){
        this.anchorBeingDragged = anchorPicked;
        this.anchorMode = true;
        return;
      }
    }
    this.anchorMode = false;
    
    //dragging canvas
    if(!FigOptions.DRAG_DIVS_VISIBLE){ 
      $canvasWrapper.find(".jfig-label").hide(); 
      $canvasWrapper.find(".jfig-image").hide();
    }
    if(!FigOptions.DRAG_ANCHORS_VISIBLE){ UIAnchor.hideAll($canvasWrapper);  }
  },
  
  onDrag: function(p, dp){
  
    //dragging anchor
    if(this.anchorMode){
      var pfig = figGeom.CTXtoFIG(p);
      if(this.anchorBeingDragged.snap){ pfig = snapFIG(pfig); }
      this.anchorBeingDragged.reposition(pfig);
      this.anchorBeingDragged.onDrag(pfig);
      return;
    }
    
    //dragging canvas
    var cpos = $canvas.offset();
    $canvas.offset({left: cpos.left + dp[0], top: cpos.top + dp[1]});
    
    var cposBG = $canvasBG.offset();
    $canvasBG.offset({left: cposBG.left + dp[0], top: cposBG.top + dp[1]});
    
    figGeom.offsetx += dp[0];
    figGeom.offsety += dp[1];
    
    if(FigOptions.DRAG_DIVS_VISIBLE){ 
      UIAnchor.updateCSS(figGeom);
      project.contents.updateCSS(figGeom);
    }
  },
  
  onDragEnd: function(p){
  
    //dragging anchor
    if(this.anchorMode){
      var pfig = figGeom.CTXtoFIG(p);
      if(this.anchorBeingDragged.snap){ pfig = snapFIG(pfig); }
      this.anchorBeingDragged.onDragEnd(pfig);
      return;
    }
    
    //dragging canvas
    if(!FigOptions.DRAG_DIVS_VISIBLE){ 
      $canvasWrapper.find(".jfig-label").show(); 
      $canvasWrapper.find(".jfig-image").show();
    }
    if(!FigOptions.DRAG_ANCHORS_VISIBLE){ UIAnchor.showAll($canvasWrapper); }
    
    $canvas.css({"left": "0px", "top": "0px"});
    $canvasBG.css({"left": "0px", "top": "0px"});
    
    draw(contextBG,context,figGeom); 
    UIAnchor.updateCSS(figGeom);
    project.contents.updateCSS(figGeom);
  },
  
  onClick: function(p){
  
    //pick anchor
    var anchor = UIAnchor.pick(p, figGeom);
    if(typeof(anchor) !== "undefined"){ 
      anchor.onClick();
      return;
    }
    
    //select first pick
    var pickResult = project.contents.pickSplit(context, figGeom, p);
    onSelect(pickResult);
  }
};


//=======================================================
// INITIALIZE
//=======================================================

//all HTML elements with ID
//-----------------------------------------------
var $outputJFGPanel = $("#div-jfg-output-panel");
var $outputJFG = $("#textarea-jfg-output");
var $outputJFGLink = $("#link-jfg-output");

var $inputJFGPanel = $("#div-jfg-input-panel");
var $inputJFG = $("#textarea-jfg-input");
var $inputJFGLink = $("#link-jfg-input");

var $outputPanelEPSTEX = $("#div-output-panel");
var $outputFilename = $("#text-filename");
var $outputEPS = $("#textarea-eps");
var $outputTEX = $("#textarea-tex");
var $linkEPS = $("#link-eps");
var $linkTEX = $("#link-tex");

var $toolbarMain = $("#div-toolbar-main");
var $toolbarElements = $("#div-toolbar-elements");
var $toolbarSelection = $("#div-toolbar-selection");
var $tooltip = $("#div-tooltip");

var $optionsPanel = $("#div-options-panel");
var $menu = $("#table-menu");
var $footer = $("#div-footer");

var $canvasWrapper = $("#div-canvas-wrapper");
var $canvasOverlay = $("#div-canvas-overlay");

var $canvasBG = $("#canvasBG");
var contextBG = $canvasBG[0].getContext("2d");

var $canvas = $("#canvas");
var context = $canvas[0].getContext("2d");
//-----------------------------------------------

//UI options
//-----------------------------------------------
var FigOptions = Object.freeze({
  
  DRAG_TOLERANCE: 8, //px
  DRAG_DIVS_VISIBLE: false,
  DRAG_ANCHORS_VISIBLE: false,
  
  PICK_WIDTH: 6, //px
  
  COLOR_HIGHLIGHT: "rgba(255,0,255,0.2)",
  COLOR_MOUSEOVER: "rgba(255,0,255,0.125)",
  COLOR_HIGHLIGHT_MOUSEOVER: "rgba(255,0,255,0.3)",

  COLOR_BG: function(mouseover, highlight){
    return highlight ? (mouseover ? 
      FigOptions.COLOR_HIGHLIGHT_MOUSEOVER : 
      FigOptions.COLOR_HIGHLIGHT ) :
      FigOptions.COLOR_MOUSEOVER;
  }
});

var snap = true;
var snapOrigin = [0,0]; //any point to be on snap grid (units: PT)
var snapSize = [9,9]; //0.25 inch (units: PT)

var snapFIG = function(p){
  if(!snap){ return p; }

  var pt = [ p[0]-snapOrigin[0], p[1]-snapOrigin[1] ];
  var ps = [ 
    Math.round(pt[0] / snapSize[0]) * snapSize[0],
    Math.round(pt[1] / snapSize[1]) * snapSize[1]
  ];
  return [ ps[0]+snapOrigin[0], ps[1]+snapOrigin[1] ];
};
//-----------------------------------------------


//initialize geometry
var project = new JFigProject();
project.epsSize = [3,2].map(FigTools.INtoPT);

var figGeom = new FigGeometry();
resize($canvasWrapper, $canvasBG, $canvas, figGeom);
figGeom.center();


//=======================================================
// CREATE UI/SET HANDLERS
//=======================================================

//MAIN TOOLBAR (view, output, etc)
//-------------------------------------------------------
var createToolbarMain = function(){
  
  UIToolbar.create($toolbarMain, [
    //================
    {caption: "Canvas", buttons: [
      //------------------
      {caption: "Canvas Properties", icon: "image/canvas.png", action: function(){
        onSelect();
        $optionsPanel.empty();
        canvasSetOptionsHTML($optionsPanel, figGeom, UIMethods);
        $optionsPanel.show();
      }}
      //------------------
    ]}, 
    //================
    {caption: "View", buttons: [
      //------------------
      {caption: "Center View", icon: "image/center.png", action: function(){
        figGeom.center();
        draw(contextBG,context,figGeom); 
        UIAnchor.updateCSS(figGeom);
        project.contents.updateCSS(figGeom);
      }},
      //------------------
      {caption: "Zoom In", icon: "image/zoom-in.png", action: function(){
        figGeom.zoomFix(figGeom.zoom * 1.25);
        draw(contextBG,context,figGeom); 
        UIAnchor.updateCSS(figGeom);
        project.contents.updateCSS(figGeom);
      }},
      //------------------
      {caption: "Zoom 100%", icon: "image/zoom-reset.png", action: function(){
        figGeom.zoomFix(1);
        draw(contextBG,context,figGeom); 
        UIAnchor.updateCSS(figGeom);
        project.contents.updateCSS(figGeom);
      }},
      //------------------
      {caption: "Zoom Out", icon: "image/zoom-out.png", action: function(){
        figGeom.zoomFix(figGeom.zoom * 0.8);
        draw(contextBG,context,figGeom); 
        UIAnchor.updateCSS(figGeom);
        project.contents.updateCSS(figGeom);
      }}
      //------------------
    ]}, 
    //================
    {caption: "Export", buttons: [
      //------------------
      {caption: "View EPS/TeX", icon: "image/output-epstex.png", action: function(){
        onSelect();
        $outputPanelEPSTEX.show();
      }}
      //------------------
    ]},
    //================
    {caption: "Project", buttons: [
      //------------------
      {caption: "New", icon: "image/project-new.png", action: function(){
        onSelect();
        project.contents.clear();
        UIMethods.changedObjectList();
        UIMethods.changedEPS();
        UIMethods.changedLabels();
        UIMethods.changedImages();
      }},
      //------------------
      {caption: "Load", icon: "image/project-load.png", action: function(){
        onSelect();
        $inputJFGPanel.show();
      }},
      //------------------
      {caption: "Save", icon: "image/project-save.png", action: function(){
        onSelect();
        $outputJFGPanel.show();
      }},
      //------------------
      {caption: "Save/Load Cycle Test", icon: "image/project-saveload.png", action: function(){
        
        //proj->OBJ
        console.log("1. project -> object");
        var saveObj = project.saveRaw();
        
        //obj->string
        var strJSON = JSON.stringify(saveObj);
        console.log("2. object -> JSON:");
        console.log(strJSON);
        //-------
        //string->obj
        var objJSON = $.parseJSON(strJSON);
        console.log("3. JSON -> object:");
        console.log(objJSON);
        //-------
        //deselect and load project
        console.log("4. reload from object");
        onSelect();
        project.loadRaw(objJSON, UIMethods);
        //-------
        console.log("If you noticed any changes (other than refreshing labels), something went wrong!");
      }}
      //------------------
    ]}
    //================
  ]);
};

//ELEMENT CREATION TOOLBAR
//-------------------------------------------------------
var createToolbarElements = function(){
  
  UIToolbar.create($toolbarElements, [
    //================
    {caption: "Objects", buttons: [
      //------------------
      {caption: "Create " + EPSPath.caption, icon: EPSPath.icon, action: function(){
        var path = new EPSPath();
        path.zsort = project.zmax+1;
        
        path.points.push( [0,0].map(FigTools.INtoPT) );
        path.points.push( [1,1].map(FigTools.INtoPT) );
        path.points.push( [0,1].map(FigTools.INtoPT) );
        
        UIMethods.addObject(path, true);
      }},
      //------------------
      {caption: "Create " + EPSArc.caption, icon: EPSArc.icon, action: function(){
        var arc = new EPSArc();
        arc.zsort = project.zmax+1;
        UIMethods.addObject(arc, true);
      }},
      //------------------
      {caption: "Create " + JFigLabel.caption, icon: JFigLabel.icon, action: function(){
        var label = new JFigLabel();
        label.zsort = project.zmax+1;
        UIMethods.addLabel(label, true);
      }},
      //------------------
      {caption: "Create " + JFigAxes.caption, icon: JFigAxes.icon, action: function(){
        var axes = new JFigAxes();
        axes.zsort = project.zmax+1;
        UIMethods.addCompound(axes, true);
      }},
      //------------------
      {caption: "Create " + JFigAxes3d.caption, icon: JFigAxes3d.icon, action: function(){
        var axes3d = new JFigAxes3d(figGeom);
        axes3d.zsort = project.zmax+1;
        UIMethods.addImage(axes3d, true);
      }}
      //------------------
    ]}, 
    //================
  ]);
  
};

//DOCUMENT READY BLOCK
//-------------------------------------------------------
$(document).ready(function(){  
    
  //RESIZE HANDLER
  //-----------------------------------------------
  $(window).resize(function(){ 
    resize($canvasWrapper, $canvasBG, $canvas, figGeom); 
    draw(contextBG,context,figGeom); 
    UIAnchor.updateCSS(figGeom);
    project.contents.updateCSS(figGeom);
  });
  
  //TOOLBARS
  //-----------------------------------------------
  createToolbarMain();
  createToolbarElements();
  
  //MOUSEWHEEL
  //-----------------------------------------------
  var onWheelUp = function(p){
    var pfig = figGeom.CTXtoFIG(p);
    figGeom.zoom *= 1.25;
    figGeom.setOffset(pfig, p);
    
    draw(contextBG,context,figGeom);
    UIAnchor.updateCSS(figGeom);
    project.contents.updateCSS(figGeom);
  };
  
  var onWheelDown = function(p){
    var pfig = figGeom.CTXtoFIG(p);
    figGeom.zoom *= 0.8;
    figGeom.setOffset(pfig, p);
    
    draw(contextBG,context,figGeom);
    UIAnchor.updateCSS(figGeom);
    project.contents.updateCSS(figGeom);
  };
  
  $canvasOverlay.addWheelHandler(onWheelUp, onWheelDown);
  
  //MOUSE HANDLERS
  //-----------------------------------------------
  $canvasOverlay.bind("contextmenu", function(event){ event.preventDefault(); });
  $canvasOverlay.addMouseDragHandler(UIMouseDragHandler, FigOptions.DRAG_TOLERANCE);
  
  //ADD CONTENTS TO UI
  //-----------------------------------------------
  project.contents.appendHTML($canvasWrapper, figGeom);
  $outputFilename.val(project.filename);
  UIMethods.changedFilename();
  UIMethods.changedEPS();
  UIMethods.changedLabels();
  UIMethods.changedImages();
  
  //SAVE LINKS
  //-----------------------------------------------
  $outputFilename.change(function(e){
    project.filename = $(this).val();
    UIMethods.changedFilename();
  });
  
  $linkEPS.on("click", function(){
    var blob = new Blob([ $outputEPS.val() ], {type: "text/plain;charset=utf-8"});
    saveAs(blob, project.filename + ".eps");
  });
  
  $linkTEX.on("click", function(){
    var blob = new Blob([ $outputTEX.val() ], {type: "text/plain;charset=utf-8"});
    saveAs(blob, project.filename + ".tex");
  });
  
  $outputJFGLink.on("click", function(){
    var blob = new Blob([ $outputJFG.val() ], {type: "text/plain;charset=utf-8"});
    saveAs(blob, project.filename + ".jfg");
  });
  
  //LOAD LINK
  //-----------------------------------------------
  $inputJFGLink.on("click", function(){
    var strJFG = $inputJFG.val();
    
    //string->obj
    var objJSON = $.parseJSON(strJFG);
    //deselect and load project
    onSelect();
    project.loadRaw(objJSON, UIMethods);
  });
});
