//Defines EPSMacro, EPSWriter, and EPSPen
//
//Postscript operators reference: 
//  https://atrey.karlin.mff.cuni.cz/~milanek/PostScript/Reference/REF.html

//EPS objects (e.g. EPSPath, EPSArc) should have "zsort", "caption", and "icon" properties
//(e.g., "Path", "image/object-path.png") in prototype, as well as these methods:
//
//  [Save/Load]
//  saveRaw() //save any data necessary to recreate the instance to a JS object
//  loadRaw(raw) //new instance from saveRaw output
//  
//  [Translate the object's position]
//  translate(x, y) (x,y units: PT)
//  
//  [Return true if pctx is on the object]
//  pick(context, figGeom, pctx)
//  
//  [Return true if value changed]
//  setMouseover(bool), setHighlight(bool)
//  
//  [Use putline to output the EPS to draw the object]
//  writeEPS(putline)
//  
//  [Draw the object onto the given context]
//  draw(context, figGeom)
//  
//  [UI]
//  getObjectList() (list of objects to show in UI; see UIObjectList)
//  getCustomToolbarButtons() (OPTIONAL: see UIToolbar "buttons")
//  getAnchors(figGeom, UIMethods) (OPTIONAL: create UIAnchor objects)
//  setOptionsHTML($target, figGeom, UIMethods) (OPTIONAL)

//=======================================================
// EPS WRITER
//=======================================================

//macros to make for shorter EPS
//-------------------------------------------------------
var EPSMacro = Object.freeze({
  NEWPATH:      { get:"n",  eps:"newpath" },
  CLOSEPATH:    { get:"cp", eps:"closepath" },
  STROKE:       { get:"s",  eps:"stroke" },
  FILL:         { get:"f",  eps:"gsave fill grestore" },
  CLIP:         { get:"x",  eps:"gsave eoclip" },
  UNCLIP:       { get:"ux", eps:"grestore" },
  MOVETO:       { get:"m",  eps:"moveto" },
  LINETO:       { get:"l",  eps:"lineto" },
  CURVETO:      { get:"c",  eps:"curveto" },
  SETDASH:      { get:"sd", eps:"setdash" },
  SETLINEJOIN:  { get:"lj", eps:"setlinejoin" },
  SETLINECAP:   { get:"lc", eps:"setlinecap" },
  SETLINEWIDTH: { get:"lw", eps:"setlinewidth" },
  SETRGBCOLOR:  { get:"sc", eps:"setrgbcolor" }
});

//write EPS file
//-------------------------------------------------------
var EPSWriter = Object.freeze({

  //begin EPS output
  writeBegin: function(putline, title, epsSize){
    //preamble
    putline("%!PS-Adobe-2.0 EPSF-2.0");
    putline("%%Creator: " + "jfig (debug)");
    putline("%%Title: " + title);
    putline("%%BoundingBox: 0 0 " + epsSize[0] + " "  + epsSize[1]);
    putline("%%Pages: 1");
    putline("%%Page: 1 1");
    putline("%%EndComments");
    
    //macro definitions
    for(var macro in EPSMacro){
      putline("/" + EPSMacro[macro].get + " {" + EPSMacro[macro].eps + "} bind def");
    }
  },
  
  //end EPS output
  writeEnd: function(putline){ putline("%%EOF"); }
});


//=======================================================
// EPS PEN
// style in which to stroke/fill a path
//=======================================================

function EPSPen(){
  
  //set default values
  this.stroked = EPSPen.Defaults.STROKED;
  this.strokeColor = EPSPen.Defaults.STROKE_COLOR;
  this.strokeWidth = EPSPen.Defaults.STROKE_WIDTH;
  
  this.fillColor = EPSPen.Defaults.FILL_COLOR;
  
  this.dashed = EPSPen.Defaults.DASHED;
  this.dashedFillUnits = EPSPen.Defaults.DASHED_FILL_UNITS;
  this.dashedSpaceUnits = EPSPen.Defaults.DASHED_SPACE_UNITS;
  
  this.lineJoin = EPSPen.Defaults.LINE_JOIN;
  this.lineCap = EPSPen.Defaults.LINE_CAP;
};

//enum for line join type
//-----------------------------------------------
EPSPen.LineJoinType = Object.freeze({ 
  MITER:    { eps: 0, ctx: "miter" },
  ROUND:    { eps: 1, ctx: "round" },
  BEVEL:    { eps: 2, ctx: "bevel" }
});

//enum for line cap type
//-----------------------------------------------
EPSPen.LineCapType = Object.freeze({ 
  BUTT:    { eps: 0, ctx: "butt" },
  ROUND:   { eps: 1, ctx: "round" },
  SQUARE:  { eps: 2, ctx: "square" }
});

//define default values
//-----------------------------------------------
EPSPen.Defaults = Object.freeze({
  STROKED: true,
  STROKE_COLOR: [0,0,0], //[r,g,b] 0-255
  STROKE_WIDTH: 0.75, //units: PT (0.75pt=1px if 96px/in and 72pt/in)
  
  FILL_COLOR: [128,128,255], //[r,g,b] 0-255
  
  DASHED: false,
  DASHED_FILL_UNITS: 3,
  DASHED_SPACE_UNITS: 2,
  
  LINE_JOIN: EPSPen.LineJoinType.ROUND,
  LINE_CAP: EPSPen.LineCapType.BUTT
});


//enum for stroke type (custom, not part of EPS/CTX)
//caption/dashed required; fill/space required iff dashed==true
//-----------------------------------------------
EPSPen.StrokeType = Object.freeze({
  SOLID: { caption: "Solid", dashed: false },
  DASH:  { caption: "Dash",  dashed: true, dashedFillUnits: 3, dashedSpaceUnits: 2 },
  DOT:   { caption: "Dot",   dashed: true, dashedFillUnits: 1, dashedSpaceUnits: 1 }
});

//check if dashed properties match the EPSPen.StrokeType "type"
//-----------------------------------------------
EPSPen.prototype.checkStrokeType = function(type){

  //check if dashed property matches
  if(this.dashed != type.dashed){ return false; }
  
  //if dashed is true, need to check fill/space
  if(type.dashed){
    if(this.dashedFillUnits != type.dashedFillUnits){ return false; }
    if(this.dashedSpaceUnits != type.dashedSpaceUnits){ return false; }
  }
  
  //true if no mismatch by now
  return true;
};

//set dashed properties to match EPSPen.StrokeType "type"
//-----------------------------------------------
EPSPen.prototype.setStrokeType = function(type){

  //set dashed property
  this.dashed = type.dashed;
  
  //if dashed, set fill/space
  if(type.dashed){
    this.dashedFillUnits = type.dashedFillUnits;
    this.dashedSpaceUnits = type.dashedSpaceUnits;
  }
};

//=======================================================
// SAVE/LOAD
// skip default values to avoid writing tons of default pens
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
EPSPen.colorsEqual = function(x, y){
  return (x[0] == y[0]) && (x[1] == y[1]) && (x[2] == y[2]);
};

EPSPen.prototype.saveRaw = function(){
  var ret = {};
  
  if(this.stroked != EPSPen.Defaults.STROKED)
    { ret.stroked = this.stroked; }
  if(!EPSPen.colorsEqual(this.strokeColor, EPSPen.Defaults.STROKE_COLOR))
    { ret.strokeColor = this.strokeColor; }
  if(this.strokeWidth != EPSPen.Defaults.STROKE_WIDTH)
    { ret.strokeWidth = this.strokeWidth; }
  
  if(!EPSPen.colorsEqual(this.fillColor, EPSPen.Defaults.FILL_COLOR))
    { ret.fillColor = this.fillColor; }
  
  if(this.dashed != EPSPen.Defaults.DASHED)
    { ret.dashed = this.dashed; }
  if(this.dashedFillUnits != EPSPen.Defaults.DASHED_FILL_UNITS)
    { ret.dashedFillUnits = this.dashedFillUnits; }
  if(this.dashedSpaceUnits != EPSPen.Defaults.DASHED_SPACE_UNITS)
    { ret.dashedSpaceUnits = this.dashedSpaceUnits; }
  
  if(this.lineJoin.eps != EPSPen.Defaults.LINE_JOIN.eps)
    { ret.lineJoin = this.lineJoin; }
  if(this.lineCap.eps != EPSPen.Defaults.LINE_CAP.eps)
    { ret.lineCap = this.lineCap; }
  
  //no skipping default values
  /*ret.stroked = this.stroked;
  ret.strokeColor = this.strokeColor;
  ret.strokeWidth = this.strokeWidth;

  ret.fillColor = this.fillColor;

  ret.dashed = this.dashed;
  ret.dashedFillUnits = this.dashedFillUnits;
  ret.dashedSpaceUnits = this.dashedSpaceUnits;

  ret.lineJoin = this.lineJoin;
  ret.lineCap = this.lineCap;*/

  return ret;
}

//create instance from data returned by saveRaw
//-----------------------------------------------
EPSPen.loadRaw = function(raw){
  var ret = new EPSPen(); //loads EPSPen.Defaults
  
  if(raw.stroked !== undefined){ ret.stroked = raw.stroked; }
  if(raw.strokeColor !== undefined){ ret.strokeColor = raw.strokeColor; }
  if(raw.strokeWidth !== undefined){ ret.strokeWidth = raw.strokeWidth; }
  
  if(raw.fillColor !== undefined){ ret.fillColor = raw.fillColor; }
  
  if(raw.dashed !== undefined){ ret.dashed = raw.dashed; }
  if(raw.dashedFillUnits !== undefined){ ret.dashedFillUnits = raw.dashedFillUnits; }
  if(raw.dashedSpaceUnits !== undefined){ ret.dashedSpaceUnits = raw.dashedSpaceUnits; }
  
  if(raw.lineJoin !== undefined){ ret.lineJoin = raw.lineJoin; }
  if(raw.lineCap !== undefined){ ret.lineCap = raw.lineCap; }
  
  return ret;
};


//=======================================================
// EPS OUTPUT
//=======================================================

//get EPS code for filling current path with this pen
//-------------------------------------------------------
EPSPen.prototype.epsFill = function(putline){

  var eps = FigTools.RGBto01(this.fillColor, " ") + " " + EPSMacro.SETRGBCOLOR.get;
  eps += " " + EPSMacro.FILL.get;
  
  putline(eps);
};

//get EPS code for stroking current path with this pen
//-------------------------------------------------------
EPSPen.prototype.epsStroke = function(putline){
  
  //color and width
  var eps = FigTools.RGBto01(this.strokeColor, " ") + " " + EPSMacro.SETRGBCOLOR.get;
  eps += " " + this.strokeWidth + " " + EPSMacro.SETLINEWIDTH.get;
  
  //dashed
  if(this.dashed){
    eps += " [" + this.dashedFillUnits + " " + this.dashedSpaceUnits + "] 0 " + EPSMacro.SETDASH.get;
  }else{
    eps += " [] 0 " + EPSMacro.SETDASH.get;
  }
  
  //line join type
  eps += " " + this.lineJoin.eps + " " + EPSMacro.SETLINEJOIN.get;
  eps += " " + this.lineCap.eps + " " + EPSMacro.SETLINECAP.get;
  
  //stroke
  eps += " " + EPSMacro.STROKE.get;
  
  putline(eps);
};


//=======================================================
// DRAW ONTO CANVAS
//=======================================================

//prepare context for filling but do NOT fill
//-------------------------------------------------------
EPSPen.prototype.drawFill = function(context){
  
  context.fillStyle = FigTools.RGBtoHEX(this.fillColor);
};

//prepare context for stroking, but do NOT stroke
//-------------------------------------------------------
EPSPen.prototype.drawStroke = function(context, figGeom){

  //color and width
  context.strokeStyle = FigTools.RGBtoHEX(this.strokeColor);
  context.lineWidth = figGeom.PTtoPX(this.strokeWidth);
  
  //dashed
  if(this.dashed){
    FigTools.DASH(context, [figGeom.PTtoPX(this.dashedFillUnits), figGeom.PTtoPX(this.dashedSpaceUnits)]);
  }else{
    FigTools.DASH(context, [1,0]);
  }
  
  //line join type
  context.lineJoin = this.lineJoin.ctx;
  context.lineCap = this.lineCap.ctx;
};

//prepare context for highlighting, but NO COLOR and do NOT stroke
//-------------------------------------------------------
EPSPen.prototype.drawHighlight = function(context, figGeom){

  //color and width
  context.lineWidth = figGeom.PTtoPX(this.strokeWidth) + FigOptions.PICK_WIDTH;
  
  //dashed
  FigTools.DASH(context, [1,0]);
  
  //line join type
  context.lineJoin = this.lineJoin.ctx;
  context.lineCap = this.lineCap.ctx;
};


//=======================================================
// UI
//=======================================================

//append options for stroke
//-------------------------------------------------------
EPSPen.prototype.appendStrokeOptionsHTML = function($target, caption, figGeom, UIMethods){
  var _this = this;
  
  //PEN STROKE
  //------------------------
  $strokedSpan = $("<span>");
  $target.append($("<div class=panel-subtitle>").append(caption, $strokedSpan));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $stroked = FormTools.createCheckbox($strokedSpan, "Stroke", _this.stroked, function(val){
    _this.stroked = val;
    UIMethods.changedEPS();
  });
  
  //dashed
  var strokeRadioOption = function(type){
    return {
      caption: type.caption,
      onSelect: function(){
        _this.setStrokeType(type);
        UIMethods.changedEPS();    
      },
      selected: _this.checkStrokeType(type)
    };
  };
  var $dashed = FormTools.createRadioGroup($("<td align=right>"), "halign",
    strokeRadioOption(EPSPen.StrokeType.SOLID),
    strokeRadioOption(EPSPen.StrokeType.DASH),
    strokeRadioOption(EPSPen.StrokeType.DOT)
  );
  $table.append($("<tr>").append( $("<td>").append("Dashed:"), $dashed ));
  
  //color
  var $colorContainerStroke = $("<td colspan=2>");
  FormTools.createColorSelect($colorContainerStroke, _this.strokeColor, function(rgb){
    _this.strokeColor = rgb;
    UIMethods.changedEPS();
  });
  $table.append($("<tr>").append( $colorContainerStroke ));
};

//append options for fill
//pen does not have a "filled" property, so need to supply: bool getFilled(){}, void setFilled(bool){}
//-------------------------------------------------------
EPSPen.prototype.appendFillOptionsHTML = function($target, caption, figGeom, UIMethods, getFilled, setFilled){
  var _this = this;
  
  //PEN FILL
  //------------------------
  $filledSpan = $("<span>");
  $target.append($("<div class=panel-subtitle>").append(caption, $filledSpan));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $filled = FormTools.createCheckbox($filledSpan, "Fill", getFilled(), function(val){
    setFilled(val);
    UIMethods.changedEPS();
  });
  
  var $colorContainerFill = $("<td colspan=2>");
  FormTools.createColorSelect($colorContainerFill, _this.fillColor, function(rgb){
    _this.fillColor = rgb;
    UIMethods.changedEPS();
  });
  $table.append($("<tr>").append( $colorContainerFill ));
  
};
