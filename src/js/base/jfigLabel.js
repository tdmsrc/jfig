//Defines LatexWriter, JFigLabel

//=======================================================
// LATEX WRITER
//=======================================================

//write latex file for array of labels
//-------------------------------------------------------
LatexWriter = Object.freeze({

  //begin Latex output
  //NOTE: eps filename (e.g. "test.eps") is included in the latex without escaping etc
  writeBegin: function(putline, epsFilename, epsSize){
    putline("\\begin{picture}(0,0)%");
    putline("  \\includegraphics{" + epsFilename + "}%");
    putline("\\end{picture}%");
    putline("\\setlength{\\unitlength}{1pt}%");
    putline("\\begin{picture}(" + epsSize[0] + "," + epsSize[1] + ")(0,0)%");
  },
  
  //end Latex output
  writeEnd: function(putline){
    putline("\\end{picture}%");
  }
});


//=======================================================
// FIG LABEL
//=======================================================

function JFigLabel(){
  this.$label = $("<span>", {"class": "jfig-label"});
  this.zsort = 0;
  
  //set default values
  this.p = [0,0]; //position, units: PT
  this.halign = JFigLabel.Align.LEFT;
  this.valign = JFigLabel.Align.BOTTOM;
  
  this.color = [0,0,0]; //[r,g,b] 0-255
  this.size = 12; //units: PT
  
  this.text = "New Label";
  this.latex = false;
  
  this.mouseover = false;
  this.highlight = false;
};

//enum for alignment (value irrelevant)
//-------------------------------------------------------
JFigLabel.Align = Object.freeze({ 
  LEFT:   0,
  RIGHT:  1,
  CENTER: 2, 
  TOP:    3,
  BOTTOM: 4
});

//translate position (x,y units: PT)
//-------------------------------------------------------
JFigLabel.prototype.translate = function(x,y){
  this.p[0] += x;
  this.p[1] += y;
};

//picking (pctx is point on context); return true if picked
//-------------------------------------------------------
JFigLabel.prototype.pick = function(figGeom, pctx){

  //bounding box of $label in px
  var lt = [ this.$label.position().left, this.$label.position().top ];
  var w = this.$label.width(), h = this.$label.height();
  
  //check if p is in the bounding box
  return( 
    (pctx[0] >= lt[0]) && (pctx[0] <= (lt[0]+w)) &&
    (pctx[1] >= lt[1]) && (pctx[1] <= (lt[1]+h))
  );
};

//[PRIVATE] update "background" CSS based on mouseover/highlight status
//-------------------------------------------------------
JFigLabel.prototype.updateBGColor = function(){
  
  if(this.highlight || this.mouseover){
    this.$label.css("background", FigOptions.COLOR_BG(this.mouseover, this.highlight));
  }else{
    this.$label.css("background", "none");
  }
};

//set mouseover; return false (no redraw)
//-------------------------------------------------------
JFigLabel.prototype.setMouseover = function(val){
  this.mouseover = val;
  this.updateBGColor();
  return false;
};

//set highlight; return false (no redraw)
//-------------------------------------------------------
JFigLabel.prototype.setHighlight = function(val){
  this.highlight = val;
  this.updateBGColor();
  return false;
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigLabel.prototype.saveRaw = function(){
  return {
    zsort: this.zsort,
    
    p: this.p,
    halign: this.halign,
    valign: this.valign,
    
    color: this.color,
    size: this.size,
    
    text: this.text,
    latex: this.latex
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigLabel.loadRaw = function(raw){
  var ret = new JFigLabel();
  ret.zsort = raw.zsort;
  
  ret.p = raw.p;
  ret.halign = raw.halign;
  ret.valign = raw.valign;

  ret.color = raw.color;
  ret.size = raw.size;

  ret.text = raw.text;
  ret.latex = raw.latex;
  
  return ret;
};


//=======================================================
// OUTPUT
// write latex file
//=======================================================

//output latex for label (\put, to be used inside \picture)
//-------------------------------------------------------
JFigLabel.prototype.writeLatex = function(putline){

  //alignment
  //----------------------
  var align = "";
  
  switch(this.halign){
  case JFigLabel.Align.LEFT:  align += "l"; break;
  case JFigLabel.Align.RIGHT: align += "r"; break;
  }
  
  switch(this.valign){
  case JFigLabel.Align.TOP:    align += "t"; break;
  case JFigLabel.Align.BOTTOM: align += "b"; break;
  }
  
  if(align.length > 0){ align = "[" + align + "]"; }
  //----------------------
  
  putline("  \\put(" + this.p[0] + "," + this.p[1] + "){\\makebox(0,0)" + align + "{%");
  putline("    \\fontsize{" + this.size + "pt}{14.4pt}\\selectfont%");
  putline("    \\color[rgb]{" + FigTools.RGBto01(this.color, ",") + "}%");
  putline("    " + (this.latex ? "$" + this.text + "$" : this.text) + "%");
  putline("  }}%");
};


//=======================================================
// HTML
// create/modify HTML object(s)
//=======================================================

//add to, or move to, a target container
//-------------------------------------------------------
JFigLabel.prototype.appendHTML = function($target, figGeom){

  $target.append(this.$label);
  this.updateText(figGeom);
};

//remove from current container
//-------------------------------------------------------
JFigLabel.prototype.removeHTML = function(){
  this.$label.remove();
};

//call after changing text/latex property, or after adding
//-------------------------------------------------------
JFigLabel.prototype.updateText = function(figGeom){

  this.$label.html(this.latex ? "\\(" + this.text + "\\)" : this.text);
  this.updateCSS(figGeom);
  
  if(this.latex){
    var l = this;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.$label[0]]);
    MathJax.Hub.Queue(function(){ l.updateCSS(figGeom); });
  }
};

//call to update labels' display properties (e.g. size, position, color, alignment)
//-------------------------------------------------------
JFigLabel.prototype.updateCSS = function(figGeom){

  //size and color
  this.$label.css({
    "font-size": (this.size * figGeom.zoom) + "pt",
    "color": FigTools.RGBtoHEX(this.color)
  });
  
  //alignment
  //----------------------
  var cposlt = figGeom.FIGtoCTX(this.p);
  var cposbr = figGeom.FIGtoCTXrb(this.p);
  
  switch(this.halign){
  case JFigLabel.Align.LEFT:
    this.$label.css({"left": cposlt[0] + "px", "right": ""}); break;
  case JFigLabel.Align.RIGHT: 
    this.$label.css({"left": "", "right": cposbr[0] + "px"}); break;
  default:
    var w = this.$label.width();
    this.$label.css({"left": Math.round(cposlt[0]-w/2) + "px", "right": ""});
  }
  
  switch(this.valign){
  case JFigLabel.Align.TOP: 
    this.$label.css({"top": cposlt[1] + "px", "bottom": ""}); break;
  case JFigLabel.Align.BOTTOM: 
    this.$label.css({"top": "", "bottom": cposbr[1] + "px"}); break;
  default:
    var h = this.$label.height();
    this.$label.css({"top": Math.round(cposlt[1]-h/2) + "px", "bottom": ""});
  }
  //----------------------
};


//=======================================================
// UI
//=======================================================

JFigLabel.caption = "Label";
JFigLabel.icon = "image/object-label.png";
JFigLabel.prototype.caption = JFigLabel.caption;
JFigLabel.prototype.icon = JFigLabel.icon;

//objects
//-------------------------------------------------------
JFigLabel.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: JFigLabel.icon, caption: JFigLabel.caption, object: this }];
};

//anchors
//-------------------------------------------------------
JFigLabel.prototype.getAnchors = function(figGeom, UIMethods){
  var me = this;
  
  var moveAnchor = new UIAnchor("image/arrow-move.png", this.p);
  moveAnchor.onDragEnd = function(p){
    me.p = p;
    me.updateCSS(figGeom);
    UIMethods.changedLabels();
    
    if(typeof(me.$posTextX) !== "undefined"){ me.$posTextX.val(FigTools.PTtoIN(p[0])) }
    if(typeof(me.$posTextY) !== "undefined"){ me.$posTextY.val(FigTools.PTtoIN(p[1])) }
  };
};

//options
//-------------------------------------------------------
JFigLabel.prototype.setOptionsHTML = function($target, figGeom, UIMethods){

  var l = this;
  var $table, $subtitleExtra;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigLabel.icon + "> LABEL"));
  
  //TEXT
  //------------------------
  $subtitleExtra = $("<span>");
  $target.append($("<div class=panel-subtitle>").append("Text", $subtitleExtra));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  FormTools.createCheckbox($subtitleExtra, "<img src=image/object-label-latex.png>", l.latex, function(val){
    l.latex = val;
    l.updateText(figGeom);
    UIMethods.changedLabels();
  });
  
  var $text = $("<input>", {"type": "text", "value": l.text}).on("input", function(e){
    l.text = $(this).val();
    l.updateText(figGeom);
    UIMethods.changedLabels();
  });
  
  $table.append($("<tr>").append( $("<td colspan=2>").append($text) ));  
    
  //POSITION
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Position"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //text input (store refs--anchors may modify)
  this.$posTextX = $("<input>", { "type": "text", "width": "40px", "value": FigTools.PTtoIN(l.p[0]) });
  this.$posTextX.change(function(e){
    l.p[0] = FigTools.INtoPT($(this).val());
    l.updateCSS(figGeom);
    UIMethods.changedLabels();
  });
  
  this.$posTextY = $("<input>", { "type": "text", "width": "40px", "value": FigTools.PTtoIN(l.p[1]) });  
  this.$posTextY.change(function(e){
    l.p[1] = FigTools.INtoPT($(this).val());
    l.updateCSS(figGeom);
    UIMethods.changedLabels();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("(x,y):"), 
    $("<td align=right>").append(this.$posTextX).append("in, ").append(this.$posTextY).append("in")
  ));
  
  //ALIGNMENT
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Alignment"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //vertical
  var $valign = FormTools.createRadioGroup($("<td align=right>"), "valign",
    { caption: "<img src=image/valign-top.png>", selected: (l.valign == JFigLabel.Align.TOP),
      onSelect: function(){
        l.valign = JFigLabel.Align.TOP;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }},
    { caption: "<img src=image/valign-center.png>", selected: (l.valign == JFigLabel.Align.CENTER),
      onSelect: function(){
        l.valign = JFigLabel.Align.CENTER;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }},
    { caption: "<img src=image/valign-bottom.png>", selected: (l.valign == JFigLabel.Align.BOTTOM), 
      onSelect: function(){
        l.valign = JFigLabel.Align.BOTTOM;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }}
  );
  $table.append($("<tr>").append( $("<td>").append("Vertical:"), $valign ));
  
  //horizontal
  var $halign = FormTools.createRadioGroup($("<td align=right>"), "halign",
    { caption: "<img src=image/halign-left.png>", selected: (l.halign == JFigLabel.Align.LEFT), 
      onSelect: function(){
        l.halign = JFigLabel.Align.LEFT;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }},
    { caption: "<img src=image/halign-center.png>", selected: (l.halign == JFigLabel.Align.CENTER), 
      onSelect: function(){
        l.halign = JFigLabel.Align.CENTER;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }},
    { caption: "<img src=image/halign-right.png>", selected: (l.halign == JFigLabel.Align.RIGHT), 
      onSelect: function(){
        l.halign = JFigLabel.Align.RIGHT;
        l.updateCSS(figGeom);
        UIMethods.changedLabels();
      }}
  );
  $table.append($("<tr>").append( $("<td>").append("Horizontal:"), $halign ));
    
  //SIZE AND COLOR
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Color"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $colorContainer = $("<td colspan=2>");
  FormTools.createColorSelect($colorContainer, this.color, function(rgb){
    l.color = rgb;
    l.updateCSS(figGeom);
    UIMethods.changedLabels();
  });
  $table.append($("<tr>").append( $colorContainer ));
};

