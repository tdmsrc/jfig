//JFigCompound base uses these (OPTIONAL) properties of labels/objects/compounds:
//-------------------------------------------------------
//  isChild = bool; (if true, ignores in saveRaw() and getObjectList()--handled by parent)

//Instructions for creating a compound object:
//-------------------------------------------------------
//  Custom.prototype = new JFigCompound();
//  Custom.prototype.constructor = Custom;
//  function Custom(){ };
//  
//  Custom.prototype.saveRaw(){ }; (save any data necessary to recreate instance to JS obj)
//  Custom.prototype.loadRaw(raw){ }; (new instance from saveRaw output)
//  
//  Custom.prototype.caption = "Thing";
//  Custom.prototype.icon = "image/custom_icon.png";
//  
//  Custom.prototype.getObjectList() (list of objects to show in UI; see UIObjectList)
//  Custom.prototype.getCustomToolbarButtons(UIMethods){ }; (OPTIONAL: see UIToolbar "buttons")
//  Custom.prototype.getAnchors(figGeom, UIMethods){ }; (OPTIONAL: create UIAnchor objects)
//  Custom.prototype.setOptionsHTML($target, figGeom, UIMethods){ }; (OPTIONAL)
//  
//  internally: use clear/clearX/removeX/addX [X = Label(s), Object(s), Image(s), Compound(s)]
//  remember to save/load "zsort" property

//=======================================================
// JFIG COMPOUND OBJECT TEMPLATE
//=======================================================

function JFigCompound(){
  this.zsort = 0;
  
  //parts
  this.labels = [];
  this.images = [];
  this.objects = [];
  this.compounds = [];
  
  //synchronized properties for parts
  this.mouseover = false;
  this.highlight = false;
};


//=======================================================
// COMMON METHODS
//=======================================================

//translate position (x,y units: PT)
//-------------------------------------------------------
JFigCompound.prototype.translate = function(x,y){

  //translate all parts
  $.each(this.labels, function(i,L){ L.translate(x,y); });
  $.each(this.images, function(i,D){ D.translate(x,y); });
  $.each(this.objects, function(i,O){ O.translate(x,y); });
  $.each(this.compounds, function(i,C){ C.translate(x,y); });
};

//picking (pctx is point on context); return true if anything is picked
//-------------------------------------------------------
JFigCompound.prototype.pick = function(context, figGeom, pctx){

  var pickedComponent = this.pickSplit(context, figGeom, pctx);
  return (typeof(pickedComponent) !== "undefined");
};

//picking: Return picked JFigLabel, EPS-type object, or compound; undefined if none
//-------------------------------------------------------
JFigCompound.prototype.pickSplit = function(context, figGeom, pctx){
  var picked = false;
  var pickedComponent;
    
  //labels
  //----------------------------
  $.each(this.labels, function(i,L){ 
    pickedComponent = L;
    picked = L.pick(figGeom, pctx);
    return !picked; //break if picked, otherwise continue
  });
  if(picked){ return pickedComponent; }
  
  //objects and compounds
  //----------------------------
  var queue = [];
  $.each(this.objects, function(i,O){ queue.push(O); });
  $.each(this.compounds, function(i,C){ queue.push(C); });
  
  //sort by "zsort"
  queue.sort(function(x,y){ return y.zsort-x.zsort; });
  
  //pick
  $.each(queue, function(i,X){ 
    pickedComponent = X; 
    picked = X.pick(context, figGeom, pctx);
    return !picked; //break if picked, otherwise continue
  });
  if(picked){ return pickedComponent; }  
  
  //images
  //----------------------------
  $.each(this.images, function(i,D){ 
    pickedComponent = D;
    picked = D.pick(figGeom, pctx);
    return !picked; //break if picked, otherwise continue
  });
  if(picked){ return pickedComponent; }
  
  return;
};

//propagate; return true if an EPS sub-object changed (redraw)
//-------------------------------------------------------
JFigCompound.prototype.setMouseover = function(val){
  var redraw = false;  
  this.mouseover = val;
  
  //labels and images
  $.each(this.labels, function(i,L){ L.setMouseover(val); });
  $.each(this.images, function(i,D){ D.setMouseover(val); });
  
  //objects (check for EPS change)
  $.each(this.objects, function(i,O){ 
    if(O.setMouseover(val)){ redraw = true; }
  });
  
  //compounds (check for EPS change)
  $.each(this.compounds, function(i,C){ 
    if(C.setMouseover(val)){ redraw = true; }
  });
  
  //return info
  return redraw;
};

//propagate; return true if an EPS sub-object changed (redraw)
//-------------------------------------------------------
JFigCompound.prototype.setHighlight = function(val){
  var redraw = false;
  this.highlight = val;
  
  //labels and images
  $.each(this.labels, function(i,L){ L.setHighlight(val); });
  $.each(this.images, function(i,D){ D.setHighlight(val); });
  
  //objects (check for EPS change)
  $.each(this.objects, function(i,O){ 
    if(O.setHighlight(val)){ redraw = true; }
  });
  
  //compounds (check for EPS change)
  $.each(this.compounds, function(i,C){ 
    if(C.setHighlight(val)){ redraw = true; }
  });
  
  //return info
  return redraw;
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigCompound.prototype.saveRaw = function(){
  
  var labelsRaw = [];
  $.each(this.labels, function(i,L){ 
    if(L.isChild){ return true; } //continue
    labelsRaw.push(L.saveRaw()); 
  });
  
  var imagesRaw = [];
  $.each(this.images, function(i,D){
    if(D.isChild){ return true; } //continue
    imagesRaw.push(D.saveRaw());
  });
  
  var objectsRaw = [];
  $.each(this.objects, function(i,O){
    if(O.isChild){ return true; } //continue
    objectsRaw.push(O.saveRaw()); 
  });
  
  var compoundsRaw = [];
  $.each(this.compounds, function(i,C){ 
    if(C.isChild){ return true; } //continue
    compoundsRaw.push(C.saveRaw()); 
  });
  
  return {
    zsort: this.zsort,
    labels: labelsRaw,
    images: imagesRaw,
    objects: objectsRaw,
    compounds: compoundsRaw
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigCompound.loadRaw = function(raw){
  var ret = new JFigCompound();
  ret.zsort = raw.zsort;
  
  $.each(raw.labels, function(i,L){
    ret.labels.push(JFigLabel.loadRaw(L));
  });
  
  //load according to "type", or load generic if "type" not found
  $.each(raw.images, function(i,D){
    var loadFunc;
    if(D.type !== undefined){ loadFunc = window[D.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){    
      ret.images.push(loadFunc(D));
    }else{
      ret.images.push(JFigImage.loadRaw(D));
    }
  });
  
  //load according to "type", or error if "type" not found
  $.each(raw.objects, function(i,O){
    var loadFunc;
    if(O.type !== undefined){ loadFunc = window[O.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      ret.objects.push(loadFunc(O));
    }else{
      console.log("EPS object has unknown type: ");
      console.log(O);
    }
  });
  
  //load according to "type", or load generic if "type" not found
  $.each(raw.compounds, function(i,C){
    var loadFunc;
    if(C.type !== undefined){ loadFunc = window[C.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      ret.compounds.push(loadFunc(C));
    }else{
      ret.compounds.push(JFigCompound.loadRaw(C));
    }
  });
  
  return ret;
};


//=======================================================
// MODIFY SUB-OBJECTS
//=======================================================

//clear parts (handles $label removal, no other UI action)
//-------------------------------------------------------
JFigCompound.prototype.clear = function(){
  this.clearLabels();
  this.clearImages();
  this.clearObjects();
  this.clearCompounds();
};

JFigCompound.prototype.clearObjects = function(){ 
  this.objects = []; 
};

JFigCompound.prototype.clearLabels = function(){

  //remove from current container and reset array
  $.each(this.labels, function(i,L){ L.removeHTML(); });
  this.labels = [];
};

JFigCompound.prototype.clearImages = function(){
  
  //remove from current container and reset array
  $.each(this.images, function(i,D){ D.removeHTML(); });
  this.images = [];
};

JFigCompound.prototype.clearCompounds = function(){ 

  //recursively remove compounds' labels from container and reset array
  $.each(this.compounds, function(i,C){ C.removeHTML(); });
  this.compounds = [];
};

//remove parts (handles $label removal, no other UI action)
//-------------------------------------------------------
JFigCompound.prototype.removeObject = function(O){

  var i = $.inArray(O, this.objects);
  if(i > -1){ this.objects.splice(i,1); }
};

JFigCompound.prototype.removeLabel = function(L){

  var i = $.inArray(L, this.labels);
  if(i > -1){ L.removeHTML(); this.labels.splice(i,1); }
};

JFigCompound.prototype.removeImage = function(D){
  
  var i = $.inArray(D, this.images);
  if(i > -1){ D.removeHTML(); this.images.splice(i,1); }
};

JFigCompound.prototype.removeCompound = function(C){

  var i = $.inArray(C, this.compounds);
  if(i > -1){ C.removeHTML(); this.compounds.splice(i,1); }
};

//add parts (handles appending $label, no other UI action)
//-------------------------------------------------------
JFigCompound.prototype.addObject = function(O){
  this.objects.push(O);
  
  O.setHighlight(this.highlight);
  O.setMouseover(this.mouseover);
};

JFigCompound.prototype.addLabel = function(L, figGeom){
  this.labels.push(L);
  
  L.setHighlight(this.highlight);
  L.setMouseover(this.mouseover);
  
  //add to current container, if there is one
  if(typeof(this.$container) !== "undefined"){
    L.appendHTML(this.$container, figGeom);
  }
};

JFigCompound.prototype.addImage = function(D, figGeom){
  this.images.push(D);
  
  D.setHighlight(this.highlight);
  D.setMouseover(this.mouseover);
  
  //add to current container, if there is one
  if(typeof(this.$container) !== "undefined"){
    D.appendHTML(this.$container, figGeom);
  }
};

JFigCompound.prototype.addCompound = function(C, figGeom){
  this.compounds.push(C);
  
  C.setHighlight(this.highlight);
  C.setMouseover(this.mouseover);
  
  //add to current container, if there is one
  if(typeof(this.$container) !== "undefined"){
    C.appendHTML(this.$container, figGeom);
  }
};


//=======================================================
// EPS OUTPUT/PREVIEW
//=======================================================

//get EPS code for drawing
//-------------------------------------------------------
JFigCompound.prototype.writeEPS = function(putline){

  //objects and compounds to process
  var queue = [];
  $.each(this.objects, function(i,O){ queue.push(O); });
  $.each(this.compounds, function(i,C){ queue.push(C); });
  
  //sort by "zsort"
  queue.sort(function(x,y){ return x.zsort-y.zsort; });
  
  //write
  $.each(queue, function(i,X){ X.writeEPS(putline); });
};

//draw onto given context
//-------------------------------------------------------
JFigCompound.prototype.draw = function(context, figGeom){

  //objects and compounds to process
  var queue = [];
  $.each(this.objects, function(i,O){ queue.push(O); });
  $.each(this.compounds, function(i,C){ queue.push(C); });
  
  //sort by "zsort"
  queue.sort(function(x,y){ return x.zsort-y.zsort; });
  
  //write
  $.each(queue, function(i,X){ X.draw(context, figGeom); });
};


//=======================================================
// LABELS OUTPUT
//=======================================================

//output latex for labels/compounds
//-------------------------------------------------------
JFigCompound.prototype.writeLatex = function(putline){

  //labels and compounds
  $.each(this.labels, function(i,L){ L.writeLatex(putline); });
  $.each(this.compounds, function(i,C){ C.writeLatex(putline); });
};


//=======================================================
// LABELS/IMAGES HTML UPDATE
//=======================================================

//add labels to, or move labels to, a target container
//-------------------------------------------------------
JFigCompound.prototype.appendHTML = function($target, figGeom){

  //store target
  this.$container = $target;
  
  $.each(this.labels, function(i,L){ L.appendHTML($target, figGeom); });
  $.each(this.images, function(i,D){ D.appendHTML($target, figGeom); });
  $.each(this.compounds, function(i,C){ C.appendHTML($target, figGeom); });
};

//remove labels from current container
//-------------------------------------------------------
JFigCompound.prototype.removeHTML = function(){
  
  $.each(this.labels, function(i,L){ L.removeHTML(); });
  $.each(this.images, function(i,D){ D.removeHTML(); });
  $.each(this.compounds, function(i,C){ C.removeHTML(); });
};

//call to update labels' display properties (e.g. size, position, color, alignment)
//-------------------------------------------------------
JFigCompound.prototype.updateCSS = function(figGeom){

  $.each(this.labels, function(i,L){ L.updateCSS(figGeom); });
  $.each(this.images, function(i,D){ D.updateCSS(figGeom); });
  $.each(this.compounds, function(i,C){ C.updateCSS(figGeom); });
};


//=======================================================
// UI
//=======================================================

//object list
//-------------------------------------------------------
JFigCompound.prototype.getObjectList = function(indent){
  //generic version: no entry for compound, but one for each part
  var ret = [];

  //labels
  $.each(this.labels, function(i,L){ 
    if(L.isChild){ return true; } //continue
    ret = ret.concat(L.getObjectList(indent));
  });
  
  //images
  $.each(this.images, function(i,D){
    if(D.isChild){ return true; } //continue
    ret = ret.concat(D.getObjectList(indent));
  });
  
  //objects (check for EPS change)
  $.each(this.objects, function(i,O){ 
    if(O.isChild){ return true; } //continue
    ret = ret.concat(O.getObjectList(indent));
  });
  
  //compounds (check for EPS change)
  $.each(this.compounds, function(i,C){ 
    if(C.isChild){ return true; } //continue
    ret = ret.concat(C.getObjectList(indent));
  });
  
  return ret;
};

