//defines EPSPath
//see jfigEPS.js for info on EPS-type objects

//=======================================================
// EPS PATH
// polyline which may be closed/filled
//=======================================================

EPSPath.prototype = new EPSPathGeneric();
EPSPath.prototype.constructor = EPSPath;

function EPSPath(){  
  //set default values
  this.points = []; //units: PT
  
  this.smoothed = false;
  this.smoothAmount = 1; //between 0 and 1
  
  this.init();
};

//inserting/deleting points
//-------------------------------------------------------
//insert p (units: PT) before index j; if j>=n, append
EPSPath.prototype.insertPoint = function(j, p){
  if(j >= this.points.length){ 
    this.points.push(p);
  }else{
    this.points.splice(j,0, p);
  }
};

//delete point at index i; do nothing if there are 2 or fewer points
EPSPath.prototype.deletePoint = function(i){
  if(this.points.length < 3){ return false; }
  
  this.points.splice(i,1);
  return true;
};

//translate position (x,y units: PT)
//-------------------------------------------------------
EPSPath.prototype.translate = function(x,y){

  for(var i=0, il=this.points.length; i<il; i++){
    this.points[i][0] += x;
    this.points[i][1] += y;
  }
};

//picking (pctx is point on context); return true if picked
//-------------------------------------------------------
EPSPath.prototype.pick = function(context, figGeom, pctx){

  //create path
  this.definePath(context, figGeom);
  
  if(this.closed && this.filled){ 
    this.pen.drawFill(context);
  }
  
  //set up stroke, but don't draw
  this.pen.drawHighlight(context, figGeom);
  
  //use isPointInPath
  var hit = context.isPointInStroke(pctx[0], pctx[1]);
  if(this.closed && this.filled){
    hit = hit || context.isPointInPath(pctx[0], pctx[1]);
  }
  return hit;
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
EPSPath.prototype.saveRaw = function(){
  return {
    type: "EPSPath",
    zsort: this.zsort,
    
    points: this.points,
    pen: this.pen.saveRaw(),
    
    closed: this.closed,
    filled: this.filled,
    
    smoothed: this.smoothed,
    smoothAmount: this.smoothAmount,

    capStart: this.capStart.saveRaw(),
    capEnd: this.capEnd.saveRaw()
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
EPSPath.loadRaw = function(raw){
  var ret = new EPSPath();
  ret.zsort = raw.zsort;
  
  ret.points = raw.points;
  ret.pen = EPSPen.loadRaw(raw.pen);
  
  ret.closed = raw.closed;
  ret.filled = raw.filled;
  
  ret.smoothed = raw.smoothed;
  ret.smoothAmount = raw.smoothAmount;

  ret.capStart = EPSPathCap.loadRaw(raw.capStart);
  ret.capEnd = EPSPathCap.loadRaw(raw.capEnd);
  
  return ret;
};


//=======================================================
// SMOOTHING
//=======================================================

//normalize v then scale by len
//-------------------------------------------------------
EPSPath.setlen = function(v, len){
  var s = len / Math.sqrt(v[0]*v[0]+v[1]*v[1]);
  return [ s*v[0], s*v[1] ];
};

//get Bezier control point for path smoothing (direction ? forward : backward)
//-------------------------------------------------------
EPSPath.prototype.getBezierControl = function(i, direction){

  //if <= 2 points, degenerate control points
  var n = this.points.length;
  if(n <= 2){ return this.points[i]; }
  
  //starting point of non-closed
  if(!this.closed && (i == 0)){
    //only forward direction makes sense
    if(!direction){ return; } 
    
    //segment from q to (r's backward control point), scaled to len(qr)
    var q = this.points[0]; //current point
    var r = this.points[1]; //next point
    
    var qr = [ r[0]-q[0], r[1]-q[1] ];
    var tlen = this.smoothAmount * 0.3 * Math.sqrt(qr[0]*qr[0] + qr[1]*qr[1]);
    
    var c = this.getBezierControl(1, false); 
    var qc = [ c[0]-q[0], c[1]-q[1] ];
    
    //return q + tangent
    var t = EPSPath.setlen(qc, tlen);
    return [ q[0]+t[0], q[1]+t[1] ];
  }
  
  //ending point of non-closed
  else if(!this.closed && (i == n-1)){
    //only backward direction makes sense
    if(direction){ return; } 
    
    //segment from (p's forward control point) to q, scaled to -len(pq)
    var p = this.points[n-2]; //previous point
    var q = this.points[n-1]; //current point
    
    var pq = [ q[0]-p[0], q[1]-p[1] ];
    var tlen = -this.smoothAmount * 0.3 * Math.sqrt(pq[0]*pq[0] + pq[1]*pq[1]);
    
    var c = this.getBezierControl(n-2, true);
    var cq = [ q[0]-c[0], q[1]-c[1] ];
    
    //return q + tangent
    var t = EPSPath.setlen(cq, tlen);
    return [ q[0]+t[0], q[1]+t[1] ];
  }
  
  //any point with both a previous (p) and next (r) point
  else{
    var p = this.points[(i==0) ? n-1 : i-1]; 
    var q = this.points[i];
    var r = this.points[(i==n-1) ? 0 : i+1];
    
    var pq = [ q[0]-p[0], q[1]-p[1] ];
    var qr = [ r[0]-q[0], r[1]-q[1] ];
    
    //tangent is normalize(pq)+normalize(qr), scaled to length = tlen
    var tlen = this.smoothAmount * 0.3 * (direction ? 
       Math.sqrt(qr[0]*qr[0] + qr[1]*qr[1]) : //if forward, scale to len(qr)
      -Math.sqrt(pq[0]*pq[0] + pq[1]*pq[1]) ); //if back, flip and scale to len(pq)
    
    //return q + tangent
    var pqn = EPSPath.setlen(pq, 1);
    var qrn = EPSPath.setlen(qr, 1);
    
    var t = EPSPath.setlen([pqn[0]+qrn[0], pqn[1]+qrn[1]], tlen);
    return [ q[0]+t[0], q[1]+t[1] ];
  }
};

//Bezier parameterization (t \in [0,1])
//-------------------------------------------------------
EPSPath.bezier = function(p, cp, cq, q, t){
  return [
    (1-t)*(1-t)*(1-t)*p[0] + 3*(1-t)*(1-t)*t*cp[0] + 3*(1-t)*t*t*cq[0] + t*t*t*q[0],
    (1-t)*(1-t)*(1-t)*p[1] + 3*(1-t)*(1-t)*t*cp[1] + 3*(1-t)*t*t*cq[1] + t*t*t*q[1]
  ];
};

//start at p; trace Bezier curve forwards; return first X w/ d(p,X) > d (else q)
//-------------------------------------------------------
EPSPath.bezierStart = function(p, cp, cq, q, d){
  var n = 15;

  for(var i=1; i<n; i++){
    var b = EPSPath.bezier(p,cp,cq,q, i/n);
    var pb = [b[0]-p[0], b[1]-p[1]];
    if(Math.sqrt(pb[0]*pb[0] + pb[1]*pb[1]) > d){ return b; }
  }
  return q;
};

//start at q; trace Bezier curve backwards; return first X w/ d(q,X) > d (else p)
//-------------------------------------------------------
EPSPath.bezierEnd = function(p, cp, cq, q, d){
  var n = 15;

  for(var i=1; i<n; i++){
    var b = EPSPath.bezier(p,cp,cq,q, 1-i/n);
    var bq = [q[0]-b[0], q[1]-b[1]];
    if(Math.sqrt(bq[0]*bq[0] + bq[1]*bq[1]) > d){ return b; }
  }
  return p;
};

//get vector XP where P = initial, X = first along path w/ d(X,P) > d
//-------------------------------------------------------
EPSPath.prototype.getStartDisplacement = function(d){
  var n = this.points.length;
  
  //indices making up initial edge
  var i=0, j=1;
  
  if(!this.smoothed || (this.points.length <= 2)){
  
    //endpoints of initial edge pq
    var p = this.points[i];
    var q = this.points[j];
    
    //vector from q to p
    return EPSPath.setlen([p[0]-q[0], p[1]-q[1]], 1);
  
  }else{
    
    //endpoints and control points for initial edge
    var p = this.points[i], cp = this.getBezierControl(i, true);
    var q = this.points[j], cq = this.getBezierControl(j, false);
    
    //first point on Bezier curve at correct dist from p
    var b = EPSPath.bezierStart(p,cp,cq,q, d);
    return EPSPath.setlen([p[0]-b[0], p[1]-b[1]], 1);
  }
};

//get vector XQ where Q = final, X = first along reverse path w/ d(X,Q) > d
//-------------------------------------------------------
EPSPath.prototype.getEndDisplacement = function(d){
  var n = this.points.length;
  
  //indices making up final edge
  var i=n-2, j=n-1; if(this.closed){ i=n-1; j=0; }
      
  if(!this.smoothed || (this.points.length <= 2)){
  
    //endpoints of final edge pq
    var p = this.points[i];
    var q = this.points[j];
    
    //vector from p to q
    return EPSPath.setlen([q[0]-p[0], q[1]-p[1]], 1);
  
  }else{
    
    //endpoints and control points for final edge
    var p = this.points[i], cp = this.getBezierControl(i, true);
    var q = this.points[j], cq = this.getBezierControl(j, false);
    
    //first point on Bezier curve at correct dist from q
    var b = EPSPath.bezierEnd(p,cp,cq,q, d);
    return EPSPath.setlen([q[0]-b[0], q[1]-b[1]], 1);
  }
};


//=======================================================
// CAPS
//=======================================================

//return (start ? initial : final) point along path
//-------------------------------------------------------
EPSPath.prototype.capTip = function(start){
  return start ? 
    this.points[0] : 
    this.points[this.closed ? 0 : (this.points.length-1)];
};

//return direction of cap at (start ? initial : final) point along path
//-------------------------------------------------------
EPSPath.prototype.capDirection = function(start){
  return start ?
    this.getStartDisplacement(this.capStart.size) : 
    this.getEndDisplacement(this.capEnd.size);
};


//=======================================================
// EPS OUTPUT
//=======================================================

//define the path (including begin and close); no drawing or fill
//-------------------------------------------------------
EPSPath.prototype.definePathEPS = function(putline){
  
  //create path
  putline(EPSMacro.NEWPATH.get);
  
  if(!this.smoothed || (this.points.length <= 2)){
    
    //NON-SMOOTHED PATH
    //--------------------
    for(var i=0, il=this.points.length; i<il; i++){
      var p = this.points[i];
      putline(p[0] + " " + p[1] + " " + ((i==0) ? EPSMacro.MOVETO.get : EPSMacro.LINETO.get));
    }
    
    if(this.closed){ putline(EPSMacro.CLOSEPATH.get); }
    //--------------------
    
  }else{
    
    //SMOOTHED PATH
    //--------------------
    //initial point
    var p = this.points[0];
    putline(p[0] + " " + p[1] + " " + EPSMacro.MOVETO.get);
    
    //curve back to initial point if closed
    var n = this.points.length;
    for(var i=1, il=(this.closed ? n+1 : n); i<il; i++){
      
      //control points for previous (p) and current (q) points
      var cp = this.getBezierControl(i-1, true);
      var cq = this.getBezierControl(i%n, false);
      
      //curve to current point
      p = this.points[i%n];
      putline(cp[0] + " " + cp[1] + " " + cq[0] + " " + cq[1] + " " + p[0] + " " + p[1] + " " + EPSMacro.CURVETO.get);
    }
    //--------------------
  }
};


//=======================================================
// DRAW ONTO CANVAS
//=======================================================

//define the path (including begin and close); no drawing or fill
//-------------------------------------------------------
EPSPath.prototype.definePath = function(context, figGeom){

  //create path
  context.beginPath();
  
  if(!this.smoothed || (this.points.length <= 2)){
  
    //NON-SMOOTHED PATH
    //--------------------
    for(var i=0, il=this.points.length; i<il; i++){
      var p = figGeom.FIGtoCTX(this.points[i]);
      
      if(i==0){ context.moveTo(p[0],p[1]); }
      else{ context.lineTo(p[0],p[1]); }
    }
    
    if(this.closed){ context.closePath(); }
    //--------------------
    
  }else{
  
    //SMOOTHED PATH
    //--------------------
    //initial point
    var pctx = figGeom.FIGtoCTX(this.points[0]);
    context.moveTo(pctx[0], pctx[1]);
    
    //curve back to initial point if closed
    var n = this.points.length;
    for(var i=1, il=(this.closed ? n+1 : n); i<il; i++){
      
      //control points for previous (p) and current (q) points
      var cp = figGeom.FIGtoCTX(this.getBezierControl(i-1, true));
      var cq = figGeom.FIGtoCTX(this.getBezierControl(i%n, false));
      
      //curve to current point
      pctx = figGeom.FIGtoCTX(this.points[i%n]);
      context.bezierCurveTo(cp[0], cp[1], cq[0], cq[1], pctx[0], pctx[1]);
    }
    //--------------------
  }
};


//=======================================================
// UI
//=======================================================

EPSPath.caption = "Path";
EPSPath.icon = "image/object-path.png";
EPSPath.prototype.caption = EPSPath.caption;
EPSPath.prototype.icon = EPSPath.icon;

//objects
//-------------------------------------------------------
EPSPath.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: EPSPath.icon, caption: EPSPath.caption, object: this }];
};

//toolbar
//-------------------------------------------------------
EPSPath.AnchorMode = Object.freeze({
  MOVE:   0,
  ADD:    1,
  DELETE: 2,
});
EPSPath.prototype.anchorMode = EPSPath.AnchorMode.MOVE;

EPSPath.prototype.getCustomToolbarButtons = function(UIMethods){
  var me = this;
  
  return [
    { caption: "Move Points", icon: "image/point-toolbar-move.png", action: function(){
      me.anchorMode = EPSPath.AnchorMode.MOVE;
      UIMethods.refreshAnchors(me);
    }},
    
    { caption: "Add Points", icon: "image/point-toolbar-add.png", action: function(){
      me.anchorMode = EPSPath.AnchorMode.ADD;
      UIMethods.refreshAnchors(me);
    }},
    
    { caption: "Delete Points", icon: "image/point-toolbar-delete.png", action: function(){
      me.anchorMode = EPSPath.AnchorMode.DELETE;
      UIMethods.refreshAnchors(me);
    }}
  ];
};

//anchors
//-------------------------------------------------------
EPSPath.prototype.getAnchorsMove = function(figGeom, UIMethods){
  var me = this;
  
  //create "move" anchor at each vertex
  for(var i=0, il=this.points.length; i<il; i++){
    //create anchor
    var anchor = new UIAnchor("image/point-anchor-move.png", this.points[i]);
    (function(anchor, i){
      anchor.onDragEnd = function(p){
        me.points[i] = p;
        UIMethods.changedEPS();
      };
    })(anchor,i);
  }
};

EPSPath.prototype.getAnchorsAdd = function(figGeom, UIMethods){
  var me = this;
  
  var addCommon = function(){
    UIMethods.changedEPS();
    UIMethods.refreshAnchors(me);
  };
  var n = this.points.length;
  
  //create "add" anchor on each edge
  var imax = (this.closed ? n : (n-1));
  for(var i=0, il=imax; i<il; i++){
    var j = (i+1)%n;
    var p = this.points[i];
    var q = this.points[j];
    
    //find midpoint: depends on smoothed property
    var m = [0.5*(p[0]+q[0]), 0.5*(p[1]+q[1])];
    if(this.smoothed){
      var cp = this.getBezierControl(i, true);
      var cq = this.getBezierControl(j, false);
      m = EPSPath.bezier(p, cp, cq, q, 0.5);
    }
    
    //create anchor
    var anchor = new UIAnchor("image/point-anchor-add.png", m);
    (function(anchor, j, m){
      var k = ((j==0) ? n : j); //avoid changing path origin when closed
      anchor.onClick = function(){
        me.insertPoint(k,m); addCommon();
      };
      anchor.onDragEnd = function(p){
        me.insertPoint(k,p); addCommon();
      };
    })(anchor,j,m);
  }
  
  //if not closed, create "add" anchors on endpoints
  if(!this.closed){
    var anchorStart = new UIAnchor("image/point-anchor-add.png", this.points[0]);
    anchorStart.onDragEnd = function(p){
      me.insertPoint(0,p); addCommon();
    };
    var anchorEnd = new UIAnchor("image/point-anchor-add.png", this.points[n-1]);
    anchorEnd.onDragEnd = function(p){
      me.insertPoint(n,p); addCommon();
    };
  }
};

EPSPath.prototype.getAnchorsDelete = function(figGeom, UIMethods){
  var me = this;
  
  //create "delete" anchor at each vertex
  for(var i=0, il=this.points.length; i<il; i++){
    //create anchor
    var anchor = new UIAnchor("image/point-anchor-delete.png", this.points[i]);
    anchor.draggable = false;
    
    (function(anchor, i){
      anchor.onClick = function(){
        if(!me.deletePoint(i)){ return; }
        UIMethods.changedEPS();
        UIMethods.refreshAnchors(me);
      };
    })(anchor,i);
  }
};

EPSPath.prototype.getAnchors = function(figGeom, UIMethods){
  switch(this.anchorMode){
    case EPSPath.AnchorMode.MOVE:
      this.getAnchorsMove(figGeom, UIMethods);
      break;
    case EPSPath.AnchorMode.ADD: 
      this.getAnchorsAdd(figGeom, UIMethods);
      break;
    case EPSPath.AnchorMode.DELETE:
      this.getAnchorsDelete(figGeom, UIMethods);
      break;
  }
};

//options
//-------------------------------------------------------
EPSPath.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + EPSPath.icon + "> PATH"));
  
  //STYLE
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Style"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $smoothed = FormTools.createCheckbox($("<td>"), "Smooth", o.smoothed, function(val){
    o.smoothed = val;
    UIMethods.changedEPS();
    
    //if EPSPath.AnchorMode.ADD, need to update
    UIMethods.refreshAnchors(o); 
  });
  
  var $closed = FormTools.createCheckbox($("<td>"), "Closed", o.closed, function(val){
    o.closed = val;
    UIMethods.changedEPS();
    
    //if EPSPath.AnchorMode.ADD, need to update
    UIMethods.refreshAnchors(o);
  });
  
  $table.append($("<tr>").append( $closed, $smoothed ));
  
  //GENERIC PATH OPTIONS
  //------------------------
  o.appendOptionsHTML($target, figGeom, UIMethods);
};

