//defines EPSPathGeneric
//see jfigEPS.js for info on EPS-type objects


//REQUIRED for child classes
//-----------------------------------------------------
//call init() at some point in constructor

//return (start ? initial : final) point along path
//EPSPathType.prototype.capTip = function(start){ }
//
//return direction of cap at (start ? initial : final) point along path
//EPSPathType.prototype.capDirection = function(start){ }
//
//define the path (including begin and close); no drawing or fill
//EPSPathType.prototype.definePathEPS = function(putline){ }
//
//define the path (including begin and close); no drawing or fill
//EPSPathType.prototype.definePath = function(context, figGeom){ }
//-----------------------------------------------------


//=======================================================
// EPS PATH (GENERIC)
// any path which may be closed/filled, and which has caps
//=======================================================

function EPSPathGeneric(){
  this.zsort = 0;
  
  this.closed = false;
  this.filled = false;
  
  //general
  this.mouseover = false;
  this.highlight = false;
};

EPSPathGeneric.prototype.init = function(){
  this.pen = new EPSPen();
  
  this.capStart = new EPSPathCap();
  this.capEnd = new EPSPathCap();
};

//return true if changed (redraw)
//-------------------------------------------------------
EPSPathGeneric.prototype.setMouseover = function(val){
  var ret = (this.mouseover != val);
  this.mouseover = val;
  return ret;
};

//return true if changed (redraw)
//-------------------------------------------------------
EPSPathGeneric.prototype.setHighlight = function(val){
  var ret = (this.highlight != val);
  this.highlight = val;
  return ret;
};


//=======================================================
// CAPS
//=======================================================

//append cap bounding box path for (start ? initial : final) point on path
//-------------------------------------------------------
EPSPathGeneric.prototype.capAppendBoundingBox = function(context, figGeom, start){

  var p = this.capTip(start); //cap tip
  var t = this.capDirection(start); //direction
  (start ? this.capStart : this.capEnd).appendBoundingBox(context, figGeom, p, t); 
};

EPSPathGeneric.prototype.capAppendBoundingBoxEPS = function(putline, start){

  var p = this.capTip(start); //cap tip
  var t = this.capDirection(start); //direction
  (start ? this.capStart : this.capEnd).appendBoundingBoxEPS(putline, p, t); 
};

//append cap shape path for (start ? initial : final) point on path
//-------------------------------------------------------
EPSPathGeneric.prototype.capAppendShape = function(context, figGeom, start){

  var p = this.capTip(start); //cap tip
  var t = this.capDirection(start); //direction
  (start ? this.capStart : this.capEnd).appendShape(context, figGeom, p, t);
};

EPSPathGeneric.prototype.capAppendShapeEPS = function(putline, start){

  var p = this.capTip(start); //cap tip
  var t = this.capDirection(start); //direction
  (start ? this.capStart : this.capEnd).appendShapeEPS(putline, p, t);
};


//=======================================================
// EPS OUTPUT
//=======================================================

//get EPS code for drawing path
//-------------------------------------------------------
EPSPathGeneric.prototype.writeEPS = function(putline){

  //STATE
  //--------------------
  var me = this; 
  var clipping = false;
  var pathDefined = false;
  
  var ensurePathDefined = function(){
    if(pathDefined){ return; }
    pathDefined = true; 
    me.definePathEPS(putline); 
  };
  
  //PATH FILL
  //--------------------
  //clip by caps' shapes
  if((this.capStart.enabled && !this.capStart.filled) || (this.capEnd.enabled && !this.capEnd.filled)){
    pathDefined = false;
    clipping = true;
    
    putline(EPSMacro.NEWPATH.get);
    putline("clippath"); //put current clipping path, which is whole canvas
    if(this.capStart.enabled && !this.capStart.filled){ this.capAppendShapeEPS(putline, true ); }
    if(this.capEnd.enabled   && !this.capEnd.filled  ){ this.capAppendShapeEPS(putline, false); }
    
    putline(EPSMacro.CLIP.get);
  }
    
  //fill path
  if(this.closed && this.filled){
    ensurePathDefined();
    this.pen.epsFill(putline);
  }
  
  //undo clipping
  if(clipping){ 
    clipping = false;
    putline(EPSMacro.UNCLIP.get); 
  }
  //--------------------  
  
  //PATH STROKE
  //--------------------
  //clip by caps' bounding boxes
  if(this.capStart.enabled || this.capEnd.enabled){
    pathDefined = false;
    clipping = true;
    
    putline(EPSMacro.NEWPATH.get);
    putline("clippath"); //put current clipping path, which is whole canvas
    if(this.capStart.enabled){ this.capAppendBoundingBoxEPS(putline, true ); }
    if(this.capEnd.enabled  ){ this.capAppendBoundingBoxEPS(putline, false); }
    
    putline(EPSMacro.CLIP.get);
  }
  
  //stroke path
  if(this.pen.stroked){
    ensurePathDefined();
    this.pen.epsStroke(putline);
  }
  
  //undo clipping
  if(clipping){ 
    clipping = false;
    putline(EPSMacro.UNCLIP.get); 
  }
  //--------------------
  
  //DRAW CAPS
  //--------------------
  //function to do drawing after cap path defined
  var drawCap = function(stroked, filled){
    if(filled){
      putline(FigTools.RGBto01(me.pen.strokeColor, " ") + " " + EPSMacro.SETRGBCOLOR.get + " " + EPSMacro.FILL.get);
    }
    if(stroked){ 
      me.pen.epsStroke(putline); 
    }
  };
  
  //start cap
  if(this.capStart.enabled){ 
    //define cap path
    pathDefined = false;
    putline(EPSMacro.NEWPATH.get);
    this.capAppendShapeEPS(putline, true);
    //draw
    drawCap(this.capStart.stroked, this.capStart.filled);
  }
  
  //end cap
  if(this.capEnd.enabled){ 
    //define cap path
    pathDefined = false;
    putline(EPSMacro.NEWPATH.get);
    this.capAppendShapeEPS(putline, false);
    //draw
    drawCap(this.capEnd.stroked, this.capEnd.filled);
  }
  //--------------------
};


//=======================================================
// DRAW ONTO CANVAS
//=======================================================

//draw onto given context
//-------------------------------------------------------
EPSPathGeneric.prototype.draw = function(context, figGeom){
   
  //STATE
  //--------------------
  var me = this;
  var clipping = false;
  var pathDefined = false; 
  
  var ensurePathDefined = function(){
    if(pathDefined){ return; }
    pathDefined = true; 
    me.definePath(context, figGeom); 
  };
  //--------------------
  
  //PATH FILL
  //--------------------
  //clip by caps' shapes
  if((this.capStart.enabled && !this.capStart.filled) || (this.capEnd.enabled && !this.capEnd.filled)){
    pathDefined = false;
    clipping = true;
    context.save();

    context.beginPath();    
    context.rect(0,0,figGeom.canvasSize[0],figGeom.canvasSize[1]);
    if(this.capStart.enabled && !this.capStart.filled){ this.capAppendShape(context, figGeom, true ); }
    if(this.capEnd.enabled   && !this.capEnd.filled  ){ this.capAppendShape(context, figGeom, false); }
    
    context.clip();
  }
  
  //fill path
  if(this.closed && this.filled){
    ensurePathDefined();
    this.pen.drawFill(context);
    context.fill();
  }
  
  //undo clipping
  if(clipping){
    clipping = false;
    context.restore();
  }
  //--------------------
  
  //PATH STROKE
  //--------------------
  //clip by caps' bounding boxes
  if(this.capStart.enabled || this.capEnd.enabled){
    pathDefined = false;
    clipping = true;
    context.save(); 
    
    context.beginPath();
    context.rect(0,0,figGeom.canvasSize[0],figGeom.canvasSize[1]);
    if(this.capStart.enabled){ this.capAppendBoundingBox(context, figGeom, true ); }
    if(this.capEnd.enabled  ){ this.capAppendBoundingBox(context, figGeom, false); }
    
    context.clip();
  }
  
  //stroke path
  if(this.pen.stroked){
    ensurePathDefined();
    this.pen.drawStroke(context, figGeom);
    context.stroke();
  }
  
  //highlight/mouseover
  if(this.highlight || this.mouseover){
    //stroke
    ensurePathDefined();
    this.pen.drawHighlight(context, figGeom);
    context.strokeStyle = FigOptions.COLOR_BG(this.mouseover, this.highlight);
    context.stroke();
  }
  
  //undo clipping
  if(clipping){
    clipping = false;
    context.restore();
  }
  //--------------------
  
  //DRAW CAPS
  //--------------------
  //function to do drawing after cap path defined
  var drawCap = function(stroked, filled){
    //fill/stroke
    if(filled){
      context.fillStyle = FigTools.RGBtoHEX(me.pen.strokeColor);
      context.fill();
    }
    if(stroked){
      me.pen.drawStroke(context, figGeom);
      context.stroke();
    }
    
    //highlight/mouseover
    if(me.highlight || me.mouseover){
      me.pen.drawHighlight(context, figGeom);
      context.strokeStyle = FigOptions.COLOR_BG(me.mouseover, me.highlight);
      context.stroke();
    }
  };
  
  //start cap
  if(this.capStart.enabled){ 
    //define cap path
    pathDefined = false;
    context.beginPath();
    this.capAppendShape(context, figGeom, true);
    //draw
    drawCap(this.capStart.stroked, this.capStart.filled);
  }
  
  //end cap
  if(this.capEnd.enabled){ 
    //define cap path
    pathDefined = false;
    context.beginPath();
    this.capAppendShape(context, figGeom, false); 
    //draw
    drawCap(this.capEnd.stroked, this.capEnd.filled);
  }
  //--------------------
};


//=======================================================
// UI
//=======================================================

//options
//-------------------------------------------------------
EPSPathGeneric.prototype.appendOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  
  //CAPS
  //------------------------
  o.capStart.appendOptionsHTML($target, "Cap Start", figGeom, UIMethods);
  o.capEnd.appendOptionsHTML($target, "Cap End", figGeom, UIMethods);
  
  //PEN
  //------------------------
  o.pen.appendStrokeOptionsHTML($target, "Pen (Stroke)", figGeom, UIMethods);
  o.pen.appendFillOptionsHTML($target, "Pen (Fill)", figGeom, UIMethods, 
    //getFilled
    function(){ return o.filled; }, 
    //setFilled
    function(val){ o.filled = val; }
  );
};
