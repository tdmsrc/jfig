//defines EPSArc
//see jfigEPS.js for info on EPS-type objects

//=======================================================
// EPS ARC
// circle or circle segment which may be filled
//=======================================================

EPSArc.prototype = new EPSPathGeneric();
EPSArc.prototype.constructor = EPSArc;

function EPSArc(){
  
  //set default values
  this.center = [0,0]; //units: PT
  this.radius = 36; //units: PT
  
  this.angleStart = 0; //degrees
  this.angleEnd = 270; //degrees
  
  this.closeType = EPSArc.CloseType.SECTOR;
  
  this.init();
};

//enum for cap type (value irrelevant)
//-------------------------------------------------------
EPSArc.CloseType = Object.freeze({ 
  SEGMENT:  1, //connect arc endpoints by segment
  SECTOR:   2  //connect arc endpoints to center
});

//translate position (x,y units: PT)
//-------------------------------------------------------
EPSArc.prototype.translate = function(x,y){
  this.center[0] += x;
  this.center[1] += y;
};

//picking (pctx is point on context); return true if picked
//-------------------------------------------------------
EPSArc.prototype.pick = function(context, figGeom, pctx){

  //create path
  context.beginPath();
  
  var p = figGeom.FIGtoCTX(this.center);
  context.arc(p[0], p[1], figGeom.PTtoPX(this.radius), -this.angleStart*Math.PI/180, -this.angleEnd*Math.PI/180, true);
  
  if(this.closed){
    if(this.closeType == EPSArc.CloseType.SECTOR){
      context.lineTo(p[0],p[1]);
    }
    context.closePath();
    
    if(this.filled){
      this.pen.drawFill(context);
      context.fill();
    }
  }
  
  //set up stroke, but don't draw
  this.pen.drawHighlight(context, figGeom);
  
  //use isPointInPath
  var hit = this.filled ? context.isPointInPath(pctx[0], pctx[1]) : false;
  hit = hit || context.isPointInStroke(pctx[0], pctx[1]);
  return hit;
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
EPSArc.prototype.saveRaw = function(){
  return {
    type: "EPSArc",
    zsort: this.zsort,
    
    center: this.center,
    radius: this.radius,
    
    angleStart: this.angleStart,
    angleEnd: this.angleEnd,
    
    pen: this.pen.saveRaw(),
    
    closed: this.closed,
    closeType: this.closeType,
    filled: this.filled,
    
    capStart: this.capStart.saveRaw(),
    capEnd: this.capEnd.saveRaw()
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
EPSArc.loadRaw = function(raw){
  var ret = new EPSArc();
  ret.zsort = raw.zsort;
  
  ret.center = raw.center;
  ret.radius = raw.radius;
  
  ret.angleStart = raw.angleStart;
  ret.angleEnd = raw.angleEnd;

  ret.pen = EPSPen.loadRaw(raw.pen);
  
  ret.closed = raw.closed;
  ret.closeType = raw.closeType;
  ret.filled = raw.filled;
  
  ret.capStart = EPSPathCap.loadRaw(raw.capStart);
  ret.capEnd = EPSPathCap.loadRaw(raw.capEnd);
  
  return ret;
};


//=======================================================
// CAPS
//=======================================================

//return (start ? initial : final) point along path
//-------------------------------------------------------
EPSArc.prototype.capTip = function(start){
  var t = start ? this.angleStart*Math.PI/180 : this.angleEnd*Math.PI/180;
  return [
    this.center[0] + this.radius*Math.cos(t),
    this.center[1] + this.radius*Math.sin(t)
  ];
};

//return direction of cap at (start ? initial : final) point along path
//-------------------------------------------------------
EPSArc.prototype.capDirection = function(start){
  var t = start ? this.angleStart*Math.PI/180 : this.angleEnd*Math.PI/180;
  return start ? 
    [ Math.sin(t), -Math.cos(t)] :
    [-Math.sin(t),  Math.cos(t)];
};


//=======================================================
// EPS OUTPUT
//=======================================================

//define the path (including begin and close); no drawing or fill
//-------------------------------------------------------
EPSArc.prototype.definePathEPS = function(putline){
  
  //create path
  putline(EPSMacro.NEWPATH.get);
  
  putline(
    this.center[0] + " " + this.center[1] + " " + 
    this.radius + " " + 
    this.angleStart + " " + this.angleEnd + " arc");
  
  //close and fill
  if(this.closed){
    if(this.closeType == EPSArc.CloseType.SECTOR){
      putline(this.center[0] + " " + this.center[1] + " " + EPSMacro.LINETO.get);
    }
    putline(EPSMacro.CLOSEPATH.get);
  }
};


//=======================================================
// DRAW ONTO CANVAS
//=======================================================

//define the path (including begin and close); no drawing or fill
//-------------------------------------------------------
EPSArc.prototype.definePath = function(context, figGeom){
  
  //create path
  context.beginPath();
  
  var p = figGeom.FIGtoCTX(this.center);
  context.arc(p[0], p[1], figGeom.PTtoPX(this.radius), -this.angleStart*Math.PI/180, -this.angleEnd*Math.PI/180, true);
  
  if(this.closed){
    if(this.closeType == EPSArc.CloseType.SECTOR){
      context.lineTo(p[0],p[1]);
    }
    context.closePath();
  }
};


//=======================================================
// UI
//=======================================================

EPSArc.caption = "Arc";
EPSArc.icon = "image/object-arc.png";
EPSArc.prototype.caption = EPSArc.caption;
EPSArc.prototype.icon = EPSArc.icon;

//objects
//-------------------------------------------------------
EPSArc.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: EPSArc.icon, caption: EPSArc.caption, object: this }];
};

//anchors
//-------------------------------------------------------
EPSArc.prototype.getAnchors = function(figGeom, UIMethods){
  var me = this;
  
  var moveAnchor = new UIAnchor("image/arrow-move.png", this.center);
  moveAnchor.onDragEnd = function(p){
    me.center = p;
    UIMethods.changedEPS();
  };
};

//options
//-------------------------------------------------------
EPSArc.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table, $subtitleExtra;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + EPSArc.icon + "> ARC"));
  
  //PATH STYLE
  //------------------------
  $subtitleExtra = $("<span>");
  $target.append($("<div class=panel-subtitle>").append("Style", $subtitleExtra));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  FormTools.createCheckbox($subtitleExtra, "Closed", o.closed, function(val){
    o.closed = val;
    UIMethods.changedEPS();
  });
  
  //close type
  var $closeType = FormTools.createRadioGroup($("<td colspan=2>"), "closeType",
    { caption: "Segment", selected: (o.closeType == EPSArc.CloseType.SEGMENT),
      onSelect: function(){
        o.closeType = EPSArc.CloseType.SEGMENT;
        UIMethods.changedEPS();
      }},
    { caption: "Sector", selected: (o.closeType == EPSArc.CloseType.SECTOR), 
      onSelect: function(){
        o.closeType = EPSArc.CloseType.SECTOR;
        UIMethods.changedEPS();
      }}
  );
  $table.append($("<tr>").append( $closeType ));
  
  //angles
  var $posTextA = $("<input>", {"type": "text", "width": "40px", "value": o.angleStart});
  $posTextA.change(function(e){
    o.angleStart = parseFloat($(this).val());
    UIMethods.changedEPS();
  });
  
  var $posTextB = $("<input>", {"type": "text", "width": "40px", "value": o.angleEnd});  
  $posTextB.change(function(e){
    o.angleEnd = parseFloat($(this).val());
    UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("Angles (deg):"), 
    $("<td align=right>").append($posTextA).append("-").append($posTextB)
  ));
  
  //radius
  var $radius = $("<input>", {"type": "text", "width": "40px", "value": FigTools.PTtoIN(o.radius) });  
  $radius.change(function(e){
    o.radius = FigTools.INtoPT($(this).val());
    UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("Radius (in):"), 
    $("<td align=right>").append($radius)
  ));
  
  //GENERIC PATH OPTIONS
  //------------------------
  o.appendOptionsHTML($target, figGeom, UIMethods);
};

