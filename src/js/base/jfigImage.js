//This is actually just a positionable div that can be filled
//with anything--designed with image or canvas in mind

//Instructions for creating an image object:
//-------------------------------------------------------
//  Custom.prototype = new JFigImage();
//  Custom.prototype.constructor = Custom;
//  function Custom(){ };


//=======================================================
// FIG IMAGE OBJECT TEMPLATE
//=======================================================

function JFigImage(){
  this.zsort = 0;
  
  this.mouseover = false;
  this.highlight = false;
};

JFigImage.prototype.init = function(){
  this.$container = $("<div>", {"class": "jfig-image"});
  this.boxOut = {x:[0,144], y:[0,144]}; //(2x2in) units: PT
};

//translate position (x,y units: PT)
//-------------------------------------------------------
JFigImage.prototype.translate = function(x,y){

  //translate boxOut
  this.boxOut.x[0] += x; this.boxOut.x[1] += x;
  this.boxOut.y[0] += y; this.boxOut.y[1] += y;
};

//picking (pctx is point on context); return true if picked
//-------------------------------------------------------
JFigImage.prototype.pick = function(figGeom, pctx){

  //bounding box of $container in px
  var lt = [ this.$container.position().left, this.$container.position().top ];
  var w = this.$container.width(), h = this.$container.height();
  
  //check if p is in the bounding box
  return( 
    (pctx[0] >= lt[0]) && (pctx[0] <= (lt[0]+w)) &&
    (pctx[1] >= lt[1]) && (pctx[1] <= (lt[1]+h))
  );
};

//[PRIVATE] update "background" CSS based on mouseover/highlight status
//-------------------------------------------------------
JFigImage.prototype.updateSelectStyle = function(){
  
  if(this.highlight || this.mouseover){
    this.$container.css({
      "background": FigOptions.COLOR_BG(this.mouseover, this.highlight),
      "outline": "dotted 1px #000000"
    });
  }else{
    this.$container.css({
      "background": "none",
      "outline": "none"
    });
  }
};

//set mouseover; return false (no redraw)
//-------------------------------------------------------
JFigImage.prototype.setMouseover = function(val){
  this.mouseover = val;
  this.updateSelectStyle();
  return false;
};

//set highlight; return false (no redraw)
//-------------------------------------------------------
JFigImage.prototype.setHighlight = function(val){
  this.highlight = val;
  this.updateSelectStyle();
  return false;
};


//=======================================================
// HTML
// create/modify HTML object(s)
//=======================================================

//add to, or move to, a target container
//-------------------------------------------------------
JFigImage.prototype.appendHTML = function($target, figGeom){

  $target.append(this.$container);
  this.updateCSS(figGeom);
};

//remove from current container
//-------------------------------------------------------
JFigImage.prototype.removeHTML = function(){
  this.$container.remove();
};

//call to update display position
//-------------------------------------------------------
JFigImage.prototype.updateCSS = function(figGeom){

  //change position
  var p00fig = [this.boxOut.x[0], this.boxOut.y[0]];
  var p00ctx = figGeom.FIGtoCTX(p00fig);
  var wctx = figGeom.PTtoPX(this.boxOut.x[1] - this.boxOut.x[0]);
  var hctx = figGeom.PTtoPX(this.boxOut.y[1] - this.boxOut.y[0]);
  
  this.$container.css({
    "left": p00ctx[0] + "px",
    "top": (p00ctx[1]-hctx) + "px",
    "width": wctx + "px",
    "height": hctx + "px"
  });
};


//=======================================================
// UI
//=======================================================

//anchors (use within getAnchors)
//onChange: invoked when anchors are moved
//-------------------------------------------------------
JFigImage.prototype.getAnchorsBoxOut = function(figGeom, UIMethods, onChange){
  var me = this;
  
  //center of axes output box (units: PT)
  var cx = (this.boxOut.x[0] + this.boxOut.x[1]) / 2;
  var cy = (this.boxOut.y[0] + this.boxOut.y[1]) / 2;
  
  //anchors
  var anchorC = new UIAnchor("image/arrow-move.png", [ cx, cy ]);
  var anchorBL = new UIAnchor("image/arrow-resize-045.png", [ this.boxOut.x[0], this.boxOut.y[0] ]);
  var anchorBR = new UIAnchor("image/arrow-resize-135.png", [ this.boxOut.x[1], this.boxOut.y[0] ]);
  var anchorTR = new UIAnchor("image/arrow-resize-045.png", [ this.boxOut.x[1], this.boxOut.y[1] ]);
  var anchorTL = new UIAnchor("image/arrow-resize-135.png", [ this.boxOut.x[0], this.boxOut.y[1] ]);
  var anchorL = new UIAnchor("image/arrow-resize-h.png",   [ this.boxOut.x[0], cy ]);
  var anchorR = new UIAnchor("image/arrow-resize-h.png",   [ this.boxOut.x[1], cy ]);
  var anchorB = new UIAnchor("image/arrow-resize-v.png",   [ cx, this.boxOut.y[0] ]);
  var anchorT = new UIAnchor("image/arrow-resize-v.png",   [ cx, this.boxOut.y[1] ]);
  
  var onDragEndCommon = function(){
    //resize div and invoke custom onChange method
    me.updateCSS(figGeom);
    onChange();
    
    //update anchor positions
    var cx = (me.boxOut.x[0] + me.boxOut.x[1]) / 2;
    var cy = (me.boxOut.y[0] + me.boxOut.y[1]) / 2;
    anchorC.p = [ cx, cy ];
    anchorBL.p = [ me.boxOut.x[0], me.boxOut.y[0] ];
    anchorBR.p = [ me.boxOut.x[1], me.boxOut.y[0] ];
    anchorTR.p = [ me.boxOut.x[1], me.boxOut.y[1] ];
    anchorTL.p = [ me.boxOut.x[0], me.boxOut.y[1] ];
    anchorL.p = [ me.boxOut.x[0], cy ];
    anchorR.p = [ me.boxOut.x[1], cy ];
    anchorB.p = [ cx, me.boxOut.y[0] ];
    anchorT.p = [ cx, me.boxOut.y[1] ];
    
    UIAnchor.updateCSS(figGeom);
  };
  
  //anchor event handlers
  anchorC.onDragEnd = function(p){
    var dx = p[0] - (me.boxOut.x[0] + me.boxOut.x[1]) / 2;
    var dy = p[1] - (me.boxOut.y[0] + me.boxOut.y[1]) / 2;
    me.boxOut.x[0] += dx; me.boxOut.x[1] += dx;
    me.boxOut.y[0] += dy; me.boxOut.y[1] += dy;
    onDragEndCommon();
  };
  
  anchorBL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorBR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorTR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
  
  anchorTL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
  
  anchorL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    onDragEndCommon();
  };
  
  anchorR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    onDragEndCommon();
  };
  
  anchorB.onDragEnd = function(p){
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorT.onDragEnd = function(p){
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
};

