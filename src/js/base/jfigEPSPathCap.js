//used in jfigEPSPath.js

//=======================================================
// EPS PATH CAP
// cap for EPS path
//=======================================================

function EPSPathCap(){

  //set default values
  this.enabled = false;
  this.type = EPSPathCap.Style.ARROW;
  this.filled = true;
  this.stroked = false;
  this.size = 4; //units: PT
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
EPSPathCap.prototype.saveRaw = function(){
  return {
    enabled: this.enabled,
    style: this.type.styleName, //special: recover via EPSPathCap.getStyleFromName()
    filled: this.filled,
    stroked: this.stroked,
    size: this.size
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
EPSPathCap.loadRaw = function(raw){
  var ret = new EPSPathCap();
  
  ret.enabled = raw.enabled;
  ret.type = EPSPathCap.getStyleFromName(raw.style);
  ret.filled = raw.filled;
  ret.stroked = raw.stroked;
  ret.size = raw.size;
  
  return ret;
};


//=======================================================
// UTILITY METHODS
//=======================================================

//fig pt given tip (p), unit dir (t), scale (s), pCap (at p facing t: [1,0] = 1 PT right, [0,1] = 1 PT fwd)
//-------------------------------------------------------
EPSPathCap.capToFig = function(p, t, s, pCap){
  
  //vector contribution from pCap[0] (left/right direction from tip; ortho direction to t)
  var u = [ t[1]*pCap[0], -t[0]*pCap[0] ];
  //contribution from pCap[1] (forward/back direction from tip; direction of t)
  var v = [ t[0]*pCap[1],  t[1]*pCap[1] ];
  
  //final point is (tip)+u+v
  return [ p[0]+(u[0]+v[0])*s, p[1]+(u[1]+v[1])*s ];
};

EPSPathCap.capToFigArray = function(p, t, s, pCapArray){
  var ret = [];
  for(var i=0, il=pCapArray.length; i<il; i++){
    if(!(i in pCapArray)){ continue; }
    ret.push(EPSPathCap.capToFig(p, t, s, pCapArray[i]));
  }
  return ret;
};

//append closed path given by array of points (units PT); no drawing or fill
//-------------------------------------------------------
EPSPathCap.appendPath = function(context, figGeom, capPolygon){

  //points
  for(var i=0, il=capPolygon.length; i<il; i++){
    var p = figGeom.FIGtoCTX(capPolygon[i]);
    
    if(i==0){ context.moveTo(p[0],p[1]); }
    else{ context.lineTo(p[0],p[1]); }
  }
  
  //close path
  context.closePath();
};

EPSPathCap.appendPathEPS = function(putline, capPolygon){

  //points
  for(var i=0, il=capPolygon.length; i<il; i++){
    var p = capPolygon[i];
    putline(p[0] + " " + p[1] + " " + ((i==0) ? EPSMacro.MOVETO.get : EPSMacro.LINETO.get));
  }
  
  //close path
  putline(EPSMacro.CLOSEPATH.get);
};

//append cap shape path given tip (p) and direction (t)
//-------------------------------------------------------
EPSPathCap.prototype.appendShape = function(context, figGeom, p, t){
  this.type.appendShape(context, figGeom, p, t, this.size);
};

EPSPathCap.prototype.appendShapeEPS = function(putline, p, t){
  this.type.appendShapeEPS(putline, p, t, this.size);
};


//append cap bounding box path given tip (p) and direction (t)
//-------------------------------------------------------
EPSPathCap.prototype.getBoundingBox = function(p, t){
  var bounds = this.type.bounds;
  return EPSPathCap.capToFigArray(p, t, this.size, [
    [bounds.x[0], bounds.y[0]], [bounds.x[1], bounds.y[0]],
    [bounds.x[1], bounds.y[1]], [bounds.x[0], bounds.y[1]]
  ]);
};

EPSPathCap.prototype.appendBoundingBox = function(context, figGeom, p, t){
  EPSPathCap.appendPath(context, figGeom, this.getBoundingBox(p, t)); 
};

EPSPathCap.prototype.appendBoundingBoxEPS = function(putline, p, t){
  EPSPathCap.appendPathEPS(putline, this.getBoundingBox(p, t)); 
};


//=======================================================
// STYLES
//=======================================================

EPSPathCap.getStyleFromName = function(styleName){
  switch(styleName){
    case "Square": return EPSPathCap.Style.SQUARE;
    case "Circle": return EPSPathCap.Style.CIRCLE;
    default: return EPSPathCap.Style.ARROW;
  }
};

//enum for cap style (value irrelevant)
//-------------------------------------------------------
EPSPathCap.Style = Object.freeze({ 
  //each type should have:
  //  "styleName": string describing the style (see EPSPathCap.getStyleFromName)
  //  "bounds" {x:[a,b],y:[a,b]}; a,b multiply size (PT) [in "capToFig" coords]
  //  "appendShape" append path to context (p = cap tip, t = direction, s = scale)
  //  "appendShapeEPS" append path to EPS (p = cap tip, t = direction, s = scale)
  
  //standard arrow head
  //--------------------
  ARROW: { 
    styleName: "Arrow",
    bounds: { x: [-0.5, 0.5], y: [-0.95*1.5, 0.01*1.5] },
    shape: [ [0,0], [-0.5, -0.95*1.5], [ 0.5, -0.95*1.5] ],
    
    appendShape: function(context, figGeom, p, t, s){
      var path = EPSPathCap.capToFigArray(p, t, s, EPSPathCap.Style.ARROW.shape);
      EPSPathCap.appendPath(context, figGeom, path);
    },
    appendShapeEPS: function(putline, p, t, s){
      var path = EPSPathCap.capToFigArray(p, t, s, EPSPathCap.Style.ARROW.shape);
      EPSPathCap.appendPathEPS(putline, path);
    }
  },
  
  //square shape
  //--------------------
  SQUARE: { 
    styleName: "Square",
    bounds: { x: [-0.5, 0.5], y: [-0.5, 0.5] },
    shape: [ [-0.5, -0.5], [ 0.5, -0.5], [ 0.5,  0.5], [-0.5,  0.5] ],
    
    appendShape: function(context, figGeom, p, t, s){
      var path = EPSPathCap.capToFigArray(p, t, s, EPSPathCap.Style.SQUARE.shape);
      EPSPathCap.appendPath(context, figGeom, path);
    },
    appendShapeEPS: function(putline, p, t, s){
      var path = EPSPathCap.capToFigArray(p, t, s, EPSPathCap.Style.SQUARE.shape);
      EPSPathCap.appendPathEPS(putline, path);
    }
  },
  
  //circle
  //--------------------
  CIRCLE: {
    styleName: "Circle",
    bounds: { x: [-0.5, 0.5], y: [-0.5, 0.5] },
    
    appendShape: function(context, figGeom, p, t, s){
      var p = figGeom.FIGtoCTX(p);
      context.arc(p[0], p[1], figGeom.PTtoPX(s*0.5), 0, -2*Math.PI, true);
      context.closePath();
    },
    appendShapeEPS: function(putline, p, t, s){
      putline(p[0] + " " + p[1] + " " + (s*0.5) + " 0 360 arc");
      putline(EPSMacro.CLOSEPATH.get);
    }
  }
});


//=======================================================
// OPTIONS PANEL
//=======================================================

EPSPathCap.prototype.appendOptionsHTML = function($target, caption, figGeom, UIMethods){
  
  var me = this;
  var $table, $subtitleExtra;
  
  //OPTIONS
  //------------------------
  $subtitleExtra = $("<span>");
  $target.append($("<div class=panel-subtitle>").append(caption, $subtitleExtra));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  FormTools.createCheckbox($subtitleExtra, "Show", me.enabled, function(val){
    me.enabled = val;
    UIMethods.changedEPS();
  });
  
  //EPSPathCap style
  var $style = FormTools.createSelect($("<span>"),
    { caption: "Arrow", selected: (me.type == EPSPathCap.Style.ARROW), 
      onSelect: function(){
        me.type = EPSPathCap.Style.ARROW;
        UIMethods.changedEPS();
      }},
    { caption: "Square", selected: (me.type == EPSPathCap.Style.SQUARE), 
      onSelect: function(){
        me.type = EPSPathCap.Style.SQUARE;
        UIMethods.changedEPS();
      }},
    { caption: "Circle", selected: (me.type == EPSPathCap.Style.CIRCLE), 
      onSelect: function(){
        me.type = EPSPathCap.Style.CIRCLE;
        UIMethods.changedEPS();
      }}
  );
  
  //stroked and filled
  var $stroked = FormTools.createCheckbox($("<td align=right>"), "Stroked", me.stroked, function(val){
    me.stroked = val;
    UIMethods.changedEPS();
  });
  
  var $filled = FormTools.createCheckbox($("<td align=right>"), "Filled", me.filled, function(val){
    me.filled = val;
    UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $style, $stroked, $filled ));
};

