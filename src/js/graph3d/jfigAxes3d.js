//defines JFigAxes3d
//see jfigImage.js for info on image objects

//=======================================================
// AXES 3D
//=======================================================

JFigAxes3d.prototype = new JFigImage();
JFigAxes3d.prototype.constructor = JFigAxes3d;

function JFigAxes3d(){
  this.init();
  
  //children (e.g. graphs) created in getCustomToolbarButtons/loadRaw
  //should have onAxesChanged method to respond to axes modification
  //should set isChild = true; onDelete should call axes.removeChild
  this.children = [];
  
  //set default values
  this.boxCoords = {x:[-2,2], y:[-2,2], z:[-2,2]};
  
  //function
  this.f = ["u", "v", "Math.cos(Math.sqrt(u*u+v*v))"];
  this.rangeIn = {u:[-3,3], v:[-3,3]};
  this.n = [40,40]; //TODO
  
  //camera options
  this.camFOVy = 45; //deg
  this.camDistance = 10;
  this.camLookAt = [0,0,0];
  this.camTheta = -Math.PI/2; //radians
  this.camPhi = 0.5; //radians
  
  //TODO: TEST
  //==========================================
  var me = this;
  eval("var ff = function(u,v){ return [" + this.f[0] + "," + this.f[1] + "," + this.f[2] + "]; };");
  var fff = function(ut,vt){
    var u = me.rangeIn.u[0] + ut*(me.rangeIn.u[1] - me.rangeIn.u[0]);
    var v = me.rangeIn.v[0] + vt*(me.rangeIn.v[1] - me.rangeIn.v[0]);
    return ff(u,v);
  };
  
  this.graphOpts = {
    n: [me.n[0],me.n[1]],
    rangeIn: {u:[0,1], v:[0,1]},
    loopU: false, loopV: false,
    
    f: fff,
    
    color: function(u,v,p){
      var lu = (u-this.rangeIn.u[0])/(this.rangeIn.u[1]-this.rangeIn.u[0]);
      var lv = (v-this.rangeIn.v[0])/(this.rangeIn.v[1]-this.rangeIn.v[0]);
      return [0.5+0.5*Math.sin(lv*2*Math.PI), 0.5, 0.5+0.5*Math.sin(lu*2*Math.PI)];
    }
  };
  this.graph = new Fig.GeomSurface(this.graphOpts);
  
  this.graph.generate();
  
  this.graphSO = this.graph.instance();
  this.graphSO.emissiveCoefficient = 0.4;
  this.graphSO.wireframeVisible = true;
  this.graphSO.wireframeColor = [0,0,0,0.1];
  this.graphSO.solidVisible = true;
  
  var onInitialize = function(gl, fig){
    //TODO: dont like this.gl
    me.gl = gl;
    fig.autoResize = false;
    fig.addToScene(gl, me.graphSO);
  };
  
  var $target = this.$container;
  
  me.figure = new Fig.Figure($target, onInitialize);
  me.updateCamera();
  me.updateSize(FigTools.DEFAULT_PPI);
  //==========================================
};

//push "refresh" to each child object
//-------------------------------------------------------
JFigAxes3d.prototype.pushAxesChanged = function(){
  //notify each child that axes changed (boxCoords)
  $.each(this.children, function(i,c){ c.onAxesChanged(); });
};


//add to list of children (used in getCustomToolbarButtons/loadRaw)
//-------------------------------------------------------
JFigAxes3d.prototype.addChild = function(C){
  var me = this;
  
  C.isChild = true;
  C.onDelete = function(){ me.removeChild(C); };
  
  this.children.push(C);
};

//[PRIVATE] remove form list of children: when creating a child graph, use in onDelete
//-------------------------------------------------------
JFigAxes3d.prototype.removeChild = function(C){
  var i = $.inArray(C,this.children);
  if(i > -1){ this.children.splice(i,1); }
};

//remove children from project when axes are removed
//-------------------------------------------------------
JFigAxes3d.prototype.onDelete = function(){

  //duplicate list of children first
  var toRemove = this.children.slice(0);
  
  $.each(toRemove, function(i,C){ 
    //removes child from project; as a side-effect,
    //child.onDelete and then axes.removeChild will be called
    C.removeFromContents();
  });
};

//defer refreshing until added; add already-existing children to project
//-------------------------------------------------------
JFigAxes3d.prototype.onAdd = function(UIMethods, figGeom){
  
  //add children
  $.each(this.children, function(i,c){ 
    UIMethods.addCompound(c);
  });
};


//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigAxes3d.prototype.saveRaw = function(){
  //TODO
  return{
    type: "JFigAxes3d",
    zsort: this.zsort,
    boxOut: this.boxOut,
    
    children: JFigAxes.saveRawChildren(this.children),
    boxCoords: this.boxCoords,
    
    //function
    f: this.f,
    rangeIn: this.rangeIn,
    
    //camera
    camFOVy: this.camFOVy,
    camDistance: this.camDistance,
    camLookAt: this.camLookAt,
    camTheta: this.camTheta,
    camPhi: this.camPhi
  };
};

JFigAxes3d.saveRawChildren = function(children){
  var ret = [];
  
  $.each(children, function(i,child){ 
    ret.push(child.saveRaw());
  });  
  
  return ret;
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigAxes3d.loadRaw = function(raw){
  var ret = new JFigAxes3d();
  ret.zsort = raw.zsort;
  ret.boxOut = raw.boxOut;
  
  ret.boxCoords = raw.boxCoords;
  
  //function
  //TODO: must redo geometry at this point, but only if figure exists...
  ret.f = raw.f;
  ret.rangeIn = raw.rangeIn;
  
  //camera
  ret.camFOVy = raw.camFOVy;
  ret.camDistance = raw.camDistance;
  ret.camLookAt = raw.camLookAt;
  ret.camTheta = raw.camTheta;
  ret.camPhi = raw.camPhi;
  
  //children (i.e., graphs)
  ret.children = [];
  JFigAxes3d.loadRawChildren(ret, raw.children);
  
  return ret;
};

JFigAxes3d.loadRawChildren = function(axes, raw){
  
  //load according to "type", or error if "type" not found
  $.each(raw, function(i,C){
    var loadFunc;
    if(C.type !== undefined){ loadFunc = window[C.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      var child = loadFunc(axes, C);
      axes.addChild(child);
    }else{
      console.log("JFigAxes3d child has unknown type: ");
      console.log(C);
    }
  });
};

//=======================================================
// REFRESH
//=======================================================

JFigAxes3d.prototype.updateCamera = function(){
  var camera = this.figure.renderer.camera;
  
  camera.rotation.setUp([0,0,1]);
  
  var camDir = [
    Math.cos(this.camPhi)*Math.cos(this.camTheta),
    Math.cos(this.camPhi)*Math.sin(this.camTheta),
    Math.sin(this.camPhi)
  ];
  Fig.Vec3.addMultiple(camera.position, this.camLookAt, camDir, this.camDistance);
  camera.lookAt(this.camLookAt);
  
  camera.setFOVy(this.camFOVy);
  
  //TODO dont like this.gl
  this.figure.draw(this.gl);
};

JFigAxes3d.prototype.updateSize = function(ppi){
  
  var wpx = FigTools.PTtoPX(this.boxOut.x[1] - this.boxOut.x[0], ppi);
  var hpx = FigTools.PTtoPX(this.boxOut.y[1] - this.boxOut.y[0], ppi);
  
  //TODO dont like this.gl
  this.figure.renderer.setSize(this.gl, wpx, hpx);
  this.figure.draw(this.gl);
};

JFigAxes3d.prototype.refreshGeometry = function(){
  var me = this;

  eval("var ff = function(u,v){ return [" + this.f[0] + "," + this.f[1] + "," + this.f[2] + "]; };");
  var fff = function(ut,vt){
    var u = me.rangeIn.u[0] + ut*(me.rangeIn.u[1] - me.rangeIn.u[0]);
    var v = me.rangeIn.v[0] + vt*(me.rangeIn.v[1] - me.rangeIn.v[0]);
    return ff(u,v);
  };
  this.graphOpts.f = fff;
  this.graph.generate();
  
  //TODO dont like this.gl
  this.figure.update(this.gl, this.graphSO);
  this.figure.draw(this.gl);
};

//=======================================================
// UI
//=======================================================

JFigAxes3d.caption = "Axes (3D)";
JFigAxes3d.icon = "image/graph3d/object-3d.png";
JFigAxes3d.prototype.caption = JFigAxes3d.caption;
JFigAxes3d.prototype.icon = JFigAxes3d.icon;

//objects
//-------------------------------------------------------
JFigAxes3d.prototype.getObjectList = function(indent){
  var ret = [{ indent: indent, icon: JFigAxes3d.icon, caption: JFigAxes3d.caption, object: this }];
  
  $.each(this.children, function(i,c){ 
    ret = ret.concat(c.getObjectList(indent+1));
  });
  
  return ret;
};
//toolbar
//-------------------------------------------------------
JFigAxes3d.AnchorMode = Object.freeze({
  BOXOUT: 0,
  CAMERA: 1
});
JFigAxes3d.prototype.anchorMode = JFigAxes3d.AnchorMode.BOXOUT;

JFigAxes3d.prototype.getCustomToolbarButtons = function(UIMethods){
  var me = this;
  
  return [
    { caption: "Move/Resize", icon: "image/graph3d/toolbar-boxout.png", action: function(){
      me.anchorMode = JFigAxes3d.AnchorMode.BOXOUT;
      UIMethods.refreshAnchors(me);
    }},
    
    { caption: "Control Camera", icon: "image/graph3d/toolbar-camera.png", action: function(){
      me.anchorMode = JFigAxes3d.AnchorMode.CAMERA;
      UIMethods.refreshAnchors(me);
    }}
  ];
};

//anchors
//-------------------------------------------------------
JFigAxes3d.prototype.getAnchorsCamera = function(figGeom, UIMethods){
  var me = this;
  
  //center of axes output box (units: PT)
  var cfig = [
    (this.boxOut.x[0] + this.boxOut.x[1]) / 2,
    (this.boxOut.y[0] + this.boxOut.y[1]) / 2
  ];
  
  //rotation anchor
  var anchorR = new UIAnchor("image/graph3d/anchor-camera-rotate.png", cfig);
  anchorR.snap = false; //don't snap to grid
  
  var RADIANS_PER_PIXEL = 0.01;
  var theta0 = me.camTheta;
  var phi0 = me.camPhi;
  
  //reset position when released
  anchorR.onDragEnd = function(p){
    anchorR.reposition(cfig);
    
    theta0 = me.camTheta;
    phi0 = me.camPhi;
  };
  
  //rotate camera when dragged
  var phiMin = -Math.PI/2 + 0.01;
  var phiMax =  Math.PI/2 - 0.01;
  
  anchorR.onDrag = function(p){
    //update theta/phi values
    var dx = figGeom.PTtoPX(p[0] - cfig[0]);
    var dy = figGeom.PTtoPX(p[1] - cfig[1]);
    me.camTheta = theta0 - dx*RADIANS_PER_PIXEL;
    me.camPhi = phi0 - dy*RADIANS_PER_PIXEL;
    
    //clamp values
    if(me.camPhi < phiMin){ me.camPhi = phiMin; }
    if(me.camPhi > phiMax){ me.camPhi = phiMax; }
    while(me.camTheta < 0){ me.camTheta += 2*Math.PI; }
    while(me.camTheta > 2*Math.PI){ me.camTheta -= 2*Math.PI; }
    
    //update UI inputs if they exist
    if(me.$camTheta !== undefined){
      me.$camTheta.val(180.0*me.camTheta/Math.PI);
    }
    if(me.$camPhi !== undefined){
      me.$camPhi.val(180.0*me.camPhi/Math.PI);
    }
    
    //update camera
    me.updateCamera();
  };
};

JFigAxes3d.prototype.getAnchors = function(figGeom, UIMethods){
  var me = this;
  
  switch(this.anchorMode){
    case JFigAxes3d.AnchorMode.BOXOUT:
      var onChange = function(){ me.updateSize(figGeom.ppi); };
      this.getAnchorsBoxOut(figGeom, UIMethods, onChange);
      break;
    case JFigAxes3d.AnchorMode.CAMERA: 
      this.getAnchorsCamera(figGeom, UIMethods);
      break;
  }
};

//options
//-------------------------------------------------------
JFigAxes3d.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  
  var o = this;
  var $table, $subtitleExtra;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigAxes3d.icon + "> AXES (3D)"));
  
  //CAMERA
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Camera"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //FOVY
  var $camFOVy = $("<input>", {"type": "text", "width": "60px", "value": o.camFOVy});  
  $camFOVy.change(function(e){
    o.camFOVy = parseFloat($(this).val());
    o.updateCamera();
  });
  
  $table.append(
    $("<tr>").append( 
      $("<td>").append("FOV (deg):"), 
      $("<td align=right>").append($camFOVy) 
    )
  );
  
  //distance, theta, phi
  var $camDist = $("<input>", {"type": "text", "width": "60px", "value": o.camDistance});  
  $camDist.change(function(e){
    o.camDistance = parseFloat($(this).val());
    o.updateCamera();
  });
  
  var thetaDeg = 180.0 * o.camTheta / Math.PI;
  var phiDeg = 180.0 * o.camPhi / Math.PI;
  
  this.$camTheta = $("<input>", {"type": "text", "width": "60px", "value": thetaDeg});  
  this.$camTheta.change(function(e){
    o.camTheta = Math.PI * parseFloat($(this).val()) / 180.0;
    o.updateCamera();
  });
  
  this.$camPhi = $("<input>", {"type": "text", "width": "60px", "value": phiDeg});  
  this.$camPhi.change(function(e){
    o.camPhi = Math.PI * parseFloat($(this).val()) / 180.0;
    o.updateCamera();
  });
  
  $table.append(
    $("<tr>").append( 
      $("<td>").append("Distance:"), 
      $("<td align=right>").append($camDist) 
    ),
    $("<tr>").append( 
      $("<td colspan=2>").append("<span class=infotext>Orientation</span>") 
    ),
    $("<tr>").append( 
      $("<td>").append("Theta (deg):"), 
      $("<td align=right>").append(this.$camTheta) 
    ),
    $("<tr>").append( 
      $("<td>").append("Phi (deg):"), 
      $("<td align=right>").append(this.$camPhi) 
    )
  );
  
  //lookAt vector
  var $lookX = $("<input>", {"type": "text", "width": "40px", "value": o.camLookAt[0]});  
  $lookX.change(function(e){
    o.camLookAt[0] = parseFloat($(this).val());
    o.updateCamera();
  });
  
  var $lookY = $("<input>", {"type": "text", "width": "40px", "value": o.camLookAt[1]});  
  $lookY.change(function(e){
    o.camLookAt[1] = parseFloat($(this).val());
    o.updateCamera();
  });
  
  var $lookZ = $("<input>", {"type": "text", "width": "40px", "value": o.camLookAt[2]});  
  $lookZ.change(function(e){
    o.camLookAt[2] = parseFloat($(this).val());
    o.updateCamera();
  });
  
  $table.append(
    $("<tr>").append( 
      $("<td colspan=2>").append("<span class=infotext>Focus on this point</span>") 
    ),
    $("<tr>").append( 
      $("<td colspan=2>").append("x:", $lookX, "y:", $lookY, "z:", $lookZ) 
    )
  );
  
  //FUNCTION
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Function"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $rangeMinU = $("<input>", {"type": "text", "width": "40px", "value": o.rangeIn.u[0]});  
  $rangeMinU.change(function(e){
    o.rangeIn.u[0] = parseFloat($(this).val());
    o.refreshGeometry();
  });
  
  var $rangeMaxU = $("<input>", {"type": "text", "width": "40px", "value": o.rangeIn.u[1]});  
  $rangeMaxU.change(function(e){
    o.rangeIn.u[1] = parseFloat($(this).val());
    o.refreshGeometry();
  });
  
  var $rangeMinV = $("<input>", {"type": "text", "width": "40px", "value": o.rangeIn.v[0]});  
  $rangeMinV.change(function(e){
    o.rangeIn.v[0] = parseFloat($(this).val());
    o.refreshGeometry();
  });
  
  var $rangeMaxV = $("<input>", {"type": "text", "width": "40px", "value": o.rangeIn.v[1]});  
  $rangeMaxV.change(function(e){
    o.rangeIn.v[1] = parseFloat($(this).val());
    o.refreshGeometry();
  });
  
  $table.append($("<tr>").append(
    $("<td>").append("u:"), 
    $("<td align=right>").append($rangeMinU, " to ", $rangeMaxU)
  ));
  $table.append($("<tr>").append(
    $("<td>").append("v:"), 
    $("<td align=right>").append($rangeMinV, " to ", $rangeMaxV)
  ));
  
  var $fx = $("<input>", {"type": "text", "value": o.f[0]});
  $fx.change(function(e){
    o.f[0] = $(this).val();
    o.refreshGeometry();
  });
  
  var $fy = $("<input>", {"type": "text", "value": o.f[1]});
  $fy.change(function(e){
    o.f[1] = $(this).val();
    o.refreshGeometry();
  });
  
  var $fz = $("<input>", {"type": "text", "value": o.f[2]});
  $fz.change(function(e){
    o.f[2] = $(this).val();
    o.refreshGeometry();
  });
  
  $table.append($("<tr>").append( $("<td colspan=2 align=right>")
    .append("<span class=infotext>JS syntax, e.g. Math.pow(x,4)</span>") )); 
  $table.append($("<tr>").append( $("<td>").append("x:"), $("<td align=right>").append($fx) ));
  $table.append($("<tr>").append( $("<td>").append("y:"), $("<td align=right>").append($fy) ));
  $table.append($("<tr>").append( $("<td>").append("z:"), $("<td align=right>").append($fz) ));

};
  
