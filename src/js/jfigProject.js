
//=======================================================
// JFIG PROJECT
// figure contents and options
//=======================================================

function JFigProject(){
  this.zmin = 0;
  this.zmax = 0;
  
  //fig contents
  this.contents = new JFigCompound();
  
  this.epsSize = [216,144]; //(3x2in) units: PT
  this.title = "Untitled";
  this.filename = "figure";
};

JFigProject.prototype.expandZ = function(z){
  if(z < this.zmin){ this.zmin = z; }
  if(z > this.zmax){ this.zmax = z; }
};

//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigProject.prototype.saveRaw = function(){
  return {
    contents: this.contents.saveRaw(),
    
    epsSize: this.epsSize,
    title: this.title,
    filename: this.filename
  };
};

//restore instance from data returned by saveRaw
//NOTE: this is different from other loadRaw methods in that it
//modifies an existing object to restore a previously-saved object
//-----------------------------------------------
JFigProject.prototype.loadRaw = function(raw, UIMethods){
  this.title = raw.title;
  this.filename = raw.filename;
  this.epsSize = raw.epsSize;
  
  //clear contents
  project.contents.clear();
  
  //alt version of JFigCompound.loadRaw that adds each component to UI
  //-----------------------------------
  $.each(raw.contents.labels, function(i,L){
    var Lnew = JFigLabel.loadRaw(L);
    UIMethods.addLabel(Lnew);
  });
  
  //load according to "type", or load generic if "type" not found
  $.each(raw.contents.images, function(i,D){
    var loadFunc;
    if(D.type !== undefined){ loadFunc = window[D.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      var Dnew = loadFunc(D);
    }else{
      var Dnew = JFigImage.loadRaw(D);
    }
    UIMethods.addImage(Dnew);
  });
  
  //load according to "type", or error if "type" not found
  $.each(raw.contents.objects, function(i,O){
    var loadFunc;
    if(O.type !== undefined){ loadFunc = window[O.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      var Onew = loadFunc(O);
      UIMethods.addObject(Onew);
    }else{
      console.log("EPS object has unknown type: ");
      console.log(O);
    }
  });
  
  //load according to "type", or load generic if "type" not found
  $.each(raw.contents.compounds, function(i,C){
    var loadFunc;
    if(C.type !== undefined){ loadFunc = window[C.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      var Cnew = loadFunc(C);
    }else{
      var Cnew = JFigCompound.loadRaw(C);
    }
    UIMethods.addCompound(Cnew);
  });
  //-----------------------------------
};


//=======================================================
// OUTPUT (eps, tex)
//=======================================================

//EPS/LATEX OUTPUT ($target = e.g. textarea)
//-----------------------------------------------
JFigProject.prototype.outputEPS = function($target){
  
  //output EPS
  var eps = "";
  var putlineEPS = function(line){ eps += line + "\n"; }
  
  EPSWriter.writeBegin(putlineEPS, this.title, this.epsSize);
  this.contents.writeEPS(putlineEPS);
  EPSWriter.writeEnd(putlineEPS);
  
  $target.html(eps);
};

JFigProject.prototype.outputTEX = function($target){

  //output latex
  var latex = "";
  var putlineLatex = function(line){ latex += line + "\n"; }
  
  LatexWriter.writeBegin(putlineLatex, this.filename + ".eps", this.epsSize);
  this.contents.writeLatex(putlineLatex);
  LatexWriter.writeEnd(putlineLatex);
  
  $target.html(latex);
};

JFigProject.prototype.outputJFG = function($target){
  
  //project->obj
  var saveObj = this.saveRaw();      
  //obj->string
  var strJSON = JSON.stringify(saveObj);
  
  $target.html(strJSON);
};


//=======================================================
// OUTPUT (draw)
//=======================================================

//DRAW
//-----------------------------------------------
var draw = function(contextBG, context, figGeom){
  
  //clear canvas
  contextBG.clearRect(0,0,figGeom.canvasSize[0],figGeom.canvasSize[1]);
  context.clearRect(0,0,figGeom.canvasSize[0],figGeom.canvasSize[1]);
  
  //EPS size in PX
  var epsSizePX = [figGeom.PTtoPX(project.epsSize[0]), figGeom.PTtoPX(project.epsSize[1])];
  
  //figure background
  if(!snap){
    contextBG.fillStyle = "#FFFFFF";
    contextBG.fillRect(figGeom.offsetx, figGeom.offsety, epsSizePX[0], epsSizePX[1]);
  }else{
    var snapSizePX = [figGeom.PTtoPX(snapSize[0]), figGeom.PTtoPX(snapSize[1])];
    var nx = Math.ceil(project.epsSize[0] / snapSize[0]);
    var ny = Math.ceil(project.epsSize[1] / snapSize[1]);
    
    for(var i=0; i<nx; i++){
    for(var j=0; j<ny; j++){
      var altMinor = (i%2) == (j%2);
      var altMajor = (Math.floor(i/4)%2) == (Math.floor(j/4)%2);
      contextBG.fillStyle = altMajor ? 
        ( altMinor ? "#F0F0F0" : "#FFFFFF") :
        ( !altMinor ? "#E8E8E8" : "#F0F0F0");
      
      var overx = (i+1)*snapSizePX[0] - epsSizePX[0];
      var wfix = snapSizePX[0] - ((overx > 0) ? overx : 0);
      
      var overy = (j+1)*snapSizePX[1] - epsSizePX[1];
      var hfix = snapSizePX[1] - ((overy > 0) ? overy : 0);
      
      contextBG.fillRect(figGeom.offsetx + i*snapSizePX[0], figGeom.offsety + j*snapSizePX[1], wfix+1, hfix+1);
    }}
  }
  
  //draw contents
  project.contents.draw(context,figGeom);
  
  //boundary rulers
  var drawRulerH = function(y){
    context.beginPath(); 
    context.moveTo(0, y); 
    context.lineTo(figGeom.canvasSize[0], y); 
    context.stroke();
  };
  
  var drawRulerV = function(x){
    context.beginPath(); 
    context.moveTo(x, 0); 
    context.lineTo(x, figGeom.canvasSize[1]); 
    context.stroke();
  };
  
  context.strokeStyle = "#C0C0C0";
  context.lineWidth = 1;
  FigTools.DASH(context, [1,0]);
  
  drawRulerV(figGeom.offsetx);
  drawRulerV(figGeom.offsetx+epsSizePX[0]+1);
  drawRulerH(figGeom.offsety);
  drawRulerH(figGeom.offsety+epsSizePX[1]+1);
};
