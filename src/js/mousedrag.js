//===============================================
// JQUERY MOUSE HANDLER
//===============================================

//MOUSE EVENTS (MOVE, CLICK, DRAG)
//-----------------------------------------------
//dragTolerance: press+move won't initiate drag for this many pixels
//events: object w/ the following properties (all optional):
//  events.onMove = function(p)         <- not called when dragging
//  events.onDrag = function(p, dp, b)  <- called on mousemove when dragging
//  events.onDragStart = function(p, b) <- called immediately before the first call of onDrag
//  events.onDragEnd = function(p, b)   <- called on mouseup when dragging occurred
//  events.onClick = function(p, b)     <- called on mouseup when dragging did not occur
//-----------------------------------------------
$.prototype.addMouseDragHandler = function(events, dragTolerance){

  var pressed = false;  //is a mouse button pressed?
  var dragInitiated = false;    //has a drag event been initiated?
  var button = 0;       //which mouse button was the first to be pressed
  var p = [0,0];        //mouse position
  
  //get mouse position from event
  var $me = this;
  var getP = function(e){
    var o = $me.offset();
    return [e.pageX - o.left, e.pageY - o.top];
  };
  
  this.off("mousedown").on("mousedown", function(e){
    //ignore if mousedown occurs while dragging (e.g. hit another button)
    if(pressed){ return; }
    
    //initialize
    pressed = true;
    dragInitiated = false;
    button = e.which;
    p = getP(e);    
  });
  
  var stopDragAction = function(e){
    //ignore if not preceded by mousedown from same button
    if(!pressed || (e.which != button)){ return; }
    
    //no longer have a button pressed
    pressed = false;
    
    //either dragEnd or click, depending on whether a drag has been initiated
    if(dragInitiated){ 
      if(typeof(events.onDragEnd) == typeof(Function)){ events.onDragEnd(p, button); }
    }else{ 
      if(typeof(events.onClick) == typeof(Function)){ events.onClick(p, button); }
    }
  };
  
  this.off("mouseup").on("mouseup", stopDragAction);
  this.off("mouseleave").on("mouseleave", stopDragAction);
  
  this.off("mousemove").on("mousemove", function(e){
    //moving without dragging
    if(!pressed){ 
      if(typeof(events.onMove) == typeof(Function)){ events.onMove(getP(e)); }
      return; 
    }
    
    //check for drag event
    var pNew = getP(e);
    var dp = [pNew[0]-p[0], pNew[1]-p[1]];
    
    //set "dragInitiated", but only if dragTolerance surpassed
    if(!dragInitiated){
      if( (dp[0] < dragTolerance) && (dp[0] > -dragTolerance) && 
          (dp[1] < dragTolerance) && (dp[1] > -dragTolerance) ){ return; }
      
      if(typeof(events.onDragStart) == typeof(Function)){ events.onDragStart(p, button); }
      dragInitiated = true; 
    }
    
    //drag event and update position
    if(typeof(events.onDrag) == typeof(Function)){ events.onDrag(pNew, dp, button); }
    p = pNew;
  });
    
  return this;
};


//SCROLLWHEEL EVENTS
//-----------------------------------------------
$.prototype.addWheelHandler = function(onWheelUp, onWheelDown){
  
  var $me = this;
  this.bind("wheel", function(e){
    var o = $me.offset();
    var p = [e.originalEvent.clientX - o.left, e.originalEvent.clientY - o.top];
        
    if(e.originalEvent.deltaY > 0){ onWheelDown(p); }
    else{ onWheelUp(p); }
  });
};

