//=======================================================
// TOOLS FOR CREATING FORM/UI ELEMENTS
//=======================================================

function FormTools(){ };

//selected = current color; onChange = function(val); selected and val are RGB (array 0-255)
//---------------------------------------------------------------
FormTools.recentColors = [ 
  [255,0,0], [255,170,0], [255,255,0], [0,170,85], [0,170,255], [0,85,255], [170,85,255],
  [0,0,0], [64,64,64], [128,128,128], [192,192,192], [255,255,255]
];

FormTools.createColorSelect = function($target, selected, onChange){
  
  var $table = $("<table>", {"cellspacing": "1px", "cellpadding": "0px"});
  
  var w = "6px";
  var h = "8px";
  
  var createColorTD = function(rgb){
    var toHex = function(d){ return ("0" + Number(d).toString(16)).slice(-2); };
    var colorHex = "#" + toHex(rgb[0]) + toHex(rgb[1]) + toHex(rgb[2]);
    
    var $td = $("<td>", {"width": w, "height": h, "bgcolor": colorHex});
    $td.on("click", function(e){ 
      FormTools.recentColors.push([ rgb[0], rgb[1], rgb[2] ]);
      onChange(rgb); 
    });
    return $td;
  }
  
  var levels = [0,85,170,255];//=00 55 AA FF
  var getTD = function(lr,lg,lb){
    return createColorTD( [levels[lr], levels[lg], levels[lb]] );
  };
  
  var createRow = function(y){
    var $ret = $("<tr>", {"height": h});
    
    var createSubRow = function(z){
      for(var x=0; x<4; x++){ $ret.append( getTD(z,x,y) ); }
    };
    for(var z=0; z<4; z++){ createSubRow(z); }
    
    return $ret;
  };
  
  var $recentTR = $("<tr>", {"height": h});
  var nRecent = FormTools.recentColors.length;
  for(var i=1; (i<=nRecent) && (i<=16); i++){
    var rgb = FormTools.recentColors[nRecent-i];
    $recentTR.append( createColorTD(rgb) );
  }
  
  $table.append( $recentTR, createRow(0), createRow(1), createRow(2), createRow(3));
  
  $target.append($table);
  return $target;
};

//arguments are: target, groupName, then objects with "caption", "selected", and "onSelect" properties
//---------------------------------------------------------------
FormTools.createRadioGroup = function(){
  var $target = arguments[0];
  var groupName = arguments[1];
  
  for(var i=2, il=arguments.length; i<il; i++){
    
    var $opt = arguments[i].selected ? 
      $("<input>", {"type": "radio", "name": groupName, "checked": "checked"}) : 
      $("<input>", {"type": "radio", "name": groupName});
    var onSelect = arguments[i].onSelect;
    
    (function($opt,onSelect){
      $opt.change(function(){
        if(!$(this).prop("checked")){ return; }
        onSelect();
      });
    })($opt,onSelect);
    
    $target.append($("<label>").append($opt, arguments[i].caption));
  }
  
  return $target;
};

//arguments are: target, then objects with "caption", "selected", and "onSelect" properties
//---------------------------------------------------------------
FormTools.createSelect = function(){
  var $target = arguments[0];

  //create select box
  var $sel = $("<select>");
  for(var i=1, il=arguments.length; i<il; i++){
    
    var $opt = arguments[i].selected ? 
      $("<option>", {"value": i, "selected": "1"}) : 
      $("<option>", {"value": i});
    $opt.append(arguments[i].caption);
    
    $sel.append($opt);
  }
  
  //add handler
  var actions = [];
  for(var i=1, il=arguments.length; i<il; i++){
    actions[i] = arguments[i].onSelect;
  }
  $sel.change(function(){
    var i = this.value;
    actions[i].call();
  });
  
  //finish
  $target.append($sel);
  return $target;
};

//onChange = function(val)
//---------------------------------------------------------------
FormTools.createCheckbox = function($target, caption, selected, onChange){
  $cbox = selected ? 
    $("<input>", {"type": "checkbox", "checked": "checked"}) : 
    $("<input>", {"type": "checkbox"});
    
  $cbox.change(
    function(){ onChange($(this).prop("checked")); }
  );
  
  $target.append($("<label>").append($cbox, caption));
  return $target;
};

