//defines JFigGraph
//see jfigCompound.js for info on compound objects 

//=======================================================
// GRAPH
//=======================================================

JFigGraph.prototype = new JFigCompound();
JFigGraph.prototype.constructor = JFigGraph;

function JFigGraph(axes, f){
  this.axes = axes;  
  this.f = f;
  
  //set default values
  this.boundLeft = false;
  this.xmin = this.axes.boxCoords.x[0];
  this.boundRight = false;
  this.xmax = this.axes.boxCoords.x[1];
  
  this.capStart = new EPSPathCap();
  this.capStart.enabled = true;
  this.capEnd = new EPSPathCap();
  this.capEnd.enabled = true;
  
  this.n = 40;
  this.smoothed = true;
  this.smoothAmount = 1; //between 0 and 1
  this.pen = new EPSPen();
};

//defer refreshing until added
//-------------------------------------------------------
JFigGraph.prototype.onAdd = function(UIMethods, figGeom){
  //refresh path
  this.refreshEPS();
};

//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigGraph.prototype.saveRaw = function(){
  var ret = {
    type: "JFigGraph",
    f: this.f,
    zsort: this.zsort,
    
    capStart: this.capStart.saveRaw(),
    capEnd: this.capEnd.saveRaw(),
    
    n: this.n,
    smoothed: this.smoothed,
    smoothAmount: this.smoothAmount,
    pen: this.pen.saveRaw()
  };
  
  //conditional properties
  ret.boundLeft = this.boundLeft;
  if(this.boundLeft){ ret.xmin = this.xmin; }
  
  ret.boundRight = this.boundRight;
  if(this.boundRight){ ret.xmax = this.xmax; }
  
  return ret;
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigGraph.loadRaw = function(axes, raw){
  var ret = new JFigGraph(axes, raw.f); 
  ret.zsort = raw.zsort;
  
  ret.capStart = EPSPathCap.loadRaw(raw.capStart);
  ret.capEnd = EPSPathCap.loadRaw(raw.capEnd);
  
  ret.n = raw.n;
  ret.smoothed = raw.smoothed;
  ret.smoothAmount = raw.smoothAmount;
  ret.pen = EPSPen.loadRaw(raw.pen);
  
  //conditional properties
  ret.boundLeft = raw.boundLeft;
  if(ret.boundLeft){ ret.xmin = raw.xmin; }
  
  ret.boundRight = raw.boundRight;
  if(ret.boundRight){ ret.xmax = raw.xmax; }
  
  return ret;
};


//=======================================================
// PATH CREATION
//=======================================================

//respond to axes update
//-----------------------------------------------
JFigGraph.prototype.onAxesChanged = function(){
  this.refreshEPS();
  
  if(!this.boundLeft){ this.xmin = this.axes.boxCoords.x[0]; }
  if(!this.boundRight){ this.xmax = this.axes.boxCoords.x[1]; }
};

//recreate outline, guides, and grid
//-------------------------------------------------------
JFigGraph.prototype.refreshEPS = function(){
  this.clearObjects();
  
  //sample x-value min/max
  var x0 = this.axes.boxCoords.x[0];
  var x1 = this.axes.boxCoords.x[1];
  
  if(this.boundLeft){
    if(this.xmin > this.axes.boxCoords.x[1]){ return; }
    if(this.xmin > this.axes.boxCoords.x[0]){ x0 = this.xmin; }
  }
  
  if(this.boundRight){
    if(this.xmax < this.axes.boxCoords.x[0]){ return; }
    if(this.xmax < this.axes.boxCoords.x[1]){ x1 = this.xmax; }
  }
  
  //create graph path
  eval("var f = function(x){ return " + this.f + "; };");
  
  var me = this;
  var createGraphPiece = function(){
    var graph = new EPSPath();
    graph.pen = me.pen;
    graph.smoothed = me.smoothed;
    graph.smoothAmount = me.smoothAmount;
    return graph;
  };
  
  //create initial graph piece and point
  var p0 = [x0, f(x0)];
  var v0 = this.axes.isVisible(p0);
  
  var graph = createGraphPiece();
  if(v0){ 
    graph.capStart = me.capStart;
    graph.points.push(this.axes.coordsToEPS(p0)); 
  }
  
  //loop through samples
  for(var i=1; i<this.n; i++){ 
  
    //update current point
    var x = (x1-x0)*i/(this.n-1) + x0;
    var p = [x, f(x)];
    var v = this.axes.isVisible(p);
    
    //update path(s) depending on visibility of current and previous point
    if(v0){
      if(v){
        //both current and previous point are visible:
        //just append the current point
        graph.points.push(this.axes.coordsToEPS(p)); 
      }else{
        //current point visible but previous point not:
        //put intersection; end graph piece and start a new one
        var q = this.axes.clip(p0,p);
        graph.points.push(this.axes.coordsToEPS(q));
        this.addObject(graph);
        graph = createGraphPiece();
      }
    }else{
      if(v){
        //previous point visible but current point not:
        //put intersection, then p
        var q = this.axes.clip(p,p0);
        graph.points.push(this.axes.coordsToEPS(q));
        graph.points.push(this.axes.coordsToEPS(p));
      }
    }
    
    //update last
    p0 = p; v0 = v;
  }
  
  //add last graph piece if necessary
  if(v0){ 
    graph.capEnd = me.capEnd;
    this.addObject(graph); 
  }
};


//=======================================================
// UI
//=======================================================

JFigGraph.caption = "Graph";
JFigGraph.icon = "image/graph2d/object-graph.png";
JFigGraph.prototype.caption = JFigGraph.caption;
JFigGraph.prototype.icon = JFigGraph.icon;

//objects
//-------------------------------------------------------
JFigGraph.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: JFigGraph.icon, caption: JFigGraph.caption, object: this }];
};

//options
//-------------------------------------------------------
JFigGraph.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigGraph.icon + "> GRAPH"));
  
  //PARENT AXES
  //------------------------
  o.axes.appendRangeOptionsHTML($target, "Axes", figGeom, UIMethods);
  
  //BOUNDS
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Bounds"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $boundLeftEnable = FormTools.createCheckbox($("<td>"), "Bound left", o.boundLeft, function(val){
    o.boundLeft = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundRightEnable = FormTools.createCheckbox($("<td>"), "Bound right", o.boundRight, function(val){
    o.boundRight = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundLeftValue = $("<input>", { "type": "text", "width": "40px", "value": o.xmin });  
  $boundLeftValue.change(function(e){
    o.xmin = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundRightValue = $("<input>", { "type": "text", "width": "40px", "value": o.xmax });  
  $boundRightValue.change(function(e){
    o.xmax = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $boundLeftEnable, $("<td align=right>").append($boundLeftValue) ));
  $table.append($("<tr>").append( $boundRightEnable, $("<td align=right>").append($boundRightValue) ));  
  
  //CAPS
  //------------------------
  this.capStart.appendOptionsHTML($target, "Cap Start", figGeom, UIMethods);
  this.capEnd.appendOptionsHTML($target, "Cap End", figGeom, UIMethods);
  
  //FUNCTION
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Function"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //smoothing
  var $smoothing = FormTools.createCheckbox($("<span>"), "Smooth", o.smoothed, function(val){
    o.smoothed = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  //samples
  var $samples = $("<input>", { "type": "text", "width": "40px", "value": o.n });
  $samples.change(function(e){
    o.n = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("N: ", $samples),
    $("<td align=right>").append($smoothing)
  ));
  
  //function
  var $f = $("<input>", {"type": "text", "value": o.f});
  $f.change(function(e){
    o.f = $(this).val();
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $("<td colspan=2 align=right>")
    .append("<span class=infotext>JS syntax, e.g. Math.pow(x,4)</span>") )); 
  $table.append($("<tr>").append( $("<td colspan=2>").append($f) ));
  
  //PEN
  //------------------------
  o.pen.appendStrokeOptionsHTML($target, "Pen (Stroke)", figGeom, UIMethods);
};

