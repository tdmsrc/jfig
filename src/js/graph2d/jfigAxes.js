//defines JFigAxes
//see jfigCompound.js for info on compound objects 

//=======================================================
// AXES
//=======================================================

JFigAxes.prototype = new JFigCompound();
JFigAxes.prototype.constructor = JFigAxes;

function JFigAxes(){
  
  //children (e.g. graphs) created in getCustomToolbarButtons/loadRaw
  //should have onAxesChanged method to respond to axes modification
  //should set isChild = true; onDelete should call axes.removeChild
  this.children = [];
  
  //set default values
  this.boxOut = {x:[18,207], y:[18,135]}; //(3x2in) units: PT
  this.boxCoords = {x:[-3,6], y:[-2,4]};
  
  this.tickBigStep = [1,1];
  this.tickBigSize = 3;
  
  this.tickSmallStep = [0.25, 0.25];
  this.tickSmallSize = 2;
  
  this.grid = true;
  this.gridPen = new EPSPen();
  this.gridPen.dashed = true;
  this.gridPen.dashedFillUnits = 1;
  this.gridPen.dashedSpaceUnits = 2;
  
  this.vguides = [{x: 0, tickLeft: false, tickRight: false, pen: new EPSPen()}];
  this.hguides = [{y: 0, tickTop: false, tickBottom: false, pen: new EPSPen()}];
  
  this.outline = true;
  this.outlinePen = new EPSPen();
  this.outlineTicks = false;
  
  this.labelsShowX = true;
  this.labelsShowY = true;
  this.labelSkipZero = false;
  this.labelPadding = 3; //units: PT
  this.labelYPosition = JFigLabel.Align.LEFT; //center = position along axis
  this.labelXPosition = JFigLabel.Align.BOTTOM; //center = position along axis
};

//override "translate" to update boxOut
//-------------------------------------------------------
JFigAxes.prototype.translate = function(x,y){

  //translate boxOut
  this.boxOut.x[0] += x; this.boxOut.x[1] += x;
  this.boxOut.y[0] += y; this.boxOut.y[1] += y;
  
  //"super.translate(x,y)"
  JFigCompound.prototype.translate.call(this, x,y);
};

//push "refresh" to each child object
//-------------------------------------------------------
JFigAxes.prototype.pushAxesChanged = function(){
  //notify each child that axes changed (either boxOut or boxCoords)
  $.each(this.children, function(i,c){ 
    c.onAxesChanged(); 
  });
};

//add to list of children (used in getCustomToolbarButtons/loadRaw)
//-------------------------------------------------------
JFigAxes.prototype.addChild = function(C){
  var me = this;
  
  C.zsort = this.zsort; //TODO
  C.isChild = true;
  C.onDelete = function(){ me.removeChild(C); };
  
  this.children.push(C);
};

//[PRIVATE] remove form list of children: when creating a child graph, use in onDelete
//-------------------------------------------------------
JFigAxes.prototype.removeChild = function(C){
  var i = $.inArray(C,this.children);
  if(i > -1){ this.children.splice(i,1); }
};

//remove children from project when axes are removed
//-------------------------------------------------------
JFigAxes.prototype.onDelete = function(){

  //duplicate list of children first
  var toRemove = this.children.slice(0);
  
  $.each(toRemove, function(i,C){ 
    //removes child from project; as a side-effect,
    //child.onDelete and then axes.removeChild will be called
    C.removeFromContents();
  });
};

//defer refreshing until added; add already-existing children to project
//-------------------------------------------------------
JFigAxes.prototype.onAdd = function(UIMethods, figGeom){

  //refresh lines and labels
  this.refreshEPS();
  this.refreshLabels(figGeom);
  
  //add children
  $.each(this.children, function(i,c){ 
    UIMethods.addCompound(c);
  });
};

//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigAxes.prototype.saveRaw = function(){
  return{
    type: "JFigAxes",
    zsort: this.zsort,
    children: JFigAxes.saveRawChildren(this.children),
    
    boxOut: this.boxOut,
    boxCoords: this.boxCoords,
    
    tickBigStep: this.tickBigStep,
    tickBigSize: this.tickBigSize,
    
    tickSmallStep: this.tickSmallStep,
    tickSmallSize: this.tickSmallSize,
    
    grid: this.grid,
    gridPen: this.gridPen.saveRaw(),
    
    vguides: JFigAxes.saveRawVGuides(this.vguides),
    hguides: JFigAxes.saveRawHGuides(this.hguides),
    
    outline: this.outline,
    outlinePen: this.outlinePen.saveRaw(),
    outlineTicks: this.outlineTicks,
    
    labelsShowX: this.labelsShowX,
    labelsShowY: this.labelsShowY,
    labelSkipZero: this.labelSkipZero,
    labelPadding: this.labelPadding,
    labelYPosition: this.labelYPosition,
    labelXPosition: this.labelXPosition
  };
};

JFigAxes.saveRawChildren = function(children){
  var ret = [];
  
  $.each(children, function(i,child){ 
    ret.push(child.saveRaw());
  });  
  
  return ret;
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigAxes.loadRaw = function(raw){
  var ret = new JFigAxes(); 
  ret.zsort = raw.zsort;
  
  ret.boxOut = raw.boxOut;
  ret.boxCoords = raw.boxCoords;
  
  ret.tickBigStep = raw.tickBigStep;
  ret.tickBigSize = raw.tickBigSize;
  
  ret.tickSmallStep = raw.tickSmallStep;
  ret.tickSmallSize = raw.tickSmallSize;
  
  ret.grid = raw.grid;
  ret.gridPen = EPSPen.loadRaw(raw.gridPen);
  
  ret.vguides = JFigAxes.loadRawVGuides(raw.vguides);
  ret.hguides = JFigAxes.loadRawHGuides(raw.hguides);
  
  ret.outline = raw.outline;
  ret.outlinePen = EPSPen.loadRaw(raw.outlinePen);
  ret.outlineTicks = raw.outlineTicks;
  
  ret.labelsShowX = raw.labelsShowX;
  ret.labelsShowY = raw.labelsShowY;
  ret.labelSkipZero = raw.labelSkipZero;
  ret.labelPadding = raw.labelPadding;
  ret.labelYPosition = raw.labelYPosition;
  ret.labelXPosition = raw.labelXPosition;
  
  //children (i.e., graphs)
  ret.children = [];
  JFigAxes.loadRawChildren(ret, raw.children);
  
  return ret;
};

JFigAxes.loadRawChildren = function(axes, raw){
  
  //load according to "type", or error if "type" not found
  $.each(raw, function(i,C){
    var loadFunc;
    if(C.type !== undefined){ loadFunc = window[C.type]["loadRaw"]; }
    
    if(typeof(loadFunc) == typeof(Function)){
      var child = loadFunc(axes, C);
      axes.addChild(child);
    }else{
      console.log("JFigAxes child has unknown type: ");
      console.log(C);
    }
  });
};

//save/load guides
//-------------------------------------------------------
JFigAxes.saveRawVGuides = function(vguides){
  var ret = [];
  
  $.each(vguides, function(i,g){ 
    var gRaw = {
      x: g.x,
      tickLeft: g.tickLeft, tickRight: g.tickRight,
      pen: g.pen.saveRaw()
    };
    ret.push(gRaw);
  });
  
  return ret;
};

JFigAxes.saveRawHGuides = function(hguides){
  var ret = [];
  
  $.each(hguides, function(i,g){ 
    var gRaw = {
      y: g.y,
      tickTop: g.tickTop, tickBottom: g.tickBottom,
      pen: g.pen.saveRaw()
    };
    ret.push(gRaw);
  });  
  
  return ret;
};

JFigAxes.loadRawVGuides = function(raw){
  var ret = [];
  
  $.each(raw, function(i,gRaw){ 
    var gRet = {
      x: gRaw.x,
      tickLeft: gRaw.tickLeft, tickRight: gRaw.tickRight,
      pen: EPSPen.loadRaw(gRaw.pen)
    };
    ret.push(gRet);
  });
  
  return ret;
};

JFigAxes.loadRawHGuides = function(raw){
  var ret = [];
  
  $.each(raw, function(i,gRaw){ 
    var gRet = {
      y: gRaw.y,
      tickTop: gRaw.tickTop, tickBotom: gRaw.tickBotom,
      pen: EPSPen.loadRaw(gRaw.pen)
    };
    ret.push(gRet);
  });
  
  return ret;
};


//=======================================================
// UTILITY METHODS
//=======================================================

//convert "boxCoords" coordinates to "boxOut" coordinates
//-------------------------------------------------------
JFigAxes.prototype.coordsToEPSx = function(x){

  var coordsOff = x - this.boxCoords.x[0];
  var coordsW = this.boxCoords.x[1] - this.boxCoords.x[0];
  var outW = this.boxOut.x[1] - this.boxOut.x[0];
  
  return (outW * coordsOff) / coordsW + this.boxOut.x[0];
};

JFigAxes.prototype.coordsToEPSy = function(y){

  var coordsOff = y - this.boxCoords.y[0];
  var coordsH = this.boxCoords.y[1] - this.boxCoords.y[0];
  var outH = this.boxOut.y[1] - this.boxOut.y[0];
  
  return (outH * coordsOff) / coordsH + this.boxOut.y[0];
};

JFigAxes.prototype.coordsToEPS = function(p){
  return [ this.coordsToEPSx(p[0]), this.coordsToEPSy(p[1]) ];
};

//check if point visible (p = [x,y], units in boxCoords)
//-------------------------------------------------------
JFigAxes.prototype.isVisible = function(p){
  return !(
    (this.boxCoords.x[0] > p[0]) ||
    (p[0] > this.boxCoords.x[1]) ||
    (this.boxCoords.y[0] > p[1]) ||
    (p[1] > this.boxCoords.y[1])
  );
};

//p,q = [x,y] (units in boxCoords); return intersection of pq with axes boundary
//assume: p visible; q.x > p.x and q.y is out of bounds
//-------------------------------------------------------
JFigAxes.prototype.clip = function(p, q){

  //q below axes
  if(this.boxCoords.y[0] > q[1]){ 
    //parameterization p + t(q-p): find t so y = boxCoords.y[0]
    var t = (this.boxCoords.y[0]-p[1]) / (q[1]-p[1]);
    //return point along parameterization
    return [ p[0] + t*(q[0]-p[0]), p[1] + t*(q[1]-p[1]) ];
  }
  
  //q above axes
  else if(q[1] > this.boxCoords.y[1]){ 
    //parameterization p + t(q-p): find t so y = boxCoords.y[1]
    var t = (this.boxCoords.y[1]-p[1]) / (q[1]-p[1]);
    //return point along parameterization
    return [ p[0] + t*(q[0]-p[0]), p[1] + t*(q[1]-p[1]) ];
  }
  
  //invalid input; return q
  else{ return q; }
};


//=======================================================
// SUB-OBJECT CREATION (labels and EPS objects)
//=======================================================

//call body(x) for each multiple of "step" in the x-range
//-------------------------------------------------------
JFigAxes.prototype.loopX = function(step, body){

  var sx = step * Math.ceil(this.boxCoords.x[0]/step);
  for(i=0; i<1000; i++){ 
    if(sx > this.boxCoords.x[1]){ break; }
    body(sx); sx += step;
  }
};

//call body(y) for each multiple of "step" in the y-range
//-------------------------------------------------------
JFigAxes.prototype.loopY = function(step, body){

  var sy = step * Math.ceil(this.boxCoords.y[0]/step);  
  for(i=0; i<1000; i++){ 
    if(sy > this.boxCoords.y[1]){ break; }
    body(sy); sy += step;
  }
};

//add vertical line on axes at boxCoords position x, with optional ticks
//-------------------------------------------------------
JFigAxes.prototype.addVGuide = function(x, tickLeft, tickRight, pen){
  var gEPS = new EPSPath();
  gEPS.pen = pen;
  gEPS.points.push(
    this.coordsToEPS( [x, this.boxCoords.y[0]] ),
    this.coordsToEPS( [x, this.boxCoords.y[1]] ) 
  );
  this.addObject(gEPS);
  
  //ticks
  if(!(tickLeft || tickRight)){ return; }
  var me = this;
  
  //small ticks
  this.loopY(this.tickSmallStep[1], function(y){
    var tickPath = new EPSPath();
    tickPath.pen = pen;
    
    var p = me.coordsToEPS([x,y]);
    tickPath.points.push(
      [p[0] - (tickLeft  ? me.tickSmallSize : 0), p[1]],
      [p[0] + (tickRight ? me.tickSmallSize : 0), p[1]]
    );
    me.addObject(tickPath);
  });
  
  //big ticks
  this.loopY(this.tickBigStep[1], function(y){
    var tickPath = new EPSPath();
    tickPath.pen = pen;
    
    var p = me.coordsToEPS([x,y]);
    tickPath.points.push(
      [p[0] - (tickLeft  ? me.tickBigSize : 0), p[1]],
      [p[0] + (tickRight ? me.tickBigSize : 0), p[1]]
    );
    me.addObject(tickPath);
  });
};

//add horizontal line on axes at boxCoords position y, with optional ticks
//-------------------------------------------------------
JFigAxes.prototype.addHGuide = function(y, tickTop, tickBottom, pen){
  var gEPS = new EPSPath();
  gEPS.pen = pen;
  gEPS.points.push(
    this.coordsToEPS( [this.boxCoords.x[0], y] ),
    this.coordsToEPS( [this.boxCoords.x[1], y] ) 
  );
  this.addObject(gEPS);
  
  //ticks
  if(!(tickTop || tickBottom)){ return; }
  var me = this;
  
  //small ticks
  this.loopX(this.tickSmallStep[0], function(x){
    var tickPath = new EPSPath();
    tickPath.pen = pen;
    
    var p = me.coordsToEPS([x,y]);
    tickPath.points.push(
      [p[0], p[1] - (tickBottom ? me.tickSmallSize : 0)],
      [p[0], p[1] + (tickTop    ? me.tickSmallSize : 0)]
    );
    me.addObject(tickPath);
  });
  
  //big ticks
  this.loopX(this.tickBigStep[0], function(x){
    var tickPath = new EPSPath();
    tickPath.pen = pen;
    
    var p = me.coordsToEPS([x,y]);
    tickPath.points.push(
      [p[0], p[1] - (tickBottom ? me.tickBigSize : 0)],
      [p[0], p[1] + (tickTop    ? me.tickBigSize : 0)]
    );
    me.addObject(tickPath);
  });
};

//recreate outline, guides, and grid
//-------------------------------------------------------
JFigAxes.prototype.refreshEPS = function(){
  this.clearObjects();
  var me = this;
  
  //grid
  if(this.grid){
    //vertical grid lines
    this.loopX(this.tickBigStep[0], function(x){
      me.addVGuide(x, false, false, me.gridPen);
    });
    
    //horizontal grid lines
    this.loopY(this.tickBigStep[1], function(y){
      me.addHGuide(y, false, false, me.gridPen);
    });
  }
  
  //guides
  $.each(this.vguides, function(i,g){ 
    //continue if the guide is out of range
    if( (g.x < me.boxCoords.x[0]) || (g.x > me.boxCoords.x[1]) ){ return true; }
    me.addVGuide(g.x, g.tickLeft, g.tickRight, g.pen);
  });
  
  $.each(this.hguides, function(i,g){ 
    //continue if the guide is out of range
    if( (g.y < me.boxCoords.y[0]) || (g.y > me.boxCoords.y[1]) ){ return true; }
    me.addHGuide(g.y, g.tickTop, g.tickBottom, g.pen);
  });
  
  //outline
  if(this.outline){
    this.addVGuide(this.boxCoords.x[0], false, this.outlineTicks, this.outlinePen);
    this.addVGuide(this.boxCoords.x[1], this.outlineTicks, false, this.outlinePen);
    this.addHGuide(this.boxCoords.y[0], this.outlineTicks, false, this.outlinePen);
    this.addHGuide(this.boxCoords.y[1], false, this.outlineTicks, this.outlinePen);
  }
};

//recreate labels
//-------------------------------------------------------
//add label for x-axis, with given caption, at boxCoords position (x,y)
JFigAxes.prototype.addXLabel = function(x, y, caption, figGeom){

  var label = new JFigLabel();
  label.size = 8;
  label.halign = JFigLabel.Align.CENTER;
  label.valign = JFigLabel.Align.TOP;
  if(this.labelXPosition == JFigLabel.Align.TOP){
    label.valign = JFigLabel.Align.BOTTOM;
  }
  label.text = caption;
  label.latex = true;
  
  label.p = this.coordsToEPS( [x,y] );
  if(label.valign == JFigLabel.Align.TOP){ label.p[1] -= this.labelPadding; }
  if(label.valign == JFigLabel.Align.BOTTOM){ label.p[1] += this.labelPadding; }
  
  this.addLabel(label, figGeom);
};

//add label for y-axis, with given caption, at boxCoords position (x,y)
JFigAxes.prototype.addYLabel = function(x, y, caption, figGeom){

  var label = new JFigLabel();
  label.size = 8;
  label.valign = JFigLabel.Align.CENTER;
  label.halign = JFigLabel.Align.RIGHT;
  if(this.labelYPosition == JFigLabel.Align.RIGHT){
    label.halign = JFigLabel.Align.LEFT;
  }
  label.text = caption;
  label.latex = true;
  
  label.p = this.coordsToEPS( [x,y] );
  if(label.halign == JFigLabel.Align.RIGHT){ label.p[0] -= this.labelPadding; }
  if(label.halign == JFigLabel.Align.LEFT){ label.p[0] += this.labelPadding; }
  
  this.addLabel(label, figGeom);
};

//recreate all labels
JFigAxes.prototype.refreshLabels = function(figGeom){
  this.clearLabels();
  var me = this;
  
  //x-value labels
  if(this.labelsShowX){
    var ly = 0;
    if(this.labelXPosition == JFigLabel.Align.TOP){
      ly = this.boxCoords.y[1];
    }else if(this.labelXPosition == JFigLabel.Align.BOTTOM){
      ly = this.boxCoords.y[0];
    }else{
      if(this.boxCoords.y[0] > 0){ ly = this.boxCoords.y[0]; }
      else if(this.boxCoords.y[1] < 0){ ly = this.boxCoords.y[1]; }
    }
    
    this.loopX(this.tickBigStep[0], function(x){
      if(me.labelSkipZero && (x == 0)){ return; }
      me.addXLabel(x, ly, "" + x, figGeom);
    });
  }
  
  //y-value labels
  if(this.labelsShowY){
    var lx = 0;
    if(this.labelYPosition == JFigLabel.Align.LEFT){
      lx = this.boxCoords.x[0];
    }else if(this.labelYPosition == JFigLabel.Align.RIGHT){
      lx = this.boxCoords.x[1];
    }else{
      if(this.boxCoords.x[0] > 0){ lx = this.boxCoords.x[0]; }
      else if(this.boxCoords.x[1] < 0){ lx = this.boxCoords.x[1]; }
    }
    
    this.loopY(this.tickBigStep[1], function(y){
      if(me.labelSkipZero && (y == 0)){ return; }
      me.addYLabel(lx, y, "" + y, figGeom);
    });
  }
};


//=======================================================
// UI
//=======================================================

JFigAxes.caption = "Axes (2D)";
JFigAxes.icon = "image/graph2d/object-axes.png";
JFigAxes.prototype.caption = JFigAxes.caption;
JFigAxes.prototype.icon = JFigAxes.icon;

//objects
//-------------------------------------------------------
JFigAxes.prototype.getObjectList = function(indent){
  var ret = [{ indent: indent, icon: JFigAxes.icon, caption: JFigAxes.caption, object: this }];
  
  $.each(this.children, function(i,c){ 
    if(typeof(c.getObjectList) == typeof(Function)){ 
      ret = ret.concat(c.getObjectList(indent+1));
    }
  });
  
  return ret;
};

//toolbar
//-------------------------------------------------------
JFigAxes.prototype.getCustomToolbarButtons = function(UIMethods){
  var me = this;
  
  return [
    { caption: "Create " + JFigGraph.caption, icon: JFigGraph.icon, action: function(){
      var graph = new JFigGraph(me, "x*(x+1)*(x-1)");
      me.addChild(graph, UIMethods, true);
      UIMethods.addCompound(graph, true);
    }},
    
    { caption: "Create " + JFigGraphImplicit.caption, icon: JFigGraphImplicit.icon, action: function(){
      var graph = new JFigGraphImplicit(me, "x*x+y*y-4");
      me.addChild(graph, UIMethods, true);
      UIMethods.addCompound(graph, true);
    }},
    
    { caption: "Create " + JFigGraphDiscrete.caption, icon: JFigGraphDiscrete.icon, action: function(){
      var graph = new JFigGraphDiscrete(me, "4/(1+x*x)");
      me.addChild(graph, UIMethods, true);
      UIMethods.addCompound(graph, true);
    }},
    
    { caption: "Create " + JFigGraphBars.caption, icon: JFigGraphBars.icon, action: function(){
      var graph = new JFigGraphBars(me, "4/(1+x*x)");
      me.addChild(graph, UIMethods, true);
      UIMethods.addCompound(graph, true);
    }}
  ];
};

//anchors
//-------------------------------------------------------
JFigAxes.prototype.getAnchors = function(figGeom, UIMethods){
  var me = this;
  
  //center of axes output box (units: PT)
  var cx = (this.boxOut.x[0] + this.boxOut.x[1]) / 2;
  var cy = (this.boxOut.y[0] + this.boxOut.y[1]) / 2;
  
  //anchors
  var anchorC = new UIAnchor("image/arrow-move.png", [ cx, cy ]);
  var anchorBL = new UIAnchor("image/arrow-resize-045.png", [ this.boxOut.x[0], this.boxOut.y[0] ]);
  var anchorBR = new UIAnchor("image/arrow-resize-135.png", [ this.boxOut.x[1], this.boxOut.y[0] ]);
  var anchorTR = new UIAnchor("image/arrow-resize-045.png", [ this.boxOut.x[1], this.boxOut.y[1] ]);
  var anchorTL = new UIAnchor("image/arrow-resize-135.png", [ this.boxOut.x[0], this.boxOut.y[1] ]);
  var anchorL = new UIAnchor("image/arrow-resize-h.png",   [ this.boxOut.x[0], cy ]);
  var anchorR = new UIAnchor("image/arrow-resize-h.png",   [ this.boxOut.x[1], cy ]);
  var anchorB = new UIAnchor("image/arrow-resize-v.png",   [ cx, this.boxOut.y[0] ]);
  var anchorT = new UIAnchor("image/arrow-resize-v.png",   [ cx, this.boxOut.y[1] ]);
  
  //anchor update
  var onDragEndCommon = function(){
  
    me.pushAxesChanged(); 
    me.refreshEPS(); me.refreshLabels(figGeom); 
    UIMethods.changedEPS(); UIMethods.changedLabels();
    
    var cx = (me.boxOut.x[0] + me.boxOut.x[1]) / 2;
    var cy = (me.boxOut.y[0] + me.boxOut.y[1]) / 2;
    anchorC.p = [ cx, cy ];
    anchorBL.p = [ me.boxOut.x[0], me.boxOut.y[0] ];
    anchorBR.p = [ me.boxOut.x[1], me.boxOut.y[0] ];
    anchorTR.p = [ me.boxOut.x[1], me.boxOut.y[1] ];
    anchorTL.p = [ me.boxOut.x[0], me.boxOut.y[1] ];
    anchorL.p = [ me.boxOut.x[0], cy ];
    anchorR.p = [ me.boxOut.x[1], cy ];
    anchorB.p = [ cx, me.boxOut.y[0] ];
    anchorT.p = [ cx, me.boxOut.y[1] ];
    
    UIAnchor.updateCSS(figGeom);
  };
  
  //anchor event handlers
  anchorC.onDragEnd = function(p){
    var dx = p[0] - (me.boxOut.x[0] + me.boxOut.x[1]) / 2;
    var dy = p[1] - (me.boxOut.y[0] + me.boxOut.y[1]) / 2;
    me.boxOut.x[0] += dx; me.boxOut.x[1] += dx;
    me.boxOut.y[0] += dy; me.boxOut.y[1] += dy;
    onDragEndCommon();
  };
  
  anchorBL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorBR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorTR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
  
  anchorTL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
  
  anchorL.onDragEnd = function(p){
    me.boxOut.x[0] = p[0];
    onDragEndCommon();
  };
  
  anchorR.onDragEnd = function(p){
    me.boxOut.x[1] = p[0];
    onDragEndCommon();
  };
  
  anchorB.onDragEnd = function(p){
    me.boxOut.y[0] = p[1];
    onDragEndCommon();
  };
  
  anchorT.onDragEnd = function(p){
    me.boxOut.y[1] = p[1];
    onDragEndCommon();
  };
};

//options
//-------------------------------------------------------
JFigAxes.prototype._updateNoPush = function(figGeom, UIMethods){
  this.refreshEPS(); 
  this.refreshLabels(figGeom); 
  
  UIMethods.changedEPS(); 
  UIMethods.changedLabels();  
};

JFigAxes.prototype._updateAll = function(figGeom, UIMethods){
  this.pushAxesChanged(); 
  this._updateNoPush(figGeom, UIMethods);
};

JFigAxes.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table, $subtitleExtra;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigAxes.icon + "> AXES (2D)"));
    
  //OUTLINE
  //------------------------
  $subtitleExtra = $("<span>");
  $target.append($("<div class=panel-subtitle>").append("Outline", $subtitleExtra));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  FormTools.createCheckbox($subtitleExtra, "Show", o.outline, function(val){
    o.outline = val; 
    o.refreshEPS(); UIMethods.changedEPS();
  });

  //RANGE
  //------------------------
  o.appendRangeOptionsHTML($target, "Range", figGeom, UIMethods);
  
  //TICKS
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Ticks"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  $table.append($("<tr>").append( $("<td colspan=2>").append("<span class=infotext>Labels and grid occur at \"big ticks\"</span>") ));
  
  var $tickBigX = $("<input>", {"type": "text", "width": "40px", "value": o.tickBigStep[0]});  
  $tickBigX.change(function(e){
    o.tickBigStep[0] = parseFloat($(this).val());
    o._updateNoPush(figGeom, UIMethods);
  });
  
  var $tickBigY = $("<input>", {"type": "text", "width": "40px", "value": o.tickBigStep[1]});  
  $tickBigY.change(function(e){
    o.tickBigStep[1] = parseFloat($(this).val());
    o._updateNoPush(figGeom, UIMethods);
  });
  
  var $tickSmallX = $("<input>", {"type": "text", "width": "40px", "value": o.tickSmallStep[0]});  
  $tickSmallX.change(function(e){
    o.tickSmallStep[0] = parseFloat($(this).val());
    o._updateNoPush(figGeom, UIMethods);
  });
  
  var $tickSmallY = $("<input>", {"type": "text", "width": "40px", "value": o.tickSmallStep[1]});  
  $tickSmallY.change(function(e){
    o.tickSmallStep[1] = parseFloat($(this).val());
    o._updateNoPush(figGeom, UIMethods);
  });
  
  $table.append($("<tr>").append(
    $("<td>").append("Big:"), 
    $("<td align=right>").append("x: ", $tickBigX, " y: ", $tickBigY)
  ));
  
  $table.append($("<tr>").append(
    $("<td>").append("Small:"), 
    $("<td align=right>").append("x: ", $tickSmallX, " y: ", $tickSmallY)
  ));
  
  //GRID
  //------------------------
  $subtitleExtra = $("<span>");
  $target.append($("<div class=panel-subtitle>").append("Grid", $subtitleExtra));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  FormTools.createCheckbox($subtitleExtra, "Show", o.grid, function(val){
    o.grid = val; 
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  //LABELS
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Labels"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));

  //show and skip0  
  var $labels = $("<td colspan=2 align=right>");
  FormTools.createCheckbox($labels, "Show X", o.labelsShowX, function(val){
    o.labelsShowX = val; 
    o.refreshLabels(figGeom); UIMethods.changedLabels();  
  });
  FormTools.createCheckbox($labels, "Show Y", o.labelsShowY, function(val){
    o.labelsShowY = val; 
    o.refreshLabels(figGeom); UIMethods.changedLabels();  
  });
  FormTools.createCheckbox($labels, "Skip 0", o.labelSkipZero, function(val){
    o.labelSkipZero = val; 
    o.refreshLabels(figGeom); UIMethods.changedLabels();  
  });
  $table.append($("<tr>").append($labels));
  
  //Y-label position
  var $labelYPosition = FormTools.createRadioGroup($("<td align=right>"), "yposition",
    { caption: "<img src=image/halign-left.png>", selected: (o.labelYPosition == JFigLabel.Align.LEFT),
      onSelect: function(){
        o.labelYPosition = JFigLabel.Align.LEFT;
        o.refreshLabels(figGeom); UIMethods.changedLabels();
      }},
    { caption: "<img src=image/halign-center.png>", selected: (o.labelYPosition == JFigLabel.Align.CENTER),
      onSelect: function(){
        o.labelYPosition = JFigLabel.Align.CENTER;
        o.refreshLabels(figGeom); UIMethods.changedLabels();  
      }},
    { caption: "<img src=image/halign-right.png>", selected: (o.labelYPosition == JFigLabel.Align.RIGHT), 
      onSelect: function(){
        o.labelYPosition = JFigLabel.Align.RIGHT;
        o.refreshLabels(figGeom); UIMethods.changedLabels();  
      }}
  );
  $table.append($("<tr>").append( $("<td>").append("Position (Y):"), $labelYPosition ));
  
  //X-label position
  var $labelXPosition = FormTools.createRadioGroup($("<td align=right>"), "xposition",
    { caption: "<img src=image/valign-top.png>", selected: (o.labelXPosition == JFigLabel.Align.TOP),
      onSelect: function(){
        o.labelXPosition = JFigLabel.Align.TOP;
        o.refreshLabels(figGeom); UIMethods.changedLabels();  
      }},
    { caption: "<img src=image/valign-center.png>", selected: (o.labelXPosition == JFigLabel.Align.CENTER),
      onSelect: function(){
        o.labelXPosition = JFigLabel.Align.CENTER;
        o.refreshLabels(figGeom); UIMethods.changedLabels();  
      }},
    { caption: "<img src=image/valign-bottom.png>", selected: (o.labelXPosition == JFigLabel.Align.BOTTOM), 
      onSelect: function(){
        o.labelXPosition = JFigLabel.Align.BOTTOM;
        o.refreshLabels(figGeom); UIMethods.changedLabels();  
      }}
  );
  $table.append($("<tr>").append( $("<td>").append("Position (X):"), $labelXPosition ));
  
  /*//GUIDES (H)
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Guides (Horizontal)"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //GUIDES (V)
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Guides (Vertical)"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));*/
};

JFigAxes.prototype.appendRangeOptionsHTML = function($target, caption, figGeom, UIMethods){
  var o = this;
  
  //RANGE
  //------------------------
  $target.append($("<div class=panel-subtitle>").append(caption));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $rangeMinX = $("<input>", {"type": "text", "width": "40px", "value": o.boxCoords.x[0]});  
  $rangeMinX.change(function(e){
    o.boxCoords.x[0] = parseFloat($(this).val());
    o._updateAll(figGeom, UIMethods);
  });
  
  var $rangeMaxX = $("<input>", {"type": "text", "width": "40px", "value": o.boxCoords.x[1]});  
  $rangeMaxX.change(function(e){
    o.boxCoords.x[1] = parseFloat($(this).val());
    o._updateAll(figGeom, UIMethods);
  });
  
  var $rangeMinY = $("<input>", {"type": "text", "width": "40px", "value": o.boxCoords.y[0]});  
  $rangeMinY.change(function(e){
    o.boxCoords.y[0] = parseFloat($(this).val());
    o._updateAll(figGeom, UIMethods);
  });
  
  var $rangeMaxY = $("<input>", {"type": "text", "width": "40px", "value": o.boxCoords.y[1]});  
  $rangeMaxY.change(function(e){
    o.boxCoords.y[1] = parseFloat($(this).val());
    o._updateAll(figGeom, UIMethods);
  });
  
  $table.append($("<tr>").append(
    $("<td>").append("x:"), 
    $("<td align=right>").append($rangeMinX, " to ", $rangeMaxX)
  ));
  $table.append($("<tr>").append(
    $("<td>").append("y:"), 
    $("<td align=right>").append($rangeMinY, " to ", $rangeMaxY)
  ));
};

