//defines JFigGraphDiscrete
//see jfigCompound.js for info on compound objects 

//=======================================================
// GRAPH (DISCRETE)
//=======================================================

JFigGraphDiscrete.prototype = new JFigCompound();
JFigGraphDiscrete.prototype.constructor = JFigGraphDiscrete;

function JFigGraphDiscrete(axes, f){
  this.axes = axes;  
  this.f = f;
    
  //set default values
  this.boundLeft = false;
  this.xmin = this.axes.boxCoords.x[0];
  this.boundRight = false;
  this.xmax = this.axes.boxCoords.x[1];
  
  this.xstep = 1;
  
  this.radius = 2; //units: PT  
  this.pen = new EPSPen();
  this.filled = true;
};

//defer refreshing until added
//-------------------------------------------------------
JFigGraphDiscrete.prototype.onAdd = function(UIMethods, figGeom){
  //refresh path
  this.refreshEPS();
};

//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigGraphDiscrete.prototype.saveRaw = function(){
  var ret = {
    type: "JFigGraphDiscrete",
    f: this.f,
    zsort: this.zsort,
    
    xstep: this.xstep,
    
    radius: this.radius,
    pen: this.pen.saveRaw(),
    filled: this.filled
  };
  
  //conditional properties
  ret.boundLeft = this.boundLeft;
  if(this.boundLeft){ ret.xmin = this.xmin; }
  
  ret.boundRight = this.boundRight;
  if(this.boundRight){ ret.xmax = this.xmax; }
  
  return ret;
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigGraphDiscrete.loadRaw = function(axes, raw){
  var ret = new JFigGraphDiscrete(axes, raw.f); 
  ret.zsort = raw.zsort;
  
  ret.xstep = raw.xstep;
  
  ret.radius = raw.radius;
  ret.pen = EPSPen.loadRaw(raw.pen);
  ret.filled = raw.filled;
  
  //conditional properties
  ret.boundLeft = raw.boundLeft;
  if(ret.boundLeft){ ret.xmin = raw.xmin; }
  
  ret.boundRight = raw.boundRight;
  if(ret.boundRight){ ret.xmax = raw.xmax; }
  
  return ret;
};


//=======================================================
// PATH CREATION
//=======================================================

//respond to axes update
//-----------------------------------------------
JFigGraphDiscrete.prototype.onAxesChanged = function(){
  this.refreshEPS();
  
  if(!this.boundLeft){ this.xmin = this.axes.boxCoords.x[0]; }
  if(!this.boundRight){ this.xmax = this.axes.boxCoords.x[1]; }
};

//recreate outline, guides, and grid
//-------------------------------------------------------
JFigGraphDiscrete.prototype.refreshEPS = function(){
  this.clearObjects();
  
  //sample x-value min/max
  var x0 = this.axes.boxCoords.x[0];
  var x1 = this.axes.boxCoords.x[1];
  
  if(this.boundLeft){
    if(this.xmin > this.axes.boxCoords.x[1]){ return; }
    if(this.xmin > this.axes.boxCoords.x[0]){ x0 = this.xmin; }
  }
  
  if(this.boundRight){
    if(this.xmax < this.axes.boxCoords.x[0]){ return; }
    if(this.xmax < this.axes.boxCoords.x[1]){ x1 = this.xmax; }
  }
  
  //create graph dots
  eval("var f = function(x){ return " + this.f + "; };");
  
  var k0 = Math.ceil(x0/this.xstep);
  var k1 = Math.floor(x1/this.xstep);
  
  for(var i=k0; i<=k1; i++){
    var x = i*this.xstep;
    var p = [x, f(x)];
    var v = this.axes.isVisible(p);
    if(!v){ continue; }
    
    var dot = new EPSArc();
    
    dot.center = this.axes.coordsToEPS(p);
    dot.radius = this.radius;
    
    dot.angleStart = 0;
    dot.angleEnd = 360;
    dot.closed = true;
    dot.closeType = EPSArc.CloseType.SEGMENT;
    dot.filled = this.filled;
    
    dot.pen = this.pen;
    
    this.addObject(dot);
  }
};


//=======================================================
// UI
//=======================================================

JFigGraphDiscrete.caption = "Graph (Discrete)";
JFigGraphDiscrete.icon = "image/graph2d/object-graph-discr.png";
JFigGraphDiscrete.prototype.caption = JFigGraphDiscrete.caption;
JFigGraphDiscrete.prototype.icon = JFigGraphDiscrete.icon;

//objects
//-------------------------------------------------------
JFigGraphDiscrete.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: JFigGraphDiscrete.icon, caption: JFigGraphDiscrete.caption, object: this }];
};

//options
//-------------------------------------------------------
JFigGraphDiscrete.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigGraphDiscrete.icon + "> GRAPH (Discrete)"));
  
  //PARENT AXES
  //------------------------
  o.axes.appendRangeOptionsHTML($target, "Axes", figGeom, UIMethods);
  
  //BOUNDS
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Bounds"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  var $boundLeftEnable = FormTools.createCheckbox($("<td>"), "Bound left", o.boundLeft, function(val){
    o.boundLeft = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundRightEnable = FormTools.createCheckbox($("<td>"), "Bound right", o.boundRight, function(val){
    o.boundRight = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundLeftValue = $("<input>", { "type": "text", "width": "40px", "value": o.xmin });  
  $boundLeftValue.change(function(e){
    o.xmin = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $boundRightValue = $("<input>", { "type": "text", "width": "40px", "value": o.xmax });  
  $boundRightValue.change(function(e){
    o.xmax = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $boundLeftEnable, $("<td align=right>").append($boundLeftValue) ));
  $table.append($("<tr>").append( $boundRightEnable, $("<td align=right>").append($boundRightValue) ));  
  
  //FUNCTION
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Function"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //samples
  var $xstep = $("<input>", { "type": "text", "width": "40px", "value": o.xstep });
  $xstep.change(function(e){
    o.xstep = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("Step: ", $xstep)
  ));
  
  //function
  var $f = $("<input>", {"type": "text", "value": o.f});
  $f.change(function(e){
    o.f = $(this).val();
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $("<td colspan=2 align=right>")
    .append("<span class=infotext>JS syntax, e.g. Math.pow(x,4)</span>") )); 
  $table.append($("<tr>").append( $("<td colspan=2>").append($f) ));
  
  //PEN
  //------------------------
  o.pen.appendStrokeOptionsHTML($target, "Pen (Stroke)", figGeom, UIMethods);
  o.pen.appendFillOptionsHTML($target, "Pen (Fill)", figGeom, UIMethods, 
    //getFilled
    function(){ return o.filled; }, 
    //setFilled
    function(val){ 
      o.filled = val;
      o.refreshEPS(); 
    }
  );
};

