//defines JFigGraphImplicit
//see jfigCompound.js for info on compound objects 

//=======================================================
// GRAPH (IMPLICIT)
//=======================================================

JFigGraphImplicit.prototype = new JFigCompound();
JFigGraphImplicit.prototype.constructor = JFigGraphImplicit;

function JFigGraphImplicit(axes, f){
  this.axes = axes;  
  this.f = f;
  
  //set default values
  this.capBoundary = new EPSPathCap();
  this.capBoundary.enabled = true;
  
  this.n = [40,40]; //[nx, ny]
  this.smoothed = false;
  this.smoothAmount = 1; //between 0 and 1
  this.pen = new EPSPen();
};

//defer refreshing until added
//-------------------------------------------------------
JFigGraphImplicit.prototype.onAdd = function(UIMethods, figGeom){
  //refresh path
  this.refreshEPS();
};

//=======================================================
// SAVE/LOAD
//=======================================================

//save as an object: the instance should be recoverable by passing the return value to loadRaw
//-----------------------------------------------
JFigGraphImplicit.prototype.saveRaw = function(){
  return {
    type: "JFigGraphImplicit",
    f: this.f,
    zsort: this.zsort,
    
    capBoundary: this.capBoundary.saveRaw(),
    
    n: this.n,    
    smoothed: this.smoothed,
    smoothAmount: this.smoothAmount,
    pen: this.pen.saveRaw()
  };
};

//create instance from data returned by saveRaw
//-----------------------------------------------
JFigGraphImplicit.loadRaw = function(axes, raw){
  var ret = new JFigGraphImplicit(axes, raw.f);
  ret.zsort = raw.zsort;
  
  ret.capBoundary = EPSPathCap.loadRaw(raw.capBoundary);
  
  ret.n = raw.n;
  ret.smoothed = raw.smoothed;
  ret.smoothAmount = raw.smoothAmount;
  ret.pen = EPSPen.loadRaw(raw.pen);
  
  return ret;
};


//=======================================================
// PATH CREATION
//=======================================================

//respond to axes update
//-----------------------------------------------
JFigGraphImplicit.prototype.onAxesChanged = function(){
  this.refreshEPS();
};

//recreate outline, guides, and grid
//-------------------------------------------------------
JFigGraphImplicit.prototype.refreshEPS = function(){
  this.clearObjects();
  
  //store sample counts
  var nx = this.n[0]; var ny = this.n[1];
  
  //shortcuts for boxCoords and (sample index) -> (coord position)
  var x0 = this.axes.boxCoords.x[0]; var x1 = this.axes.boxCoords.x[1];
  var y0 = this.axes.boxCoords.y[0]; var y1 = this.axes.boxCoords.y[1];
  var ps = function(i,j){
    return [ (x1-x0)*i/(nx-1) + x0, (y1-y0)*j/(ny-1) + y0 ];
  };
  
  //prepare function for evaluation
  eval("var f = function(x,y){ return " + this.f + "; };");
  
  //===================================
  //I. GRID COMBINATORICS
  //===================================
  var gridV = []; //properties: boundary, edgeLeft,Right,Up,Down (incident gridE objects)
  var gridE = []; //properties: boundary, v0 v1, endpoints (gridV objects)
  var gridR = []; //properties: v00 v01 v10 v11 (gridV), edgeLeft,Right,Up,Down (gridE)
  
  //GRID VERTICES
  //------------------------
  var getGridV = function(i,j){ return gridV[i*ny + j]; }
  
  for(var i=0; i<nx; i++){  var boundaryX = (i == 0) || (i == (nx-1));
  for(var j=0; j<ny; j++){  var boundaryY = (j == 0) || (j == (ny-1));
    //create grid vertex
    var newV = {}; gridV.push(newV);
    
    //position of vertex (sample coords) and function value
    newV.fp = ps(i,j);
    newV.fval = f(newV.fp[0], newV.fp[1]);
    newV.boundary = boundaryX || boundaryY;
  }}
  
  //GRID EDGES
  //------------------------
  //horiz
  for(var i=0; i<nx-1; i++){ 
  for(var j=0; j<ny; j++){
    //create grid edge
    var newE = {}; gridE.push(newE);
    
    //add vertex refs to edge and vice-versa
    newE.v0 = getGridV(i  ,j);  newE.v0.edgeRight = newE;
    newE.v1 = getGridV(i+1,j);  newE.v1.edgeLeft  = newE;
    newE.boundary = (j == 0) || (j == (ny-1));
  }}
  
  //vertical
  for(var i=0; i<nx; i++){
  for(var j=0; j<ny-1; j++){
    //create grid edge
    var newE = {}; gridE.push(newE);
    
    //add vertex refs to edge and vice-versa
    newE.v0 = getGridV(i,j  );  newE.v0.edgeUp   = newE;
    newE.v1 = getGridV(i,j+1);  newE.v1.edgeDown = newE;
    newE.boundary = (i == 0) || (i == (nx-1));
  }}
  
  //GRID RECTS
  //------------------------
  for(var i=0; i<nx-1; i++){
  for(var j=0; j<ny-1; j++){
    //create grid rect
    var newR = {}; gridR.push(newR);
    
    //vertex refs
    newR.v01 = getGridV(i  ,j+1); newR.v11 = getGridV(i+1,j+1);
    newR.v00 = getGridV(i  ,j  ); newR.v10 = getGridV(i+1,j  );
    //edge refs
    newR.edgeDown = newR.v00.edgeRight; newR.edgeUp    = newR.v01.edgeRight;
    newR.edgeLeft = newR.v00.edgeUp;    newR.edgeRight = newR.v10.edgeUp;
  }}
  
  //===================================
  //II. GRAPH COMBINATORICS
  //===================================
  var graphV = []; //properties: boundary, pFIG (point in PT units), edges (array of graphE)
  var graphE = []; //properties: verts (array of graphV)
  
  //GRAPH POINTS ON GRID VERTS/EDGES
  //------------------------
  var EPSILON = 0.01;
  
  //check vertices (tolerance)
  for(var i=0, il=gridV.length; i<il; i++){
    var gv = gridV[i];
    
    //check for |fval| < EPSILON
    gv.hasGraphPoint = (gv.fval < EPSILON) && (-gv.fval < EPSILON);
    if(gv.hasGraphPoint){
    
      //create graph vertex
      gv.graphPoint = { boundary: gv.boundary, pFIG: this.axes.coordsToEPS(gv.fp), edges: [] };
      graphV.push(gv.graphPoint);
    }
  }
  
  //check edges (linear interpolation)
  for(var i=0, il=gridE.length; i<il; i++){
    var ge = gridE[i];
    
    //skip if endpoint vertices have a graphPoint
    if(ge.v0.hasGraphPoint || ge.v1.hasGraphPoint){ continue; }
    
    //interpolate and solve
    var p  = ge.v0.fp;    var q  = ge.v1.fp;
    var fp = ge.v0.fval;  var fq = ge.v1.fval;
    
    ge.hasGraphPoint = ((fp<0) && (fq>0)) || ((fp>0) && (fq<0));
    if(ge.hasGraphPoint){
    
      //find position along edge
      var t = (0 - fp) / (fq - fp);
      var lerp = [ p[0] + t*(q[0]-p[0]), p[1] + t*(q[1]-p[1]) ];
    
      //create graph vertex
      ge.graphPoint = { boundary: ge.boundary, pFIG: this.axes.coordsToEPS(lerp), edges: [] };
      graphV.push(ge.graphPoint);
    }
  }
  
  //GRAPH EDGES ON GRID RECTS
  //------------------------
  for(var i=0, il=gridR.length; i<il; i++){
    var gr = gridR[i];
    
    //connect pairs of graphPoints on incident vertices or edges
    var incidentObjs = [];
    incidentObjs.push(gr.v01); incidentObjs.push(gr.v11);
    incidentObjs.push(gr.v00); incidentObjs.push(gr.v10);
    incidentObjs.push(gr.edgeUp); incidentObjs.push(gr.edgeDown);
    incidentObjs.push(gr.edgeLeft); incidentObjs.push(gr.edgeRight);
    
    //temporary array of incident graph points
    var rectGraphPoints = [];
    
    for(var j=0, jl=incidentObjs.length; j<jl; j++){
      var io = incidentObjs[j];
      
      //check for an incident graph point
      if(io.hasGraphPoint){ rectGraphPoints.push(io.graphPoint); }
      
      //once we found two, make a new graphEdge and reset the temporary array
      if(rectGraphPoints.length == 2){
        //endpoints
        var ep = rectGraphPoints[0], eq = rectGraphPoints[1];
        
        //create graphEdge
        var graphEdge = { verts: [ep,eq] };
        ep.edges.push(graphEdge);
        eq.edges.push(graphEdge);
        graphE.push(graphEdge);
        
        //reset temporary array of graph points
        rectGraphPoints = [];
      }
    }
  }
  
  //===================================  
  //III. EPS PATH(S)
  //===================================
  //mark all graph edges as unexplored
  for(var i=0, il=graphE.length; i<il; i++){
    graphE[i].explored = false;
  }
  
  var existsUnexplored = true;
  while(existsUnexplored){
  
    //find any unexplored edge (preference: one with a dead-end vertex)
    existsUnexplored = false;
    var curEdge, curV0, curV1;
    for(var i=0, il=graphE.length; i<il; i++){
      var ge = graphE[i];
      if(ge.explored){ continue; }
      
      existsUnexplored = true;
      curEdge = ge;
      curV0 = curEdge.verts[0]; curV1 = curEdge.verts[1];
      if(curV0.edges.length == 1){ break; }
      curV0 = curEdge.verts[1]; curV1 = curEdge.verts[0];
      if(curV0.edges.length == 1){ break; }
    }
    //quit if there isn't one
    if(!existsUnexplored){ break; }
    
    //start new EPS path
    var graph = new EPSPath();
    graph.pen = this.pen;
    graph.smoothed = this.smoothed;
    graph.smoothAmount = this.smoothAmount;
    
    //"explore" curEdge and add to EPS path
    curEdge.explored = true;
    graph.points.push(curV0.pFIG);
    
    //keep track of initial path vertex, and add cap if boundary vertex
    var pathStart = curV0;
    if(pathStart.boundary){ graph.capStart = this.capBoundary; }
    
    //add points
    var existsUnexploredNeighbor = true;
    while(existsUnexploredNeighbor){
    
      //find any unexplored edge attached to curV1
      existsUnexploredNeighbor = false;
      for(var i=0, il=curV1.edges.length; i<il; i++){
        var neighbor = curV1.edges[i];
        if(neighbor.explored){ continue; }
        
        //found unexplored edge; update curEdge, curV0, curV1
        existsUnexploredNeighbor = true;
        curEdge = neighbor;
        curV0 = curV1; //new V0 is old V1
        //new V1 is the vertex of neighbor which is NOT old V1
        var newV1Index = (curV1 === neighbor.verts[0]) ? 1 : 0;
        curV1 = neighbor.verts[newV1Index];
        
        break;
      }
      //quit if there isn't one
      if(!existsUnexploredNeighbor){ break; }
      
      //"explore" curEdge and add to EPS path
      curEdge.explored = true;
      graph.points.push(curV0.pFIG);
    }
    
    //finish the EPS path
    var pathEnd = curV1;
    if(pathEnd.boundary){ graph.capEnd = this.capBoundary; }
    if(pathEnd === pathStart){ graph.closed = true; }
    else{ graph.points.push(curV1.pFIG); }
    
    this.addObject(graph);
  }
};


//=======================================================
// UI
//=======================================================

JFigGraphImplicit.caption = "Graph (Implicit)";
JFigGraphImplicit.icon = "image/graph2d/object-graph-imp.png";
JFigGraphImplicit.prototype.caption = JFigGraphImplicit.caption;
JFigGraphImplicit.prototype.icon = JFigGraphImplicit.icon;

//objects
//-------------------------------------------------------
JFigGraphImplicit.prototype.getObjectList = function(indent){
  return [{ indent: indent, icon: JFigGraphImplicit.icon, caption: JFigGraphImplicit.caption, object: this }];
};

//options
//-------------------------------------------------------
JFigGraphImplicit.prototype.setOptionsHTML = function($target, figGeom, UIMethods){
  var o = this;
  var $table;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=" + JFigGraphImplicit.icon + "> GRAPH (Implicit)"));
  
  //PARENT AXES
  //------------------------
  o.axes.appendRangeOptionsHTML($target, "Axes", figGeom, UIMethods);
  
  //CAPS
  //------------------------
  this.capBoundary.appendOptionsHTML($target, "Cap (Boundary)", figGeom, UIMethods);
  
  //FUNCTION
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Function"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //smoothing
  var $smoothing = FormTools.createCheckbox($("<span>"), "Smooth", o.smoothed, function(val){
    o.smoothed = val;
    o.refreshEPS(); UIMethods.changedEPS();
  });  
  
  //samples
  var $samplesX = $("<input>", { "type": "text", "width": "40px", "value": o.n[0] });
  $samplesX.change(function(e){
    o.n[0] = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  var $samplesY = $("<input>", { "type": "text", "width": "40px", "value": o.n[1] });
  $samplesY.change(function(e){
    o.n[1] = parseFloat($(this).val());
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( 
    $("<td>").append("N: ", $samplesX, "x", $samplesY),
    $("<td align=right>").append($smoothing)
  ));
  
  //function
  var $f = $("<input>", {"type": "text", "value": o.f});
  $f.change(function(e){
    o.f = $(this).val();
    o.refreshEPS(); UIMethods.changedEPS();
  });
  
  $table.append($("<tr>").append( $("<td colspan=2 align=right>")
    .append("<span class=infotext>JS syntax, e.g. Math.pow(x,4)</span>") )); 
  $table.append($("<tr>").append( $("<td colspan=2>").append($f) ));  
  
  //PEN
  //------------------------
  o.pen.appendStrokeOptionsHTML($target, "Pen (Stroke)", figGeom, UIMethods);
};

