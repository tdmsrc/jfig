
//=======================================================
// MISC UTILITY METHODS
//=======================================================

var FigTools = Object.freeze({

  //default pixels per inch
  DEFAULT_PPI: 96,

  //UNITS: convert between PT (points), IN (inches), CM (cm), PX (pixels)
  //-------------------------------------------------------
  PTtoIN: function(value){ return value/72; },
  PTtoCM: function(value){ return (2.54*value)/72; },
  INtoPT: function(value){ return 72*value; },
  INtoCM: function(value){ return 2.54*value; },
  CMtoPT: function(value){ return (72*value)/2.54; },
  CMtoIN: function(value){ return value/2.54; },
  
  //must specify PPI (pixels per inch) to do pixel conversions
  PTtoPX: function(value,ppi){ return ppi*value/72; },
  PXtoPT: function(value,ppi){ return 72*value/ppi; },
  
  //COLOR: convert RGB (array 0-255) to HEX ("#ABCDEF") or "01" (string-separated floats 0-1)
  //-------------------------------------------------------  
  DECtoHEX: function(d){ return ("0" + Number(d).toString(16)).slice(-2); },
  RGBtoHEX: function(rgb){ 
    return "#" + FigTools.DECtoHEX(rgb[0]) + FigTools.DECtoHEX(rgb[1]) + FigTools.DECtoHEX(rgb[2]);
  },
  
  RGBto01: function(rgb, sep){
    return (rgb[0]/255) + sep + (rgb[1]/255) + sep + (rgb[2]/255);
  },
  
  //LINE DASH: set line dash for context (weak cross-browser support)
  //-------------------------------------------------------  
  DASH: function(context, arr){
    if(typeof context.setLineDash === 'undefined'){
      if(typeof context.mozDash === 'undefined'){ }
      else{ context.mozDash = arr; }
    }
    else{ context.setLineDash(arr); }
  }
});


//=======================================================
// FIGURE GEOMETRY
//=======================================================

function FigGeometry(){

  //UI options
  this.ppi = FigTools.DEFAULT_PPI; //pixels per inch
  this.zoom = 1;
  this.canvasSize = [1,1]; //(units: PX) size of $canvas; set on init and $(window) resize via resizeCanvasFix)
  
  //set offset of fig on canvas (units: PX)
  this.center();
};

//-------------------------------------------------------
//BASIC FUNCTIONALITY
//-------------------------------------------------------

//convert points to pixels (using zoom)
//-------------------------------------------------------
FigGeometry.prototype.PTtoPX = function(val){
  return FigTools.PTtoPX(val, this.ppi*this.zoom);
};

//convert pixels to points (using zoom)
//-------------------------------------------------------
FigGeometry.prototype.PXtoPT = function(val){
  return FigTools.PXtoPT(val, this.ppi*this.zoom);
};

//position in figure (pt) to position in context (px) (using zoom)
//-------------------------------------------------------
FigGeometry.prototype.FIGtoCTX = function(p){
    return [
      this.offsetx + this.PTtoPX( p[0] ), 
      this.offsety + this.PTtoPX( project.epsSize[1]-p[1] )
    ];
};

//position in context (px) to position in figure (pt) (using zoom)
//-------------------------------------------------------
FigGeometry.prototype.CTXtoFIG = function(p){
    return [
      this.PXtoPT(p[0] - this.offsetx), 
      project.epsSize[1] - this.PXtoPT(p[1] - this.offsety)
    ];
};

//-------------------------------------------------------
//ADDITIONAL FUNCTIONS (no change to geometry)
//-------------------------------------------------------

//EPS size to pixels
//-------------------------------------------------------
FigGeometry.prototype.epsSizePX = function(){
  return [ 
    this.PTtoPX(project.epsSize[0]), 
    this.PTtoPX(project.epsSize[1]) 
  ];
};

//position in figure (pt) to position in context (px) [i.e. from bottom right]
//-------------------------------------------------------
FigGeometry.prototype.FIGtoCTXrb = function(p){
    var cpos = this.FIGtoCTX(p);
    return [
      this.canvasSize[0] - cpos[0],
      this.canvasSize[1] - cpos[1]
    ];
};

//get center of canvas as point on the figure
//-------------------------------------------------------
FigGeometry.prototype.CTXCenterToFIG = function(){
  return this.CTXtoFIG( [this.canvasSize[0]/2, this.canvasSize[1]/2] );
};

//-------------------------------------------------------
//ADDITIONAL FUNCTIONS (changes geometry)
//-------------------------------------------------------

//set offset so that pFig of the figure (PT) is at the point pCtx on the canvas (PX)
//-------------------------------------------------------
FigGeometry.prototype.setOffset = function(pFig, pCtx){
  this.offsetx = pCtx[0] - this.PTtoPX(pFig[0]);
  this.offsety = pCtx[1] - this.PTtoPX(project.epsSize[1] - pFig[1]);
};

//center (optional input: p on fig in units PT)
//-------------------------------------------------------
FigGeometry.prototype.center = function(p){
  if(typeof(p) === "undefined"){ p = [project.epsSize[0]/2, project.epsSize[1]/2]; }
  
  this.setOffset(p, [this.canvasSize[0]/2,this.canvasSize[1]/2]);
};

//resize canvas, fixing the point at the center of the canvas
//-------------------------------------------------------
FigGeometry.prototype.resizeCanvasFix = function(canvasSize){
  var p = this.CTXCenterToFIG();
  this.canvasSize = canvasSize;
  this.center(p); //center at original point
};

//zoom in/out, fixing the point at the center of the canvas
//-------------------------------------------------------
FigGeometry.prototype.zoomFix = function(factor){
  var p = this.CTXCenterToFIG();
  this.zoom = factor;
  this.center(p); //center at original point
};


//=======================================================
// CANVAS OPTIONS (resize etc)
//=======================================================

//create options panel
//-------------------------------------------------------
var canvasSetOptionsHTML = function($target, figGeom, UIMethods){

  var $table;
  
  //TITLE
  //------------------------
  $target.append($("<div class=panel-title>").append("<img src=image/canvas.png> CANVAS"));
  
  //SIZE
  //------------------------
  $target.append($("<div class=panel-subtitle>").append("Resize"));
  $table = $("<table>");
  $target.append($("<div class=panel-contents>").append($table));
  
  //current size
  var epsSizeIN = [ FigTools.PTtoIN(project.epsSize[0]), FigTools.PTtoIN(project.epsSize[1]) ];
  var $sizeX = $("<input>", {"type": "text", "width": "40px", "value": epsSizeIN[0], "readonly":""});  
  var $sizeY = $("<input>", {"type": "text", "width": "40px", "value": epsSizeIN[1], "readonly":""});  
  
  $table.append($("<tr>").append(
    $("<td>").append("Size (in):"), 
    $("<td align=right>").append($sizeX, "x", $sizeY)
  ));
  
  $table.append($("<tr>").append( $("<td colspan=2>").append("<span class=infotext>Amounts may be negative</span>") ));
  
  //resize H
  var $addLeft = $("<input>", {"type": "text", "width": "40px", "value": 0});
  var $addRight = $("<input>", {"type": "text", "width": "40px", "value": 0});
  
  $table.append($("<tr>").append(
    $("<td>").append("<img src=image/halign-right.png> Add left:"), 
    $("<td align=right>").append($addLeft, "in")
  ));
  
  $table.append($("<tr>").append(
    $("<td>").append("<img src=image/halign-left.png> Add right:"), 
    $("<td align=right>").append($addRight, "in")
  ));
  
  //resize V
  var $addTop = $("<input>", {"type": "text", "width": "40px", "value": 0});
  var $addBottom = $("<input>", {"type": "text", "width": "40px", "value": 0});
  
  $table.append($("<tr>").append(
    $("<td>").append("<img src=image/valign-bottom.png> Add top:"), 
    $("<td align=right>").append($addTop, "in")
  ));
  
  $table.append($("<tr>").append(
    $("<td>").append("<img src=image/valign-top.png> Add bottom:"), 
    $("<td align=right>").append($addBottom, "in")
  ));
  
  //handlers
  //----------------------------------------
  var updateAll = function(){
    UIMethods.changedEPS();
    UIMethods.changedLabels();
    
    $sizeX.val( FigTools.PTtoIN(project.epsSize[0]) );
    $sizeY.val( FigTools.PTtoIN(project.epsSize[1]) );
    
    $addLeft.val(0);  $addRight.val(0);
    $addTop.val(0);   $addBottom.val(0);
  };
  
  $addLeft.change(function(e){
    var dx = parseFloat($(this).val());
    resizeEPS(FigTools.INtoPT(dx),0,0,0);
    updateAll();
  });
  
  $addRight.change(function(e){
    var dx = parseFloat($(this).val());
    resizeEPS(0,0,FigTools.INtoPT(dx),0);
    updateAll();
  });
  
  $addTop.change(function(e){
    var dy = parseFloat($(this).val());
    resizeEPS(0,FigTools.INtoPT(dy),0,0);
    updateAll();
  });
  
  $addBottom.change(function(e){
    var dy = parseFloat($(this).val());
    resizeEPS(0,0,0,FigTools.INtoPT(dy));
    updateAll();
  });
  //----------------------------------------
};

//add indicated value to width or height (units: PT)
var resizeEPS = function(addLeft, addTop, addRight, addBottom){
  
  //save old center point before modifying
  var oldCenter = figGeom.CTXCenterToFIG();
  
  //update canvas size
  project.epsSize = [ project.epsSize[0]+addLeft+addRight, project.epsSize[1]+addTop+addBottom ];
  
  //translate objects
  project.contents.translate(addLeft, addBottom);
  
  //re-center on originally-centered point
  var newCenter = [oldCenter[0]+addLeft, oldCenter[1]+addBottom];
  figGeom.center(newCenter); 
};


//RESIZE (call when $canvasWrapper resizes)
//-----------------------------------------------
var resize = function($canvasWrapper, $canvasBG, $canvas, figGeom) 
{
  //get new size
  var w = $canvasWrapper.width();
  var h = $canvasWrapper.height();
  
  //resize canvas/canvasBG
  var canvasBG = $canvasBG[0];
  canvasBG.width = w;
  canvasBG.height = h;
  
  var canvas = $canvas[0];
  canvas.width = w;
  canvas.height = h;
  
  //recreate geometry
  figGeom.resizeCanvasFix([w,h]);
};

